SELECT page.title, c.id as category_id, ca.name as category, c.name as attribute_name,p.path, p.senseStringDEBUG, p.id as path_id, p.frequency
FROM nlpdb.WikipediaCategoryAttributeCorePath p 
	 join nlpdb.WikipediaCategoryAttribute c on c.id = p.categoryAttribute_id
	 join nlpdb.WikipediaCategory ca on ca.id = c.category_id
	 join nlpdb.WikipediaPage_Categories wpc on wpc.categories_id = c.category_id 
	 join nlpdb.WikipediaPage page on page.id = wpc.pages_id;