SELECT DISTINCT c.name AS Category, a.name As Attribute, ac.id AS PathId, ac.path as Path, ac.frequency AS frequency
FROM nlpdb.WikipediaCategory c 
	join nlpdb.WikipediaEntityCategory ec on c.id=ec.id
	join nlpdb.WikipediaCategoryAttribute a on c.id = a.category_id
	join nlpdb.WikipediaCategoryAttribute_CorePaths cp on cp.WikipediaCategoryAttribute_id=a.id
	join nlpdb.AttributeCorePath ac on ac.id=cp.corePaths_id
	ORDER BY c.name;