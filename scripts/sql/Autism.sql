


(
SELECT "shared" as source, a.subject AS subject, a.predicate as predicate, a.object as object, a.objectTypeName As aType, d.objectTypeName AS dType 
FROM auerlehmann.TemplateRelation a, dbpedia.DbPediaTriple d
where a.subject = 'Aristotle' and a.subject like d.subject and d.predicate like a.predicate and a.object like d.object
)
UNION 
(
select "auer" as source, a.subject AS subject, a.predicate as predicate, a.object as object, a.objectTypeName As aType, "None" AS dType 
FROM auerlehmann.TemplateRelation a, dbpedia.DbPediaTriple d
where a.subject = 'Aristotle' and a.subject like d.subject and (d.predicate not like a.predicate or a.object not like d.object)
);
