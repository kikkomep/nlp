SELECT DISTINCT page_id, title, name
FROM nlpdb.WikipediaPage p, nlpdb.WikipediaTemplate t, dbpedia.DbPediaTriple dbp
WHERE p.id = t.page_id AND t.name LIKE '%Infobox%' and dbp.subject = p.title
GROUP BY p.id, t.name, dbp.subject;
