SELECT DISTINCT page_id, title, name
FROM nlpdb.WikipediaPage p, nlpdb.WikipediaTemplate t
WHERE p.id = t.page_id AND t.name LIKE '%Infobox%'
GROUP BY p.id, t.name;
