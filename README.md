## Readme ##
Instructions for compiling and distributing **lamepan** project.

### Compile ###
To compile use <code>mvn package</code>

### Dist ###
To create a zip distribution within the target directory <code>mvn assembly:assembly</code>

### Launch
To launch lamepan APP use the following syntax:

<code>./lamepan.sh --app [wuweld|auerlehmann] --parameters parameterFile.properties
