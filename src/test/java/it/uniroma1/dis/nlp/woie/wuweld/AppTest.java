package it.uniroma1.dis.nlp.woie.wuweld;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit processPages for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the processPages case
     *
     * @param testName name of the processPages case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
}
