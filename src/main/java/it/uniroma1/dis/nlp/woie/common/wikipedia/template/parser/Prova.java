package it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser;

import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaTemplate;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaTemplateAttributeAssociation;
import org.apache.log4j.Logger;

/**
 * Created by kikkomep on 12/19/13.
 */
public class Prova implements WikipediaTemplateProcessor {

    private Logger logger = Logger.getLogger(Prova.class);

    @Override
    public void startTemplate(WikipediaTemplate template) {


        logger.debug("New Template ");

    }

    @Override
    public void endTemplate(WikipediaTemplate template) {

        logger.debug("\n\n End Template ");


    }

    @Override
    public void addAttribute(WikipediaTemplateAttributeAssociation attribute) {

        logger.debug("New Attribute ");

    }

    @Override
    public void addTemplateAttribute(WikipediaTemplateAttributeAssociation attribute) {

        logger.debug("New Template + ");

    }
}
