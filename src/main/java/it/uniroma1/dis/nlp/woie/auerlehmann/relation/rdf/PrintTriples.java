package it.uniroma1.dis.nlp.woie.auerlehmann.relation.rdf;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import it.uniroma1.dis.nlp.woie.App;

/**
 * Created by kikkomep on 1/2/14.
 */
public class PrintTriples extends App {

    @Override
    public void start() {

        Dataset dataset = JenaTemplateRelationsRepository.getInstance().getDataset();



        //Dataset dataset = TDBFactory.createDataset("datasets/mydata");




        Model model = dataset.getDefaultModel();

        //dataset.begin(ReadWrite.READ);
        model.write(System.out, "N-Triples");

        if(true) return;


        dataset.begin(ReadWrite.WRITE);

        Resource r = model.createResource("http://www.Pippo.it");
        Literal l = model.createTypedLiteral("Ok");
        r.addProperty(model.createProperty("pi"), l);


        model.write(System.out, "N-Triples");

        dataset.commit();
        dataset.end();
    }


    /**
     * Entry for the PrintTriples
     *
     * @param args
     */
    public static void main(String args[]) {
        init(new PrintTriples(), args);
    }
}
