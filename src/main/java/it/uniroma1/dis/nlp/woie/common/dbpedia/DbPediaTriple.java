package it.uniroma1.dis.nlp.woie.common.dbpedia;

import it.uniroma1.dis.nlp.woie.common.relation.Relation;

import javax.persistence.Entity;

/**
 * Created by kikkomep on 12/21/13.
 */
@Entity
public class DbPediaTriple extends Relation {
    private OBJECTY_TYPE type;

    public static enum OBJECTY_TYPE {

        RESOURCE,
        STRING,
        DOUBLE,
        INTEGER,
        IMAGE,
        LINK, URL,
        RANK,
        DATE,
        DATE_DAY,
        DATE_MONTH,
        DATE_YEAR,
        MONEY,
        LARGE_NUMBER,
        YEAR_RANGE,
        BIG_MONEY,
        PERCENT,
        UNIT,
        UNDEFINED;
    }

    public OBJECTY_TYPE getType() {
        return type;
    }

    protected String objectTypeName;

    public void setType(OBJECTY_TYPE type) {
        this.type = type;
        this.objectTypeName = this.type.name();
    }
}
