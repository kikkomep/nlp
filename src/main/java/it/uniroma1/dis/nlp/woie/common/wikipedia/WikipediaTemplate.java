/*
 * Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
 * Mountain View, California, 94041, USA.
 */
package it.uniroma1.dis.nlp.woie.common.wikipedia;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author kikkomep
 */
@Entity
public class WikipediaTemplate extends WikipediaTemplateAttribute {


    @NotNull
    @Lob
    protected String name;


    @Type(type = "text")
    private String rawText;


    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    protected WikipediaPage page;


    @OneToMany(cascade = CascadeType.ALL)
    protected List<WikipediaTemplateAttributeAssociation> templateAttributes = new LinkedList<>();


    public WikipediaTemplate() {
    }



    public WikipediaTemplate(String name) {
        this.name = name;
    }



    @Override
    public String getValue() {
        return this.getName();
    }

    public String getRawText() {
        return rawText;
    }

    public void setRawText(String rawText) {
        this.rawText = rawText;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    void setPage(WikipediaPage page) {
        this.page = page;
    }

    public WikipediaPage getPage() {
        return page;
    }

    public boolean isNested() {
        return this.getParentAssociation()!=null;
    }


    public List<? extends WikipediaTemplateAttributeAssociation> getAttributes() {
        return (List<WikipediaTemplateAttributeAssociation>) Collections.unmodifiableList(
                this.templateAttributes);
    }

    public void addAttribute(String name, WikipediaTemplateAttribute attribute) {
        if(name!=null && attribute!=null && attribute.getParentAssociation()==null){
            WikipediaTemplateAttributeAssociation attr = new WikipediaTemplateAttributeAssociation(name, attribute);
            this.addAttribute(attr);
        }
    }

    public void addAttribute(WikipediaTemplateAttributeAssociation attribute) {


        if(attribute.getAttribute() instanceof WikipediaTemplateValueAttribute){
            WikipediaTemplateValueAttribute ax = (WikipediaTemplateValueAttribute)attribute.getAttribute();
            if(ax.getParentAssociation()!=null)
                throw new RuntimeException("Attenzione: " + ax.getParentAssociation() + " " + " " + ax.getValue());
        }

        if (attribute != null && attribute.getParent() == null) {

            if(this.templateAttributes.contains(attribute))
                throw new RuntimeException("DUPLICATION---------->");


            attribute.setParent(this);
            this.templateAttributes.add(attribute);


            if(attribute.getAttribute() instanceof WikipediaTemplateValueAttribute){
                WikipediaTemplateValueAttribute a = (WikipediaTemplateValueAttribute) attribute.getAttribute();
                a.setParent(attribute);


            }else if(attribute.getAttribute() instanceof WikipediaTemplate){
                WikipediaTemplate a = (WikipediaTemplate) attribute.getAttribute();
                a.setParent(attribute);
                a.setPage(this.getPage());
            }
        }


        if(attribute.getAttribute()==null) throw new RuntimeException("Oh my God");
    }

    public void removeAttribute(WikipediaTemplateAttributeAssociation attribute) {
        if (attribute != null && attribute.getParent() == this) {
            this.templateAttributes.remove(attribute);

            if(attribute.getAttribute() instanceof WikipediaTemplateAttribute){
                WikipediaTemplateAttribute a = (WikipediaTemplateAttribute) attribute.getAttribute();
                a.setParent(null);
            }else if(attribute.getAttribute() instanceof WikipediaTemplate){
                WikipediaTemplate a = (WikipediaTemplate) attribute.getAttribute();
                a.setParent(null);
            }

        }
    }


    public int numberOfAttributes() {
        return countAttributes(this);
    }

    private int countAttributes(WikipediaTemplate t) {
        int count = 0;
        count += t.getAttributes().size();


        return count;
    }


    @Override
    public String toString() {
        return "WikipediaTemplate{" +
                "\n\t - name='" + name + '\'' +
                "\n\t - attributes=" + templateAttributes +
                "\n\t - rawText='" + rawText + '\'' +
                '}';
    }
}
