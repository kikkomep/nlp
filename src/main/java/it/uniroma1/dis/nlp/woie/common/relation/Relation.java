package it.uniroma1.dis.nlp.woie.common.relation;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by kikkomep on 12/14/13.
 */

@MappedSuperclass
public abstract class Relation {

    protected static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @NotNull
    @Lob
    protected String subject;

    @NotNull
    @Lob
    protected String predicate;

    @NotNull
    @Lob
    protected String object;



    public Relation() {
    }


    public Relation(String subject, String predicate, String object) {
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
    }

    public Long getId() {
        return id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPredicate() {
        return predicate;
    }

    public void setPredicate(String predicate) {
        this.predicate = predicate;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "Relation {" +
                "id=" + id +
                ", subject='" + subject + '\'' +
                ", predicate='" + predicate + '\'' +
                ", object='" + object + '\'' +
                '}';
    }
}
