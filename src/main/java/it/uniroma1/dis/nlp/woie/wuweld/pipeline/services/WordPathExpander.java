package it.uniroma1.dis.nlp.woie.wuweld.pipeline.services;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.trees.EnglishGrammaticalRelations;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.ParseException;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.RelationModifiersSelector;
import it.uniroma1.lcl.jlt.ling.Word;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.*;

/**
 * Created by lgu on 03/02/14.
 */
public class WordPathExpander extends Expander{

    private Logger logger = LoggerFactory.getLogger(WordPathExpander.class);

    private Collection<IndexedWord> relation;

    public WordPathExpander(SemanticGraph semanticGraph, IndexedWord principalWord) {
        super(semanticGraph,principalWord);
        relation = null;
    }

    private List<IndexedWord> getPrepositionalObject(IndexedWord principal,boolean includeModifier){

        LinkedList<IndexedWord> relation = new LinkedList<>();
        List<IndexedWord> prepositionalModifiers = semanticGraph.getChildrenWithReln(principal, EnglishGrammaticalRelations.PREPOSITIONAL_MODIFIER);

        for(IndexedWord word:prepositionalModifiers){
            relation.addAll(semanticGraph.getChildrenWithReln(word,EnglishGrammaticalRelations.PREPOSITIONAL_OBJECT));
        }

        if(includeModifier)
            relation.addAll(prepositionalModifiers);

        return relation;
    }

    private void expand() {

        if(relation!=null) return;

        RelationModifiersSelector selector = RelationModifiersSelector.getInstance();
        List<IndexedWord> relation = semanticGraph.getChildrenWithRelns(principalWord, selector.getRelationsToBeIncludedForNoun());
        relation.add(principalWord);

        List<IndexedWord> prepositionalObjects = getPrepositionalObject(principalWord,false);

        relation.addAll(getPrepositionalObject(principalWord,true));

        for(IndexedWord prepositionalObject:prepositionalObjects){
            List<IndexedWord> temp =getPrepositionalObject(prepositionalObject,true);
            relation.addAll(temp);
            for(IndexedWord word : temp){
                relation.addAll(semanticGraph.getChildrenWithRelns(word, selector.getRelationsToBeIncludedForNoun()));
            }
        }

        Collections.sort(relation, new Comparator<IndexedWord>() {
            @Override
            public int compare(IndexedWord o1, IndexedWord o2) {
                return o1.index() - o2.index();
            }
        });

        this.relation = relation;

    }

    public String getExpandedWordString(){
        expand();

        StringBuilder sb = new StringBuilder();
        for (IndexedWord w : relation) {
            logger.debug("Expanding: ..." + w.toString() + " " + w.tag());
            sb.append(w.word());
            sb.append(" ");
        }

        return sb.toString();
    }

    public Collection<IndexedWord> getIndexedWordCollection(){
        expand();
        return relation;
    }

    public  Collection<Word> getWordCollection() {

        expand();

        HashSet<Word> result = new HashSet<>();
        for(IndexedWord word: relation){
            try {
                result.add(transformIndexedWord(word));
            } catch (ParseException e) {

            }
        }
        return result;
    }
}
