package it.uniroma1.dis.nlp.woie.common.wikipedia;

import it.uniroma1.dis.nlp.woie.wuweld.woe.pattern.DependencyPath;

import javax.persistence.*;
import java.util.*;

/**
 * Created by kikkomep on 12/28/13.
 */
@Entity
public class WikipediaCategoryAttributeCorePath extends DependencyPath {

    /*
    private String commonGovernorBNSense;
    private String commonGovernor;
    */

    @Lob
    @Basic
    private HashMap<String, HashSet<String>> governorSenses = new HashMap<>();

    @ManyToOne(optional = false)
    private WikipediaCategoryAttribute categoryAttribute;

    private String senseStringDEBUG;

    WikipediaCategoryAttributeCorePath() {

    }

    WikipediaCategoryAttributeCorePath(WikipediaCategoryAttribute attribute, String path){
        super(path);
        this.categoryAttribute = attribute;
    }

    /*
    WikipediaCategoryAttributeCorePath(WikipediaCategoryAttribute attribute, String path, String commonGovernor, String commonGovernorBNSense){
        super(path);
        this.categoryAttribute = attribute;
        this.commonGovernorBNSense = commonGovernorBNSense;
        this.commonGovernor = commonGovernor;
    }*/

    public boolean isDisambiguated(){
        return !this.governorSenses.isEmpty();
    }


    public void addGovernorSense(String sense, String lemma){
        HashSet<String> words;
        if(this.governorSenses.containsKey(sense)){
            words = this.governorSenses.get(sense);

        }else{
            words = new HashSet<>();
            this.governorSenses.put(sense, words);
        }

        words.add(lemma);

        // Only for debug
        StringBuilder builder = new StringBuilder();
        Iterator<String> sensesIterator = governorSenses.keySet().iterator();
        while(sensesIterator.hasNext()){
            String asense = sensesIterator.next();

            builder.append(asense + " -> {");
            Iterator<String> lemmasIterator = governorSenses.get(asense).iterator();
            while(lemmasIterator.hasNext()){
                builder.append(lemmasIterator.next());
                if(lemmasIterator.hasNext())
                    builder.append(", ");
            }
            builder.append("}");
            if(sensesIterator.hasNext())
                builder.append(", ");
        }

        this.senseStringDEBUG = builder.toString();
    }

    public void removeGovernorSense(String sense, String lemma){
        if(this.governorSenses.containsKey(sense)){
            this.governorSenses.get(sense).remove(lemma);
        }
    }

    public boolean containsGovernorSense(String governorSense){
        return this.governorSenses.containsKey(governorSense);
    }

    public Set<String> getGovernorSenses(){
        return this.governorSenses.keySet();
    }

    public Set<String> getGovernorSenseLemmas(String governorSense){
        if(!this.governorSenses.containsKey(governorSense)) return null;
        return this.governorSenses.get(governorSense);
    }

    /*
    public String getCommonGovernorBNSense() {
        return commonGovernorBNSense;
    }

    public void setCommonGovernorBNSense(String commonGovernorBNSense) {
        this.commonGovernorBNSense = commonGovernorBNSense;
    }

    public String getCommonGovernor() {
        return commonGovernor;
    }*/

    public WikipediaCategoryAttribute getCategoryAttribute() {
        return categoryAttribute;
    }

    void setCategoryAttribute(WikipediaCategoryAttribute categoryAttribute) {
        this.categoryAttribute = categoryAttribute;
    }
}
