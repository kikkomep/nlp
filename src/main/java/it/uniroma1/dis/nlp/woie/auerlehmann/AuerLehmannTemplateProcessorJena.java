package it.uniroma1.dis.nlp.woie.auerlehmann;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.VCARD;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaTemplate;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaTemplateAttributeAssociation;
import it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.WikipediaTemplateProcessor;
import org.apache.log4j.Logger;

/**
 * Created by kikkomep on 12/19/13.
 */
public class AuerLehmannTemplateProcessorJena implements WikipediaTemplateProcessor{


    private Logger logger = Logger.getLogger(AuerLehmannTemplateProcessorJena.class);

    private Dataset dataset;
    private Model model;

    public AuerLehmannTemplateProcessorJena(Dataset dataset) {
        this.dataset = dataset;
        this.model = this.dataset.getDefaultModel();
    }

    @Override
    public void startTemplate(WikipediaTemplate template) {

        logger.debug("New Template " +  ":" + template.getName());

        // some definitions
        String personURI    = "http://somewhere/JohnSmith";
        String fullName     = "John Smith";

        Resource resource = model.createResource(personURI);
        resource.addProperty(VCARD.EMAIL, "kikkomep@tiscali.it");


// add the property





    }

    @Override
    public void endTemplate(WikipediaTemplate template) {
        logger.debug("End Template " + ":" + template.getName());
    }

    @Override
    public void addAttribute(WikipediaTemplateAttributeAssociation attribute) {

        logger.debug("New Attribute " + attribute.getName() + " " + attribute.getAttribute());

    }

    @Override
    public void addTemplateAttribute(WikipediaTemplateAttributeAssociation attribute) {
        logger.debug("New NestedTemplate " + attribute.getName() + " " + attribute.getAttribute());
    }

    public void addFilter(WikipediaTemplateFilter filter) {

    }
}
