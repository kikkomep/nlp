package it.uniroma1.dis.nlp.woie.common.wikipedia.utils;

import info.bliki.wiki.filter.PlainTextConverter;
import info.bliki.wiki.model.WikiModel;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaTemplateAttribute;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaTemplateComplexAttribute;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaTemplateLiteralAttribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

/**
 * @author Antonio
 */
public class WikipediaUtils {

    private static Logger logger = LoggerFactory.getLogger(WikipediaUtils.class);

    public static void main(String[] args) {

        String TEXT = "Evitamo";

        String regEx = getPartialMatchRegEx(TEXT);

        System.out.println(regEx);


        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher("E");
        int wordCount = 0;
        while (m.find()) {
            wordCount++;
            System.out.println(m.group(0));

        }
        System.out.println(wordCount);
    }

    public static String getPartialMatchRegEx(String text) {

        String[] tokens = text.split(" |_");

        if (tokens.length == 1) return tokens[0];

        StringBuilder regEx = new StringBuilder();

        regEx.append("(");

        for (int i = tokens.length - 1; i > 0; i--) {

            for (int j = i; j <= tokens.length - 1; j++) {
                regEx.append(tokens[j]);
                if (j < tokens.length - 1) {
                    regEx.append(" ");
                }
            }

            if (i > 1) {
                regEx.append("|");
            }
        }

        regEx.append("|");

        for (int i = tokens.length - 2; i >= 0; i--) {

            for (int j = 0; j <= i; j++) {
                regEx.append(tokens[j]);
                if (j < i) {
                    regEx.append(" ");
                }
            }

            if (i > 0) {
                regEx.append("|");
            }
        }

        regEx.append(")");

        return regEx.toString();
    }


    public static byte[] compressText(String text) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            OutputStream out = new DeflaterOutputStream(baos);
            out.write(text.getBytes("UTF-8"));
            out.close();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
        return baos.toByteArray();
    }


    public static String decompress(byte[] bytes) {
        InputStream in = new InflaterInputStream(new ByteArrayInputStream(bytes));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            byte[] buffer = new byte[8192];
            int len;
            while ((len = in.read(buffer)) > 0)
                baos.write(buffer, 0, len);
            return new String(baos.toByteArray(), "UTF-8");
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }


    public static String toPlainText(String rawText) {

        WikiModel wikiModel = new WikiModel(
                "http://www.mywiki.com/wiki/${image}",
                "http://www.mywiki.com/wiki/${title}");
        String plainStr = wikiModel.render(new PlainTextConverter(), rawText);

        return plainStr;
    }


    public static String toHtml(String rawText) {
        return WikiModel.toHtml((rawText));
    }

    public static String resolveImageLink(String image) {
        return "http://en.wikipedia.org/wiki/File:" + image;
    }

    public static String resolveInterwikiLink(String interwiki) {

        String result = null;

        if (interwiki != null && !interwiki.startsWith("http://")) {
            if (!interwiki.startsWith("[["))
                interwiki = "[[" + interwiki + "]]";

            String aTag = WikiModel.toHtml(interwiki);

            Pattern p = Pattern.compile("<p><a href=\"([^\"]*)\"");
            Matcher m = p.matcher(aTag);
            if (m.find()) {
                result = m.group(1);
            }

            if (result != null && result.startsWith("/"))
                result = "http://en.wikipedia.org/wiki" + result;

        }

        return result;
    }



    public static List<String> getLiteralAttributeValues(WikipediaTemplateAttribute wta) {
        List<String> result = new LinkedList<>();

        if (wta instanceof WikipediaTemplateComplexAttribute) {

            WikipediaTemplateComplexAttribute wtca = (WikipediaTemplateComplexAttribute) wta;

            for (WikipediaTemplateLiteralAttribute wtla : wtca.getAttributes()) {
                result.addAll(getValueFromLiteral(wtla));
            }

        } else if (wta instanceof WikipediaTemplateLiteralAttribute) {
            WikipediaTemplateLiteralAttribute wtla = (WikipediaTemplateLiteralAttribute) wta;
            result.addAll(getValueFromLiteral(wtla));
        }else{
            result.add(wta.getValue());
        }
        return result;
    }


    private static List<String> getValueFromLiteral(WikipediaTemplateLiteralAttribute wtla) {
        List<String> result = new LinkedList();

        if(wtla.getLiteralType()== WikipediaTemplateLiteralAttribute.TYPE.STRING){
            Matcher m = Pattern.compile("(([\\w\\p{IsLatin}]+?)($|[^\\w\\p{IsLatin}]))").matcher(wtla.getValue());
            while (m.find()) {
                if (m.end() - m.start() < 100) {
                    result.add(m.group(2));
                }
            }

        }else{
            result.add(wtla.getValue());
        }

        return result;

    }

    private static Pattern infoboxPattern = Pattern.compile("(?i)Infobox(.*)", Pattern.CASE_INSENSITIVE);
    public static String extractInfoboxTypeName(String infoboxName){
        String result = null;

        // Set the extended Category
        Matcher matcher = infoboxPattern.matcher(infoboxName);
        if (matcher.find()) {

            String categoryName = matcher.group(1).trim();
            if(!categoryName.isEmpty()){
                logger.debug("--> Extended Category detected for '" + matcher.group(0) + "': " + categoryName);
                result = categoryName;
            }
        }

        return result;
    }


}
