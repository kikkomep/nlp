/*
 * Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
 * Mountain View, California, 94041, USA.
 */
package it.uniroma1.dis.nlp.woie.auerlehmann;

import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaTemplate;

/**
 *
 * @author kikkomep
 */
public abstract class WikipediaTemplateFilter {

    private String name;

    public WikipediaTemplateFilter() {
        this.name = this.getClass().getCanonicalName();
    }

    public WikipediaTemplateFilter(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public abstract boolean reject(WikipediaTemplate template);
}
