/*
 * Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
 * Mountain View, California, 94041, USA.
 */
package it.uniroma1.dis.nlp.woie.common.wikipedia;

import it.uniroma1.dis.nlp.woie.common.wikipedia.utils.WikipediaProcessedPageHistory;
import it.uniroma1.dis.nlp.woie.utils.AppEntityManagerFactory;
import it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.ParseException;
import it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.WikipediaTemplateParser;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.io.InputStream;
import java.util.*;

/**
 * @author kikkomep
 */
public class Wikipedia implements WikipediaPageLoader {

    private static Wikipedia instance;
    private final WikipediaPageLoader loader;
    private final WikipediaTemplateParser parser;
    private final EntityManager em;

    private EntityTransaction transaction = null;

    public static Wikipedia getInstance() {
        if (instance == null) {
            instance = new Wikipedia();
        }
        return instance;
    }

    private final Logger logger = Logger.getLogger(
            Wikipedia.class.getCanonicalName());

    private Wikipedia() {
        loader = WikipediaPageLoaderFactory.getIntance().getPageLoader();
        parser = new WikipediaTemplateParser();
        em = AppEntityManagerFactory.getInstance().
                getEntityManager(AppEntityManagerFactory.NAME.pwr);

    }

    public void startSession() {
        if (this.transaction != null) {
            this.closeSession();
        }

        this.transaction = this.em.getTransaction();
        this.transaction.begin();
    }

    public boolean isSessionActive() {
        return this.transaction != null;
    }

    public void closeSession() {
        if (this.transaction != null) {
            try {
                if (this.transaction.isActive()) {
                    this.em.flush();
                    this.transaction.commit();
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (this.transaction.isActive())
                    this.transaction.rollback();
            } finally {
                this.transaction = null;
            }
        }
    }


    public WikipediaTemplateParser getDefaultParser() {
        return this.parser;
    }


    @Override
    public InputStream newInputStream(WikipediaPage page) throws WikipediaPageInputStreamNotAvailableException {
        return loader.newInputStream(page);
    }


    public WikipediaPage parse(String pageTitle) throws WikipediaPageNotFoundException, WikipediaPageInputStreamNotAvailableException, ParseException {
        WikipediaPage page = loader.loadByTitle(pageTitle);
        parser.parse(page);
        return page;
    }

    public void parse(WikipediaPage page) throws WikipediaPageNotFoundException, WikipediaPageInputStreamNotAvailableException, ParseException {
        parser.parse(page);
    }


    public WikipediaPage refresh(WikipediaPage page) throws ParseException, WikipediaPageInputStreamNotAvailableException, WikipediaPageNotFoundException {
        if (page != null) {
            try {

                delete(page);

                WikipediaPage cpage = loadByTitle(page.getTitle());
                page.setPageLoader(cpage.getPageLoader());
                page.setLastModified(cpage.getLastModified());
                parse(cpage);
                save(cpage);
                return cpage;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return page;
    }

    @Override
    public WikipediaPage loadByTitle(String pageTitle) throws WikipediaPageNotFoundException {
        return loader.loadByTitle(pageTitle);
    }


    public List<WikipediaPage> loadPages(int numberOfPages, int skipFirstPages, boolean printSkips) {
        return loader.loadPages(numberOfPages, skipFirstPages, printSkips);
    }

    @Override
    public List<WikipediaPage> loadPages(int numberOfPages, int skipFirstPages) {
        return loader.loadPages(numberOfPages, skipFirstPages);
    }

    @Override
    public List<WikipediaPage> loadPages(int numberOfPages) {
        return loader.loadPages(numberOfPages);
    }

    public WikipediaPage findById(Long id) {

        try {

            WikipediaPage page = em.find(WikipediaPage.class, id);
            if (page != null)
                page.setPageLoader(loader);
            return page;

        } catch (javax.persistence.NoResultException e) {
            logger.error(e.getMessage());
        }

        return null;
    }

    public WikipediaPage findByTitle(String title) {

        try {

            if (title != null && !title.isEmpty()) {
                String no_blank = title.replaceAll(" ", "_");
                title = no_blank.replaceAll("_", " ");

                System.out.println("Searching: " + title);

                Query q;
                q = em.createQuery(
                        "SELECT p FROM WikipediaPage p WHERE p.title = :title OR p.title = :title_nb");
                q.setParameter("title", title);
                q.setParameter("title_nb", no_blank);
                WikipediaPage page = (WikipediaPage) q.getSingleResult();
                if (page != null) page.setPageLoader(loader);
                return page;
            }

        } catch (javax.persistence.NoResultException e) {
            logger.debug(e.getMessage());
        }

        return null;
    }


    public List<WikipediaPage> find(int numberOfPages, int skipFirst) {
        return this.find(numberOfPages, skipFirst, false);
    }

    public List<WikipediaPage> find(int numberOfPages, int skipFirst, boolean forceParseAndSave) {
        List<WikipediaPage> result = new ArrayList<>();

        logger.info("Loading " + numberOfPages + " pages....");

        List<WikipediaPage> toParsePages = null;
        List<WikipediaPage> parsedPages = findAll();


        HashSet<String> titles = new HashSet<>();
        for (int i = 0; i < parsedPages.size(); i++) {
            titles.add(parsedPages.get(i).getTitle());
            if (i == numberOfPages + skipFirst) break;
            if (i >= skipFirst)
                result.add(parsedPages.get(i));
        }

        if (forceParseAndSave) {

            int numOfPagesToParse = numberOfPages - (parsedPages.size() - skipFirst);

            logger.debug("\t - Pages to find: " + numberOfPages);
            logger.debug("\t - Pages to skip: " + skipFirst);
            logger.debug("\t - Pages already parsed: " + parsedPages.size());
            logger.debug("\t - New Pages to parse: " + (numOfPagesToParse > 0 ? numOfPagesToParse : 0));

            if (numOfPagesToParse > 0) {

                int toSkip = parsedPages.size();

                do {
                    toParsePages = loadPages(numOfPagesToParse, toSkip);
                    for (WikipediaPage page : toParsePages) {
                        try {

                            toSkip++;
                            if (!containsIC(titles, page.getTitle())) {
                                parse(page);
                                save(page);
                                result.add(page);

                                numOfPagesToParse--;

                                titles.add(page.getTitle());
                            }

                            if (numOfPagesToParse == 0) break;

                        } catch (WikipediaPageNotFoundException | WikipediaPageInputStreamNotAvailableException e) {
                        } catch (ParseException e) {
                            logger.debug(e.getMessage());
                            save(page); // To skip the page
                        }
                    }

                } while (numOfPagesToParse > 0 && toParsePages.size() > 0);
            }
        }

        return result;
    }


    private boolean containsIC(Set<String> set, String s) {
        for (String x : set)
            if (x.equalsIgnoreCase(s))
                return true;
        return false;
    }


    public List<WikipediaPage> findContainingInfoboxes() {

        try {

            Query q;
            q = em.createQuery(
                    "SELECT DISTINCT p FROM WikipediaPage p, WikipediaTemplate t WHERE p=t.page");
            List<WikipediaPage> list = (List<WikipediaPage>) q.getResultList();
            for (WikipediaPage page : list)
                page.setPageLoader(loader);

            return list;

        } catch (javax.persistence.NoResultException e) {
            logger.error(e.getMessage());
        }

        return null;
    }


    public List<WikipediaPage> findAll() {

        try {

            Query q;
            q = em.createQuery(
                    "SELECT p FROM WikipediaPage p");
            List<WikipediaPage> list = (List<WikipediaPage>) q.getResultList();
            for (WikipediaPage page : list)
                page.setPageLoader(loader);

            return list;

        } catch (javax.persistence.NoResultException e) {
            logger.error(e.getMessage());
        }

        return null;
    }


    public void save(WikipediaPage page) {


        if (!isSessionActive())
            em.getTransaction().begin();

        try {

            logger.debug("Saving " + page.getTitle());


            if (page.getId() == null) {

                em.persist(page);

            } else {

                em.merge(page);
            }


            if (!isSessionActive()) {
                em.flush();
                em.getTransaction().commit();
            }


        } catch (Exception e) {
            logger.error(e.getMessage(), e);


            for (WikipediaTemplate a : page.getTemplates()) {
                logger.debug("====> Template: " + a.getName() + " --- " + a.getId());

                for (WikipediaTemplateAttributeAssociation attribute : a.getAttributes()) {

                    logger.debug("\t - " + attribute.toString() + " " + attribute.getId());

                }

            }

            if (!isSessionActive())
                em.getTransaction().rollback();
            else this.transaction.rollback();
        }


        WikipediaProcessedPageHistory history = WikipediaProcessedPageHistory.getInstance();
        history.addProcessedPage(page.getTitle(),
                page.hasOnlyInfoboxes() ? history.INFOBOXES_ONLY : page.containsTemplate("Infobox") ? history.INFOBOXES_AVAILABLE : history.INFOBOX_NOT_AVAILABLE);

        logger.info("Page ..." + page.getTitle() + " [" + page.getId() + "] saved");

    }


    public void delete(WikipediaPage page) {

        if (!isSessionActive())
            em.getTransaction().begin();

        try {

            em.remove(page);
            em.flush();

            if (!isSessionActive()) {
                em.getTransaction().commit();
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            if (!isSessionActive())
                em.getTransaction().rollback();
            else this.transaction.rollback();
        }
    }

    public WikipediaPage findLastProcessedPages() {

        try {


            Query q;

            q = em.createQuery(
                    "SELECT max(u.id) FROM WikipediaPage u");

            Long id = (Long) q.getSingleResult();
            if (id == null) {
                return null;
            } else {
                return this.findById(id);
            }

        } catch (javax.persistence.NoResultException e) {
            logger.error(e.getMessage());
        }

        return null;
    }


    public WikipediaCategory getPageCategory(String fullyQualifiedName) {
        WikipediaCategory result = null;
        WikipediaCategory parent = null;

        if (!isSessionActive())
            em.getTransaction().begin();

        try {

            if (fullyQualifiedName != null) {


                String[] categoryNames = fullyQualifiedName.split(":");
                for (String categoryName : categoryNames) {
                    //logger.info("Category: " + categoryName);
                    categoryName = categoryName.trim();
                    result = findPageCategory(categoryName);
                    //logger.info("Category: " + categoryName + " " + result + ", parent="+parent);
                    if (result == null) {
                        result = new WikipediaPageCategory(categoryName);
                        em.persist(result);
                        if (!isSessionActive())
                            em.flush();
                    }

                    if (parent != null) {
                        parent.addSubcategory(result);
                        result.addParentCategory(parent);
                        em.merge(parent);
                        em.merge(result);
                        if (!isSessionActive())
                            em.flush();
                    }
                    parent = result;
                }
            }


            if (!isSessionActive())
                em.getTransaction().commit();

        } catch (Exception ex) {
            ex.printStackTrace();
            if (!isSessionActive())
                em.getTransaction().rollback();
            else this.transaction.rollback();
        }

        return result;
    }


    public WikipediaCategory findPageCategory(String name) {
        WikipediaCategory category = null;

        try {

            Query q;
            q = em.createQuery(
                    "SELECT c FROM WikipediaPageCategory c WHERE c.name = :name");
            q.setParameter("name", name);
            category = (WikipediaCategory) q.getSingleResult();

        } catch (javax.persistence.NoResultException e) {
            logger.debug(e.getMessage());
        }


        return category;
    }


    public WikipediaEntityCategory getEntityCategory(String fullyQualifiedName) {

        WikipediaEntityCategory result = null;
        WikipediaEntityCategory parent = null;

        if (fullyQualifiedName != null) {

            fullyQualifiedName = fullyQualifiedName.toLowerCase();

            if (!isSessionActive())
                em.getTransaction().begin();

            try {

                if (fullyQualifiedName != null) {


                    String[] categoryNames = fullyQualifiedName.split(":");
                    for (String categoryName : categoryNames) {
                        //logger.info("Category: " + categoryName);
                        categoryName = categoryName.trim();
                        result = findEntityCategory(categoryName);
                        //logger.info("Category: " + categoryName + " " + result + ", parent="+parent);
                        if (result == null) {
                            result = new WikipediaEntityCategory(categoryName);
                            em.persist(result);
                            if (!isSessionActive())
                                em.flush();
                        }

                        if (parent != null) {
                            parent.addSubcategory(result);
                            result.addParentCategory(parent);
                            em.merge(parent);
                            em.merge(result);
                            if (!isSessionActive())
                                em.flush();
                        }
                        parent = result;
                    }
                }


                if (!isSessionActive())
                    em.getTransaction().commit();

            } catch (Exception ex) {
                ex.printStackTrace();
                if (!isSessionActive())
                    em.getTransaction().rollback();
                else this.transaction.rollback();
            }

        }
        return result;
    }


    public WikipediaEntityCategory findEntityCategory(String name) {
        WikipediaEntityCategory category = null;

        if (name != null) {

            name = name.toLowerCase();

            try {

                Query q;
                q = em.createQuery(
                        "SELECT c FROM WikipediaEntityCategory c WHERE c.name = :name");
                q.setParameter("name", name);
                category = (WikipediaEntityCategory) q.getSingleResult();

            } catch (javax.persistence.NoResultException e) {
                logger.debug(e.getMessage());
            }

        }

        return category;
    }
}
