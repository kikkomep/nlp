/*
 * Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
 * Mountain View, California, 94041, USA.
 */
package it.uniroma1.dis.nlp.woie.common.wikipedia.services.loader;

import it.uniroma1.dis.nlp.woie.App;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaPage;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaPageLoader;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaPageInputStreamNotAvailableException;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaPageNotFoundException;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author kikkomep
 */
public class WikipediaPageFileLoader implements WikipediaPageLoader {

    private final Logger logger;
    private final String filesPath;

    private static final String BASE_URL = "http://en.wikipedia.org/wiki/";

    public WikipediaPageFileLoader() {
        App app = App.getInstance();
        this.logger = Logger.getLogger(WikipediaPageFileLoader.class.
                getCanonicalName());
        this.filesPath = (String) app.getConfigurationProperties().get(
                "wiki_pages_folder");
        if (this.filesPath == null) {
            logger.error(
                    "wikiPagesFolder parameter must to be setted in the configuration file");
            System.exit(0);
        }
        File f = new File(this.filesPath);
        if (!f.exists()) {
            logger.error(
                    "wikiPagesFolder " + this.filesPath + " doesn't exists!");
            System.exit(0);
        }
    }

    @Override
    public List<WikipediaPage> loadPages(int numberOfPages) {

        List<WikipediaPage> list = new LinkedList<>();
        processPath(new File(this.filesPath), list, numberOfPages, null);
        return list;
    }

    @Override
    public List<WikipediaPage> loadPages(int numberOfPages, int skipFirstPages, boolean printSkips) {
        List<WikipediaPage> result = new ArrayList<>();
        List<WikipediaPage> list = loadPages(numberOfPages + skipFirstPages);
        logger.info("Pages to load: " + numberOfPages + ", to skip: " + skipFirstPages);
        int count = 0;
        for (WikipediaPage p : list) {

            count++;

            if (count > skipFirstPages) {
                result.add(p);
            }else{
                if(printSkips)
                    logger.info("\t - Skipped page [" + count + "]: " + p.getTitle());
            }
        }

        return result;
    }

    @Override
    public List<WikipediaPage> loadPages(int numberOfPages, int skipFirstPages) {

        List<WikipediaPage> result = new ArrayList<>();
        List<WikipediaPage> list = loadPages(numberOfPages + skipFirstPages);
        int count = 0;
        for (WikipediaPage p : list) {
            count++;
            if (count > skipFirstPages) {
                result.add(p);
            }
        }

        return result;
    }


    private boolean processPath(File f, List<WikipediaPage> list, int limit, String startPage) {

        String st = startPage;

        if (list.size() < limit) {

            if (st != null && f.isFile() && f.getName().replace(".txt", "").startsWith(st))
                return true;

            if (f.isFile() && f.getName().endsWith(".txt")) {
                String pageTitle = f.getName().replace(".txt", "");

                WikipediaPage page = new WikipediaPage(pageTitle, buildUrl(pageTitle), f.lastModified());
                page.setLocalurl(f.getAbsolutePath());
                page.setPageLoader(this);
                list.add(page);

            } else if (f.isDirectory()) {
                for (File child : f.listFiles()) {
                    if (processPath(child, list, limit, st))
                        st = null;
                    if (list.size() == limit) {
                        break;
                    }
                }
            }
        }

        return false;
    }

    @Override
    public WikipediaPage loadByTitle(String pageTitle) throws WikipediaPageNotFoundException {

        File file = null;

        logger.debug("Searching the page: " + pageTitle);
        file = findFile(new File(this.filesPath), pageTitle);
        if (file == null) {
            throw new WikipediaPageNotFoundException();
        }



        WikipediaPage page = new WikipediaPage(pageTitle, buildUrl(pageTitle), file.
                lastModified());
        page.setPageLoader(this);
        page.setLocalurl(file.getAbsolutePath());

        return page;
    }

    @Override
    public InputStream newInputStream(WikipediaPage page) throws WikipediaPageInputStreamNotAvailableException {
        try {

            if (page == null) {
                throw new WikipediaPageInputStreamNotAvailableException();
            }

            return new FileInputStream(new File(page.getLocalurl()));

        } catch (FileNotFoundException ex) {
            throw new WikipediaPageInputStreamNotAvailableException(ex.
                    getMessage(), ex);
        }
    }

    private File findFile(File file, String pageTitle) {

        File result = null;

        if(pageTitle!=null) pageTitle = pageTitle.replaceAll(" ", "_");

        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                result = (findFile(f, pageTitle));
                if (result != null) {
                    return result;
                }
            }
        }

        if (file.isFile() && file.getName().startsWith(pageTitle) && file.
                getName().endsWith(".txt")) {
            result = file;
            logger.debug("File found @ " + file.getAbsolutePath());
        }

        return result;
    }



    private static String buildUrl(String pageTitle){
        return BASE_URL + pageTitle;
    }
}
