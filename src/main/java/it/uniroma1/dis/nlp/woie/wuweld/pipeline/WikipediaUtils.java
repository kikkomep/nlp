package it.uniroma1.dis.nlp.woie.wuweld.pipeline;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;
import it.uniroma1.dis.nlp.woie.common.wikipedia.services.synonym.WikipediaSynonymsRepository;

import java.util.*;
import java.util.regex.Pattern;

/**
 *
 * @author lgu
 */
class WikipediaUtils {

    protected static String primaryEntityTypeRecognition(ParsedSentence sentence) throws ParseException {

        Annotation document = sentence.getAnnotation();
        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
        IndexedWord d = null;
        for (CoreMap sentenceCoreMap : sentences) {

            // this is the Stanford dependency graph of the current sentenceCoreMap
            SemanticGraph dependencies = sentenceCoreMap.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);

            try{
                d = dependencies.getFirstRoot();
                if(d.tag().contains("NN"))
                    return d.word();
            }catch(RuntimeException e){
                continue;
            }
        }

        throw new ParseException("Type cannot be found!");
    }

    protected static Pattern createSynonymsRegEx(String n) {

        WikipediaSynonymsRepository sf = WikipediaSynonymsRepository.getInstance();

        List<String> syn = new ArrayList<>(sf.findSynonyms(n));

        Collections.sort(syn, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.length() - o1.length();
            }
        });

        StringBuilder regex = new StringBuilder();
        regex.append("(?i)");
        Iterator<String> iterator = syn.iterator();

        while (iterator.hasNext()) {
            String tba = cleanString(iterator.next());
            if (tba.length() == 0) {
                continue;
            }
            String a = "\\Q" + tba + "\\E";
            regex.append(a);
            regex.append("|");
        }
        regex.append("\\Q").append(n).append("\\E");
        regex.append("|").append("\\Q").append(n.replaceAll("_", " ")).append("\\E"); // Full title matching without underscore
        return Pattern.compile(regex.toString());
    }

    protected static String cleanString(String string){
        String result = string.replaceAll("\\(|\\)|\\{|\\}|\\[|\\]","");

        return result;
    }

    protected static Pattern partialMatchingRegEx(String text) {

        String[] tokens = cleanString(text).split(" |_");

        if (tokens.length == 1) {
            return Pattern.compile("(?i)" + tokens[0]);
        }

        StringBuilder regEx = new StringBuilder();

        regEx.append("(");

        for (int i = tokens.length - 1; i > 0; i--) {

            for (int j = i; j <= tokens.length - 1; j++) {
                regEx.append(tokens[j]);
                if (j < tokens.length - 1) {
                    regEx.append(" ");
                }
            }

            if (i > 1) {
                regEx.append("|");
            }
        }

        regEx.append("|");

        for (int i = tokens.length - 2; i >= 0; i--) {

            for (int j = 0; j <= i; j++) {
                regEx.append(tokens[j]);
                if (j < i) {
                    regEx.append(" ");
                }
            }

            if (i > 0) {
                regEx.append("|");
            }
        }

        regEx.append(")");

        return Pattern.compile("(?i)" + regEx.toString());
    }



    protected static String createSynRegex(String e) {
        WikipediaSynonymsRepository sf = WikipediaSynonymsRepository.getInstance();

        List<String> syn = new ArrayList<>(sf.findSynonyms(e));
        syn.add(e);

        Collections.sort(syn, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.length() - o1.length();
            }
        });

        StringBuilder regex = new StringBuilder();
        Iterator<String> iterator = syn.iterator();

        while (iterator.hasNext()) {
            String tba = iterator.next();
            if (tba!=null && tba.length() == 0) {
                continue;
            }

            if(tba!=null && tba.split(" ").length>1){
                continue;
            }

            regex.append(tba.toLowerCase());

            if(iterator.hasNext())regex.append("|");
        }

        return regex.toString();

    }

//    protected static List<IndexedWord> getPrepositionalObject(SemanticGraph sg, IndexedWord principal,boolean includeModifier){
//
//        LinkedList<IndexedWord> relation = new LinkedList<>();
//        List<IndexedWord> prepositionalModifiers = sg.getChildrenWithReln(principal, EnglishGrammaticalRelations.PREPOSITIONAL_MODIFIER);
//
//        for(IndexedWord word:prepositionalModifiers){
//            relation.addAll(sg.getChildrenWithReln(word,EnglishGrammaticalRelations.PREPOSITIONAL_OBJECT));
//        }
//
//        if(includeModifier)
//            relation.addAll(prepositionalModifiers);
//
//        return relation;
//    }
//
//    protected static String expand(IndexedWord principal, SemanticGraph sg) {
//
//        RelationModifiersSelector selector = RelationModifiersSelector.getInstance();
//        List<IndexedWord> relation = sg.getChildrenWithRelns(principal, selector.getRelationsToBeIncludedForNoun());
//        relation.add(principal);
//
//        List<IndexedWord> prepositionalObjects = getPrepositionalObject(sg, principal,false);
//
//        relation.addAll(getPrepositionalObject(sg, principal,true));
//
//        for(IndexedWord prepositionalObject:prepositionalObjects){
//            List<IndexedWord> temp =getPrepositionalObject(sg, prepositionalObject, true);
//            relation.addAll(temp);
//            for(IndexedWord word : temp){
//                relation.addAll(sg.getChildrenWithRelns(word, selector.getRelationsToBeIncludedForNoun()));
//            }
//        }
//
//        Collections.sort(relation, new Comparator<IndexedWord>() {
//            @Override
//            public int compare(IndexedWord o1, IndexedWord o2) {
//                return o1.index() - o2.index();
//            }
//        });
//
//        StringBuilder sb = new StringBuilder();
//        for (IndexedWord w : relation) {
//            sb.append(w.word());
//            sb.append(" ");
//        }
//
//        return sb.toString();
//    }
}
