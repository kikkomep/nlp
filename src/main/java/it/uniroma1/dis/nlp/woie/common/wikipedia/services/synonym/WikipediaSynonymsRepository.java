package it.uniroma1.dis.nlp.woie.common.wikipedia.services.synonym;

import it.uniroma1.dis.nlp.woie.App;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaWord;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by kikkomep on 12/27/13.
 */
public class WikipediaSynonymsRepository {
    private static WikipediaSynonymsRepository ourInstance = new WikipediaSynonymsRepository();

    private Logger logger = Logger.getLogger(WikipediaSynonymsRepository.class);

    public static WikipediaSynonymsRepository getInstance() {
        return ourInstance;
    }

    private WikipediaWordRepository repository = WikipediaWordRepository.getInstance();

    private Object[] finders;


    private WikipediaSynonymsRepository() {
        this.finders = App.getInstance().getConfiguredObjectsByPrefix("syn_finder");
    }


    public WikipediaSynonymsRepository(WikipediaWordRepository repository) {
        this.repository = repository;
    }

    public Set<String> findSynonyms(String word) {
        logger.debug("::::::::::::::::::::::::::::::::::::::::::::::::::\nSearching for syn of " + word);
        return findSynonyms(word, false);
    }

    public Set<String> findSynonyms(String word, boolean forceUpdate) {
        HashSet<String> set = new HashSet<>();
        if (word != null) {
            word = word.trim();

            WikipediaWord iw = repository.findByText(word);

            logger.debug("---> " + forceUpdate + " " + iw + " " + word);

            if(iw==null){
                logger.debug("Word not found: I'm creating it...");

                iw = new WikipediaWord(word);
                repository.save(iw);

            }else{
                logger.debug("Word found!!!");
            }

            Set<WikipediaWord> syns = findWordSynonyms(iw, forceUpdate);
            for(WikipediaWord syn : syns)
                set.add(syn.getText());
        }

        return set;
    }


    public Set<WikipediaWord> findWordSynonyms(WikipediaWord word) {
        return findWordSynonyms(word, false);
    }

    public Set<WikipediaWord> findWordSynonyms(WikipediaWord word, boolean forceUpdate) {


        Set<WikipediaWord> set = new HashSet<>();

        if (word != null) {


            if(word.getId()!=null && forceUpdate){
                repository.delete(word);
                word.setId(null);
            }

            if (!word.isSynonymsLoaded() || forceUpdate) {

                String wordText = word.getText();

                HashSet<String> list = new HashSet<>();

                for(Object objFinder : this.finders){
                    SynonymsFinder finder = (SynonymsFinder) objFinder;
                    logger.info("Searching synonym for '" + wordText + "' by means of " + finder.getClass().getSimpleName() + " ! ");
                    list.addAll(finder.findSynonyms(wordText));
                }



                logger.debug("Found Synonyms: " + list.size());

                for (String synText : list) {

                    if(!synText.equals(wordText)){
                        synText = synText.trim();
                        WikipediaWord syn = repository.findByText(synText);
                        if (syn == null) {
                            syn = new WikipediaWord(synText);
                            repository.save(syn);
                        }

                        if (!synText.equals(wordText))
                            set.add(syn);
                    }
                }

                word = repository.findByText(wordText);
                if (word == null)
                    word = new WikipediaWord(wordText);

                for (WikipediaWord syn : set)
                    word.addSynonym(syn);

                word.setSynonymsLoaded(true);

                logger.debug("Saving the word: " + word.getText() + word.getId());
                repository.save(word);
                logger.debug("::::::::::::::::::::::::::::::::::::::::::::::::::");
            }


            set.addAll(word.getSynonyms());
        }

        return set;
    }
}
