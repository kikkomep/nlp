package it.uniroma1.dis.nlp.woie.common.wikipedia;

import javax.persistence.Entity;

/**
 * Created by kikkomep on 12/20/13.
 */
@Entity
public class WikipediaTemplateValueAttribute extends WikipediaTemplateAttribute {


    public WikipediaTemplateValueAttribute() {
    }

    public WikipediaTemplateValueAttribute(String value) {
        super(value);
    }

    public WikipediaTemplateValueAttribute(String value, String rawValue) {
        super(value, rawValue);
    }
}
