package it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser;

import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaTemplate;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaTemplateAttributeAssociation;

/**
 * Created by kikkomep on 12/19/13.
 */
public interface WikipediaTemplateProcessor {

    public void startTemplate(WikipediaTemplate template);

    public void endTemplate(WikipediaTemplate template);


    public void addAttribute(WikipediaTemplateAttributeAssociation attribute);

    public void addTemplateAttribute(WikipediaTemplateAttributeAssociation attribute);

}
