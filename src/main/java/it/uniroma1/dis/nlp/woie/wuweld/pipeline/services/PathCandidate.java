package it.uniroma1.dis.nlp.woie.wuweld.pipeline.services;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;

/**
 * Created by lgu on 03/02/14.
 */
public class PathCandidate {

    private SemanticGraphEdge first;
    private SemanticGraphEdge second;
    private boolean hasSecond;

    public PathCandidate(SemanticGraphEdge first, SemanticGraphEdge second) {
        this.first = first;
        this.second = second;
        this.hasSecond = true;
    }

    public PathCandidate(SemanticGraphEdge first) {
        this.first = first;
        this.hasSecond = false;
    }

    public SemanticGraphEdge getFirst() {
        return first;
    }

    public SemanticGraphEdge getSecond() {
        return second;
    }

    public boolean hasSecond() {
        return hasSecond;
    }

    public IndexedWord getSubj() {
        return first.getDependent();
    }

    public IndexedWord getObj() {
        if(hasSecond)
            return second.getDependent();
        else return first.getGovernor();
    }

    public IndexedWord getRelation() {
        return first.getGovernor();
    }
}
