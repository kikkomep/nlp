package it.uniroma1.dis.nlp.woie.wuweld.pipeline;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import edu.stanford.nlp.trees.GrammaticalRelation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author lgu
 */
public class Match {

    private final Logger logger = LoggerFactory.getLogger(Match.class.getName());
    private Entity primaryEntity;
    private Entity attributeValue;
    private List<SemanticGraphEdge> shortestPath;
    private IndexedWord subject, object;
    private String attribute;
    private String infoboxType;

    public Match(Entity primaryEntity, Entity attributeValue, String infoboxType) {
        this.primaryEntity = primaryEntity;
        this.attributeValue = attributeValue;
        this.infoboxType = infoboxType;
    }

    public void setAttributeName(String attribute) {
        this.attribute = attribute;
    }

    public String getAttributeName() {
        return attribute;
    }

    public Entity getPrimaryEntity() {
        return primaryEntity;
    }

    public String getInfoboxType() {
        return infoboxType;
    }

    public Entity getAttributeValue() {
        return attributeValue;
    }

    public void setShortestPath(List<SemanticGraphEdge> shortestPath) {
        this.shortestPath = shortestPath;
    }

    public void setSubject(IndexedWord subject) {
        this.subject = subject;
    }

    public void setObject(IndexedWord object) {
        this.object = object;
    }

    public List<SemanticGraphEdge> getShortestPath() {
        return shortestPath;
    }

    public IndexedWord getSubject() {
        return subject;
    }

    public IndexedWord getObject() {
        return object;
    }

    public IndexedWord getCommonGovernor() throws ParseException {
        return calculateCommonGovernor();
    }

    /**
     *
     * @return a string representing the generalized-corePath for this matching
     */
    public String getGeneralizedCorePath() {
        return getCorePath(true);
    }

    private String getCorePath(boolean generalize) {

        if (this.shortestPath == null) {
            logger.debug("No generalized core Path");
            return null;
        }

        StringBuilder sb = new StringBuilder();
        Generalizator gen = Generalizator.getInstance();

        logger.debug("Path for Matching: " + getSubject().word()+ " - " + getObject().word());
        Iterator<SemanticGraphEdge> iterator = this.shortestPath.iterator();
        while(iterator.hasNext()) {
            SemanticGraphEdge e = iterator.next();
            if(generalize)
                sb.append(createStringGeneralizedRelation(e));
            else sb.append(createStringRelation(e));

            if(iterator.hasNext())sb.append("#");
        }
        return sb.toString();
    }

    private IndexedWord calculateCommonGovernor() throws ParseException {
        Iterator<SemanticGraphEdge> iterator = this.shortestPath.iterator();
        IndexedWord gov = this.shortestPath.get(0).getGovernor();
        while(iterator.hasNext()){
            if(!gov.equals(iterator.next().getGovernor())){
                throw new ParseException("No common governor!");
            }
        }
        return gov;
    }

    public String getAttributeCorePath() throws ParseException {

        if (this.shortestPath == null) {
            throw new ParseException("Shortest path null !");
        }

        if (this.shortestPath.size()==0) {
            throw new ParseException("Shortest path null !");
        }

        IndexedWord gov = calculateCommonGovernor();
        Generalizator gen = Generalizator.getInstance();

        if(!gen.transfoPOS(gov.tag()).equals(Generalizator.VERB)){
            throw new ParseException("No common governor!");
        }

        return getCorePath(false);
    }

    public static String createStringGeneralizedRelation(SemanticGraphEdge e) {
        StringBuilder sb = new StringBuilder();
        Generalizator gen = Generalizator.getInstance();

        sb.append((gen.transformRelation(e.getRelation())));
        sb.append("(");
        sb.append(gen.transfoPOS(e.getGovernor().tag()));
        sb.append(",");
        sb.append(gen.transfoPOS(e.getDependent().tag()));
        sb.append(")");

        return sb.toString();
    }


    public static String createStringRelation(SemanticGraphEdge e) {
        StringBuilder sb = new StringBuilder();
        Generalizator gen = Generalizator.getInstance();

        sb.append(((e.getRelation())));
        sb.append("(");
        sb.append(gen.transfoPOS(e.getGovernor().tag()));
        sb.append(",");
        sb.append(gen.transfoPOS(e.getDependent().tag()));
        sb.append(")");

        return sb.toString();
    }

    public static String createStringGeneralizedRelation(GrammaticalRelation g, String tagGovernor, String tagDependent) {
        StringBuilder sb = new StringBuilder();
        Generalizator gen = Generalizator.getInstance();

        sb.append((gen.transformRelation(g)));
        sb.append("(");
        sb.append(gen.transfoPOS(tagGovernor));
        sb.append(",");
        sb.append(gen.transfoPOS(tagDependent));
        sb.append(")");

        return sb.toString();

    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        if (subject == null || object == null) {
            List<IndexedWord> terms = primaryEntity.getWords();
            for (IndexedWord iw : terms) {
                sb.append(iw.lemma());
                sb.append(" ");
            }
            sb.append(" ---- ");
            terms = attributeValue.getWords();
            for (IndexedWord iw : terms) {
                sb.append(iw.lemma());
                sb.append(" ");
            }
        } else {
            sb.append("subj: ");
            sb.append(subject.lemma());
            sb.append(" ---- obj: ");
            sb.append(object.lemma());
        }

        sb.append(" [attributeName: " + this.getAttributeName() + "]");

        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.subject);
        hash = 53 * hash + Objects.hashCode(this.object);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Match other = (Match) obj;
        if (!Objects.equals(this.subject, other.subject)) {
            return false;
        }
        if (!Objects.equals(this.object, other.object)) {
            return false;
        }
        return true;
    }
}
