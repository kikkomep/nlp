/*
 * Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
 * Mountain View, California, 94041, USA.
 */
package it.uniroma1.dis.nlp.woie.common.wikipedia;

import com.sun.istack.internal.Nullable;

import it.uniroma1.dis.nlp.woie.common.wikipedia.services.synonym.WikipediaSynonymsRepository;
import it.uniroma1.dis.nlp.woie.common.wikipedia.utils.WikipediaUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.*;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author kikkomep
 */
@Entity
public class WikipediaPage extends WikipediaEntity {

    private static final long serialVersionUID = 1L;


    @Nullable
    private String localurl;

    @NotNull
    private String url;

    @NotNull
    @Column(unique = true)
    private String title;

    @NotNull
    private Long lastModified = 0L;

    @OneToMany(cascade = CascadeType.ALL)
    private List<WikipediaTemplate> templates = new LinkedList<>();

    @Transient
    private Logger logger = Logger.getLogger(WikipediaPage.class.getSimpleName());

    @Transient
    private Set<String> subjectSynonyms = null;

    @Transient
    private String templateFilteredRawText;

    @Lob
    private byte[] compressedFilteredRawText;

    @Transient
    private WikipediaPageLoader pageLoader;

    @ManyToMany
    @JoinTable(name = "WikipediaPage_Categories")
    private Set<WikipediaCategory> categories = new HashSet<>();

    protected WikipediaPage() {
    }

    public WikipediaPage(String title) {
        this.title = title;
    }



    public WikipediaPage(String title, String url, Long lastModified) {
        this.title = title;
        this.url = url;
        this.lastModified = lastModified;
    }

    public String getLocalurl() {
        return localurl;
    }

    public void setLocalurl(String localurl) {
        this.localurl = localurl;
    }

    public WikipediaPageLoader getPageLoader() {
        return pageLoader;
    }

    public void setPageLoader(WikipediaPageLoader pageLoader) {
        this.pageLoader = pageLoader;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    protected void setTitle(String title) {
        this.title = title;
    }

    public Long getLastModified() {
        return lastModified;
    }

    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }

    public String getTemplateFilteredRawText() {
        if (this.compressedFilteredRawText != null && this.templateFilteredRawText == null)
            this.templateFilteredRawText = WikipediaUtils.decompress(this.compressedFilteredRawText);
        return templateFilteredRawText;
    }

    public void setTemplateFilteredRawText(String templateFilteredRawText) {
        this.templateFilteredRawText = templateFilteredRawText;
        this.compressedFilteredRawText = WikipediaUtils.compressText(templateFilteredRawText);
    }

    public String getRawText() throws WikipediaPageInputStreamNotAvailableException, IOException {
        StringBuilder builder = new StringBuilder();

        BufferedReader reader = new BufferedReader(new InputStreamReader(this.getInputStream()));
        while (reader.ready()) {
            builder.append(reader.readLine());//.append("\n");
        }

        reader.close();

        return builder.toString();
    }

    public String getTextAsHtml() throws WikipediaPageInputStreamNotAvailableException, IOException {
        return WikipediaUtils.toHtml(this.getTemplateFilteredRawText());
    }

    public String getPlainText() throws WikipediaPageInputStreamNotAvailableException, IOException {
        return WikipediaUtils.toPlainText(this.getTemplateFilteredRawText());
    }

    public Set<String> getSynonyms() {
        if (subjectSynonyms == null) {
            WikipediaSynonymsRepository sf = WikipediaSynonymsRepository.getInstance();

            subjectSynonyms = sf.findSynonyms(this.getTitle());

        }
        return subjectSynonyms;
    }

    public InputStream getInputStream() throws WikipediaPageInputStreamNotAvailableException {
        if (this.getPageLoader() == null)
            throw new WikipediaPageInputStreamNotAvailableException("No PageLoader for this page !!!");

        return this.pageLoader.newInputStream(this);
    }

    public void addTemplate(WikipediaTemplate template) {
        if (template != null) {
            this.templates.add(template);
            //WikipediaTemplateAttributeAssociation as = new WikipediaTemplateAttributeAssociation("", template);
            //as.setParent(this);
            template.setPage(this);
        }
    }

    public void removeTemplate(WikipediaTemplate template) {
        if (template != null) {// && template.getPage() == this) {
            this.templates.remove(template);
            template.setPage(null);
        }
    }

    public void removeTemplates() {
        for (WikipediaTemplate t : this.getTemplates()) {
            t.setParent(null);
        }

        this.templates.clear();
    }

    public List<WikipediaTemplate> getTemplates() {
        return this.getTemplates(false);
    }

    public List<WikipediaTemplate> getTemplates(boolean includeNested) {
        if (includeNested)
            return this.templates;
        else {

            List<WikipediaTemplate> list = new ArrayList<>();
            for (WikipediaTemplate t : this.templates) {
                if (!t.isNested())
                    list.add(t);
            }
            return list;
        }
    }

    public List<WikipediaTemplate> getTemplates(String name) {
        List<WikipediaTemplate> list = new ArrayList<>();

        Pattern pattern = Pattern.compile(name, Pattern.CASE_INSENSITIVE);

        for (WikipediaTemplate t : this.templates) {
            Matcher matcher = pattern.matcher(t.getName());
            if (matcher.find())
                list.add(t);
        }

        return list;
    }

    public boolean containsTemplate(String name) {
        boolean result = false;

        Pattern pattern = Pattern.compile(name, Pattern.CASE_INSENSITIVE);

        for (WikipediaTemplate t : this.templates) {

            if (t.getName() != null) {
                Matcher matcher = pattern.matcher(t.getName());
                if (matcher.find()) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    public int countTemplates(String name) {
        int count = 0;

        Pattern pattern = Pattern.compile(name, Pattern.CASE_INSENSITIVE);

        for (WikipediaTemplate t : this.templates) {

            if (t.getName() != null) {
                Matcher matcher = pattern.matcher(t.getName());
                if (matcher.find()) {
                    if (name.equals(t.getName())) {
                        count++;
                    }
                }
            }
        }

        return count;
    }

    public boolean hasOnlyInfoboxes() {
        boolean result = true;

        Pattern pattern = Pattern.compile("INFOBOX", Pattern.CASE_INSENSITIVE);

        for (WikipediaTemplate t : this.templates) {

            if (t.getName() != null) {
                Matcher matcher = pattern.matcher(t.getName());
                if (!matcher.find()) {
                    result = false;
                    break;
                }
            }
        }

        return result;
    }

    public int numberOfMainTemplates() {
        return this.templates.size();
    }

    public int numberOfTotalTemplates() {
        int count = 0;
        for (WikipediaTemplate t : this.getTemplates()) {
            count += countNestedTemplates(t);
        }
        return count;
    }

    private int countNestedTemplates(WikipediaTemplate t) {
        int count = 1;

        for (WikipediaTemplateAttributeAssociation n : t.getAttributes()) {
            if (n.getAttribute() instanceof WikipediaTemplate)
                count += countNestedTemplates((WikipediaTemplate) n.getAttribute());

        }
        return count;
    }


    public int numberOfTotalAttributes() {
        int count = 0;
        for (WikipediaTemplate t : this.getTemplates()) {
            count += t.numberOfAttributes();
        }
        return count;
    }


    public void addCategory(WikipediaCategory category) {
        if (category != null && !this.categories.contains(category)) {
            this.categories.add(category);
            category.addWikipediaPage(this);
        }
    }

    public void removeCategory(WikipediaCategory category) {
        if (category != null) {
            this.categories.remove(category);
            category.removeWikipediaPage(this);
        }
    }

    public Set<WikipediaCategory> getCategories() {
        return Collections.unmodifiableSet(this.categories);
    }

    public Set<WikipediaEntityCategory> getEntityCategories() {
        Set<WikipediaEntityCategory> set = new HashSet<>();
        for (WikipediaCategory category : getCategories()) {
            if (category instanceof WikipediaEntityCategory)
                set.add((WikipediaEntityCategory) category);
        }

        return set;
    }

    void reset(){

        Iterator<WikipediaTemplate> wikipediaTemplateIterator = (new ArrayList<>(this.templates)).iterator();
        while(wikipediaTemplateIterator.hasNext())
            this.removeTemplate(wikipediaTemplateIterator.next());

        Iterator<WikipediaCategory> wikipediaCategoryIterator = (new ArrayList<>(this.categories)).iterator();
        while(wikipediaCategoryIterator.hasNext())
            this.removeCategory(wikipediaCategoryIterator.next());
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (url != null ? url.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the url fields are not set
        if (!(object instanceof WikipediaPage)) {
            return false;
        }
        WikipediaPage other = (WikipediaPage) object;
        if ((this.url == null && other.url != null) || (this.url != null && !this.url.
                equals(other.url))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.uniroma1.dis.nlp.woie.common.wikipedia.model.WikipediaPage[ url=" + url + " ]";
    }
}
