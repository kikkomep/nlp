package it.uniroma1.dis.nlp.woie.common.wikipedia;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kikkomep on 12/19/13.
 */
@Entity
public class WikipediaTemplateComplexAttribute extends WikipediaTemplateValueAttribute {

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name="WikipediaTemplateComplexAttribute_Values")
    protected List<WikipediaTemplateLiteralAttribute> values = new ArrayList<>();

    public WikipediaTemplateComplexAttribute() {
    }

    public WikipediaTemplateComplexAttribute(List<WikipediaTemplateLiteralAttribute> values) {
        if (values != null) {
            for (WikipediaTemplateLiteralAttribute v : values) {
                this.addAttribute(v);
            }
        }
    }



    public void addAttribute(WikipediaTemplateLiteralAttribute attribute) {
        if (attribute != null && attribute.getParentAssociation() == null) {
            attribute.setParent(this.getParentAssociation());
            values.add(attribute);
        }
    }

    public void removeAttribute(WikipediaTemplateLiteralAttribute attribute) {
        if (attribute != null && attribute.getParentAssociation() == this.getParentAssociation()) {
            attribute.setParent(null);
            values.remove(attribute);
        }
    }

    public List<WikipediaTemplateLiteralAttribute> getAttributes() {
        return this.values;
    }


    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append(this.getClass().getSimpleName() + " {");
        for (WikipediaTemplateLiteralAttribute l : this.getAttributes())
            b.append("\n\t - " + l.toString());
        b.append("\n RawValue: " + this.rawValue);

        b.append("\n}");
        return b.toString();
    }
}
