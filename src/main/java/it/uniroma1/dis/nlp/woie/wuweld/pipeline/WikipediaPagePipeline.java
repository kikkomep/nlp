package it.uniroma1.dis.nlp.woie.wuweld.pipeline;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.util.StringUtils;
import it.uniroma1.dis.nlp.woie.common.wikipedia.*;
import it.uniroma1.dis.nlp.woie.common.wikipedia.utils.PronounCounter;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.services.ClauseRelationDisambiguation;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.filters.FirstFilter;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.filters.SecondFilter;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.filters.ThirdFilter;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaCategoryAttributeCorePath;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author lgu
 */
public class WikipediaPagePipeline extends Pipeline {

    protected String mostFrequentPronoun, title, primaryEntityType;
    protected WikipediaPage page;
    protected boolean primaryEntityTypeMatched = false;
    private HashMap<String, PathTypedDependency> result;
    private HashMap<AttributePattern, List<WikipediaCategoryAttributeCorePath>> attributeFiltering;
    private HashMap<AttributePattern, ParsedSentence> attributeApp;
    private boolean autoSave = true;


    public WikipediaPagePipeline(WikipediaPage page) throws IOException, WikipediaPageInputStreamNotAvailableException, ParseException {

        super(page.getPlainText());
        attributeApp = new HashMap<>();
        this.page = page;
        this.title = page.getTitle();
        result = new HashMap<>();
        attributeFiltering = new HashMap<>();

    }


    @Override
    public void process() {

        for (WikipediaTemplate template : page.getTemplates()) {
            if (template.getName().startsWith("Infobox")) {

                //Create an attribute patterns for each infobox attribute value
                createAttributePatterns(template);
                logger.info("AttributeTemplates Created!");

                //Initialize the pipeline and filter sentences: maintaining sentences that contain the title or an attribute value
                initializePipeline(true);
                logger.info("Pipeline Initialized!");

                //create the dependencyPath for finding title words and its synonyms
                Pattern titleSynonymsPattern = WikipediaUtils.createSynonymsRegEx(title);

                //Matching Phase for each sentence checking if matches the criteria
                for (int i = 0; i < parsedSentences.length; i++) {

                    //Matching of the primary entities
                    boolean result = primaryEntitiesMatching(parsedSentences[i], titleSynonymsPattern);
                    logger.debug("Result of primary entity matching: " + result + "\n");

                    //Matching Attributes
                    if (result) {
                        this.filterings(parsedSentences[i]);
                    }
                }

                logger.info("Sentences Matched!");

                //Save GeneralizedCorePath and WikipediaCategoryAttributeCorePath found
                if (autoSave) {
                    saveResult();
                    logger.info("Paths Saved!");
                }
            }
        }
    }

    protected void initializePipeline(boolean matchAttributes) {

        //split the text into sentences
        String[] sentences = super.splitIntoSentences(text);
        logger.info("Text Splitted!");

        ArrayList<ParsedSentence> sentenceslist = new ArrayList<>();
        int nsentence = 0;
        Pattern titlePattern = WikipediaUtils.createSynonymsRegEx(page.getTitle());
        logger.debug("\nTitle pattern : " + titlePattern.pattern() + "\n");

        //adding the annotation (dependency tree and POS) to each sentence
        for (String s : sentences) {

            boolean matched = false;

            if (titlePattern.matcher(s).find()) {

                matched = true;

            } else if (matchAttributes) {

                for (AttributePattern element : attributeApp.keySet()) {

                    if (element.getPattern().matcher(s).find()) {

                        matched = true;
                        break;

                    }

                }

            }

            if (matched) {

                //annotate sentence
                Annotation document = new Annotation(s);
                stanfordCorePipeline.annotate(document);
                nsentence++;
                sentenceslist.add(new ParsedSentence(document, s));
                logger.debug("Sentence " + nsentence + " annotated : " + s);
            }
        }

        logger.info("Sentences Annotated!");

        parsedSentences = sentenceslist.toArray(new ParsedSentence[nsentence]);

        logger.debug("\nTitle or attribute values appear in " + nsentence + " sentences\n");

        //recognize the most frequent pronoun
        this.mostFrequentPronoun = PronounCounter.pronounCount(text);
        logger.info("Pronoun Counted!");

        //recognize the type of the primary Entity
        try {

            if (sentences.length > 0) {

                Annotation document = new Annotation(sentences[0]);
                stanfordCorePipeline.annotate(document);
                ParsedSentence firstSentence = new ParsedSentence(document, sentences[0]);

                this.primaryEntityType = WikipediaUtils.primaryEntityTypeRecognition(firstSentence);
                this.primaryEntityTypeMatched = true;

            } else {
                primaryEntityTypeMatched = false;
            }

        } catch (it.uniroma1.dis.nlp.woie.wuweld.pipeline.ParseException ex) {
            //Logger.getLogger(WikipediaPagePipeline.class.getName()).log(Level.SEVERE, null, ex);
            primaryEntityTypeMatched = false;
        }

        logger.info("Primary Entity Recognized: " + primaryEntityTypeMatched + (primaryEntityTypeMatched ? " !!!  (type: " + primaryEntityType + ")" : ""));
    }

    private void createAttributePatterns(WikipediaTemplate template) {

        //get the attribute list of the page
        List<WikipediaTemplateAttributeAssociation> attributesList = (List<WikipediaTemplateAttributeAssociation>) template.getAttributes();
        Iterator<WikipediaTemplateAttributeAssociation> attributesItearator = attributesList.iterator();


        //associating to each attribute value a regex that matches the value and its synonyms
        while (attributesItearator.hasNext()) {

            WikipediaTemplateAttributeAssociation attributeAssociation = attributesItearator.next();
            WikipediaTemplateAttribute wta = attributeAssociation.getAttribute();

            List<String> a = it.uniroma1.dis.nlp.woie.common.wikipedia.utils.WikipediaUtils.getLiteralAttributeValues(wta);

            for (String s : a) {
                if (s != null) {
                    logger.debug("Attribute Name: " + attributeAssociation.getName() + " [path: " + s + "]");
                    attributeApp.put(new AttributePattern(WikipediaUtils.createSynonymsRegEx(s), s, attributeAssociation.getName(),
                            it.uniroma1.dis.nlp.woie.common.wikipedia.utils.WikipediaUtils.extractInfoboxTypeName(template.getName())), null);
                }
            }
        }
    }

    public void setAutoSave(boolean autoSave) {
        this.autoSave = autoSave;
    }

    private void saveResult() {

        //add recognized category
        logger.debug("TYPE MATCHING (saving): " + primaryEntityTypeMatched + " " + primaryEntityType);
        if (primaryEntityTypeMatched) {
            logger.debug("Adding primaryEntity Category (first sentence cryterion): " + primaryEntityTypeMatched + " - " + primaryEntityType);
            page.addCategory(Wikipedia.getInstance().getEntityCategory(primaryEntityType));
        }

        logger.debug("Type Matching: " + primaryEntityTypeMatched + " --- " + primaryEntityType);

        for (int i = 0; i < parsedSentences.length; i++) {
            parsedSentences[i].saveMatchings();
            SemanticGraph sg = parsedSentences[i].getSemanticGraph(grammaticalStructureFactory);
            for (Match match : parsedSentences[i].getAttributeMatchings()) {

                try {

                    String attributeCorePath = match.getAttributeCorePath();
                    IndexedWord commonGovernor = match.getCommonGovernor();

                    ClauseRelationDisambiguation clauseRelationDisambiguation =
                            new ClauseRelationDisambiguation(sg,
                                    match.getSubject(),
                                    match.getObject(),
                                    commonGovernor);

                    String relationSense = null;
                    try {
                        relationSense = clauseRelationDisambiguation.disambiguateRelation();
                    } catch (ParseException e) {
                        logger.debug("Disambiguation Problem: " + e.getMessage());
                    }


                    //Save
                    for (WikipediaCategory wc : new HashSet<>(page.getEntityCategories())) {

                        if (wc.getName().equalsIgnoreCase(match.getInfoboxType())) {

                            WikipediaCategoryAttribute wca =
                                    wc.getAttribute(match.getAttributeName());

                            WikipediaCategoryAttributeCorePath acp = wca.addCorePath(attributeCorePath);
                            acp.addGovernorSense(relationSense, commonGovernor.lemma());

                            logger.debug("Saving " + acp.toString() + " in the category: " + wc.getName());
                        }
                    }

                } catch (ParseException e) {
                    logger.debug("Attribute Core Path Problem: " + e.getMessage());
                }
            }
        }

        Wikipedia.getInstance().save(page);

        //adding results
        for (int i = 0; i < parsedSentences.length; i++) {
            List<String> matchings = parsedSentences[i].getMatchings();
            if (!matchings.isEmpty()) {
                result.put(parsedSentences[i].getSentence(),
                        new PathTypedDependency(matchings, parsedSentences[i].getSemanticGraphTypedCCprocessed(grammaticalStructureFactory)));
            }
        }
    }

    public HashMap<String, PathTypedDependency> getMatchings() {
        return result;
    }

    protected boolean primaryEntitiesMatching(ParsedSentence sentence, Pattern titleSynonymsPattern) {

        logger.debug("Checking primary entity matching in : " + sentence.getSentence());

        boolean fsm = PrimaryEntityMatchings.fullSynonymMatch(titleSynonymsPattern, sentence);
        logger.debug("Full Synon " + fsm);

        boolean pm = PrimaryEntityMatchings.partialMatch(title, sentence);
        logger.debug("Partial Match " + pm);

        // TODO: CHECK please!!!!
        //boolean tm = primaryEntityTypeMatched ? PrimaryEntityMatchings.typeMatch(this.primaryEntityType, sentence) : false;
        boolean tm = PrimaryEntityMatchings.typeMatch(this.primaryEntityType, sentence);
        logger.debug("Type Match " + tm);

        boolean mfp = mostFrequentPronoun != null && PrimaryEntityMatchings.mostFrequentPronoum(this.mostFrequentPronoun, sentence);
        logger.debug("Most Frequent Pron " + mfp);

        logger.debug("Primary Entity Recognition: " + (fsm || pm || tm || mfp));

        return fsm || pm || tm || mfp;
    }

    private String[] getWordTerms(String substring) {
        ArrayList<String> matchList = new ArrayList<>();

        Matcher matchingAttributeTerms = Pattern.compile("(([\\w\\p{IsLatin}]+?)($|[^\\w\\p{IsLatin}]))")
                .matcher(substring);

        while (matchingAttributeTerms.find()) {
            matchList.add(WikipediaUtils.cleanString(matchingAttributeTerms.group(2)));
        }

        return matchList.toArray(new String[matchList.size()]);
    }

    //When there are multiple matches to the primary entity in a sentence,
    //the matcher picks the one which is closest to the matched infobox attribute
    //value in the parser dependecy graph
    private Match getMatchCandidate(ParsedSentence sentence, List<IndexedWord> attributeTermNodes, AttributePattern ap, SemanticGraph sg) {

        logger.debug("\n\n ** " + ap.getAttributeName() + " " + ap.getAttribute() + " " + ap.getPattern() + " ** \n\n");

        Match matchCandidate = null;
        int currentMinimumDistance = Integer.MAX_VALUE;

        for (Entity primaryEntity : sentence.getPrimaryEntityMatchings()) {

            logger.debug("Primary Entity: " + primaryEntity.getRoot());

            for (IndexedWord primaryEntityWord : primaryEntity.getWords()) {

                logger.debug("PrimaryEntityWord: " + primaryEntityWord.lemma());

                for (IndexedWord attributeNode : attributeTermNodes) {

                    logger.debug("AttributeNode: " + attributeNode.lemma());

                    //Exclude the case when the attribute value and the primary entity refer to the same term
                    if (!attributeNode.equals(primaryEntityWord)) {

                        List path = sg.getShortestUndirectedPathEdges(primaryEntityWord, attributeNode);
                        if (path == null) {
                            //When primary entity word and attribute node are in two distinct core maps
                            continue;
                        }
                        int temp = path.size();

                        if (temp < currentMinimumDistance) {
                            matchCandidate =
                                    new Match(primaryEntity, new Entity(attributeNode, attributeTermNodes, ap.getAttribute()), ap.getInfoboxType());

                            matchCandidate.setAttributeName(ap.getAttributeName());
                            matchCandidate.setShortestPath(path);
                            matchCandidate.setObject(attributeNode);
                            matchCandidate.setSubject(primaryEntityWord);

                            currentMinimumDistance = temp;

                            logger.debug("\n*** New matching canditate: " + matchCandidate + " ***\n");
                        }
                    }
                }
            }
        }

        return matchCandidate;
    }


    private void filterings(ParsedSentence sentence) {

        logger.debug("------------------------------------------------- ");
        logger.debug("Filtering in : " + sentence.getSentence());

        //Get attribute values for the current page
        Iterator<AttributePattern> attributeIterator = new HashSet<>(attributeApp.keySet()).iterator();

        while (attributeIterator.hasNext()) {

            //Get the current attribute value
            AttributePattern ap = attributeIterator.next();
            logger.debug("---------- Checking attribute : " + ap.getAttribute() + "---------- ");
            logger.debug("Attribute DependencyPath: " + ap.getPattern().toString());

            //Check the presence of the attribute in the sentence
            Matcher matchingAttribute = ap.getPattern().matcher(sentence.getSentence());
            while (matchingAttribute.find()) {

                String[] attributeTerms = getWordTerms(sentence.getSentence().substring(matchingAttribute.start(), matchingAttribute.end()));
                SemanticGraph sg = sentence.getSemanticGraphTypedCCprocessed(grammaticalStructureFactory);
                logger.debug("Pattern for finding attributeTermNodes: " + "(?i)" + StringUtils.join(attributeTerms) + "|" + ap.getAttribute());
                List<IndexedWord> attributeTermNodes = sg.getAllNodesByWordPattern("(?i)" + StringUtils.join(attributeTerms) + "|" + ("\\Q" + ap.getAttribute() + "\\E"));
                logger.debug("AttributeTermNodes: " + attributeTermNodes.size());
                Match matchCandidate = getMatchCandidate(sentence, attributeTermNodes, ap, sg);

                logger.debug("Matching Candidate: " + matchCandidate);

                if (matchCandidate == null) {
                    continue;
                }

                if (FirstFilter.isApplicable(attributeApp, ap, sentence)) {

                    FirstFilter.apply(attributeFiltering, attributeApp, ap);

                } else if (!SecondFilter.isApplicable(sentence, attributeTermNodes, matchCandidate, sg) &&
                        !ThirdFilter.isApplicable(sentence, grammaticalStructureFactory, matchCandidate) &&
                        checkCandidate(matchCandidate)) {

                    matchCandidate.setAttributeName(ap.getAttributeName());
                    sentence.addAttributeMatch(matchCandidate);
                    attributeApp.put(ap, sentence);
                    logger.debug("New Matching Added: " + matchCandidate + " ");
                }
            }
        }
    }

    private boolean checkCandidate(Match matchCandidate) {
        logger.debug("\nChecking candidate validity...");
        List<SemanticGraphEdge> path = matchCandidate.getShortestPath();

        if (path == null || path.size() == 0 || path.size() > 2) {
            return false;
        }

        logger.debug("check first edge");

        if (!path.get(0).getRelation().toString().matches(FIRST_EDGE_PATTERN)) {

            if (path.size() == 2 &&
                    path.get(1).getRelation().toString().matches(FIRST_EDGE_PATTERN) &&
                    path.get(0).getGovernor().equals(path.get(1).getGovernor())) {

                logger.debug("swap edges");

                SemanticGraphEdge first = path.get(1);
                SemanticGraphEdge second = path.get(0);
                LinkedList<SemanticGraphEdge> newPath = new LinkedList<>();

                newPath.addFirst(first);
                newPath.addLast(second);

                matchCandidate.setShortestPath(newPath);

                return true;

            } else {
                return false;
            }
        }
        logger.debug("check size and common governor");
        boolean result = path.size() == 1 || path.get(0).getGovernor().equals(path.get(1).getGovernor());

        if (!result) logger.debug("Candidate filtered");

        return result;

    }

    public boolean isPrimaryEntityTypeMatched() {
        return primaryEntityTypeMatched;
    }

    public WikipediaPage getPage() {
        return page;
    }

    public String getPrimaryEntityType() {
        return primaryEntityType;
    }

    public String getTitle() {
        return title;
    }

    public String getMostFrequentPronoun() {
        return mostFrequentPronoun;
    }

}
