package it.uniroma1.dis.nlp.woie.common.wikipedia;

import javax.persistence.Entity;

/**
 * Created by kikkomep on 12/18/13.
 */
@Entity
public class WikipediaTemplateLiteralAttribute extends WikipediaTemplateValueAttribute {

    public static enum TYPE {URL, STRING, DOUBLE, INTEGER, IMAGE, INTERNAL_LINK, RANK, DATE, DATE_DAY, DATE_MONTH, DATE_YEAR, MONEY, LARGE_NUMBER, BIG_MONEY, PERCENT, LARGE_INTEGER, LARGE_DOUBLE, YEAR_RANGE, UNIT}



    private TYPE literalType;


    protected WikipediaTemplateLiteralAttribute() {
    }

    public WikipediaTemplateLiteralAttribute(TYPE literalType, String value) {
        super(value);
        this.literalType = literalType;
    }

    public WikipediaTemplateLiteralAttribute(TYPE literalType, String value, String rawValue) {
        super(value, rawValue);
        this.literalType = literalType;
    }


    public TYPE getLiteralType() {
        return literalType;
    }

    public void setLiteralType(TYPE literalType) {
        this.literalType = literalType;
    }

    @Override
    public String toString() {
        return "WikipediaTemplateLiteralAttribute{" +
                "value='" + value + '\'' +
                ", rawValue='" + rawValue + '\'' +
                ", literalType=" + literalType +
                '}';
    }
}
