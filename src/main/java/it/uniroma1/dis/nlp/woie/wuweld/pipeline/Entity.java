package it.uniroma1.dis.nlp.woie.wuweld.pipeline;

import edu.stanford.nlp.ling.IndexedWord;

import java.util.List;

/**
 *
 * @author lgu
 */
public class Entity {

    private List<IndexedWord> words;
    private IndexedWord root;
    private String attributeKey;

    public Entity(IndexedWord root, List<IndexedWord> words) {
        this.root = root;
        this.words = words;

    }

    public Entity(IndexedWord root, List<IndexedWord> words, String a) {
        this.root = root;
        this.words = words;
        this.attributeKey = a;

    }

    public List<IndexedWord> getWords() {
        return words;
    }

    public IndexedWord getRoot() {
        return root;
    }

    public void setAttributeKey(String a) {
        this.attributeKey = a;
    }

    public String getAttributeKey() {
        return this.attributeKey;
    }

}
