package it.uniroma1.dis.nlp.woie.common.wikipedia;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;

/**
 * Created by kikkomep on 12/18/13.
 */

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class WikipediaCategory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @NotNull
    protected String name;

    @ManyToMany
    @JoinTable(name = "WikipediaCategory_ParentCategories")
    protected Set<WikipediaCategory> parentCategories = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "WikipediaCategory_SubCategories")
    protected Set<WikipediaCategory> subcategories = new HashSet<>();

    @ManyToMany(mappedBy = "categories", cascade = CascadeType.MERGE)
    @JoinTable(name = "WikipediaPage_Categories")
    protected Set<WikipediaPage> pages = new HashSet<>();


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "category")
    protected Set<WikipediaCategoryAttribute> attributes = new HashSet<>();


    protected WikipediaCategory() {
    }

    public WikipediaCategory(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void addSubcategory(WikipediaCategory category) {
        if (category != null && !this.subcategories.contains(category))
            this.subcategories.add(category);
    }

    public void removeSubcategory(WikipediaCategory category) {
        if (category != null)
            this.subcategories.remove(category);
    }

    public Set<WikipediaCategory> getSubcategories() {
        return Collections.unmodifiableSet(this.subcategories);
    }

    public void addParentCategory(WikipediaCategory category) {
        if (category != null && !this.parentCategories.contains(category)) {
            this.parentCategories.add(category);
        }
    }

    public void removeParentCategory(WikipediaCategory category) {
        if (category != null)
            this.parentCategories.remove(category);
    }

    public Set<WikipediaCategory> getParentCategories() {
        return Collections.unmodifiableSet(this.parentCategories);
    }


    void addWikipediaPage(WikipediaPage page) {
        if (page != null && !this.pages.contains(page))
            this.pages.add(page);
    }

    void removeWikipediaPage(WikipediaPage page) {
        if (page != null && this.pages.contains(page))
            this.pages.remove(page);
    }


    public WikipediaCategoryAttribute getAttribute(String name) {

        if (name == null)
            throw new RuntimeException("Name must be not empty!!!");


        WikipediaCategoryAttribute attribute = null;
        for (WikipediaCategoryAttribute a : this.getAttributes()) {
            if (name.equals(a.getName())) {
                attribute = a;
                break;
            }
        }

        if (attribute == null) {
            attribute = new WikipediaCategoryAttribute(name, this);
            this.attributes.add(attribute);
        }

        return attribute;
    }


    public void removeAttribute(String name) {
        WikipediaCategoryAttribute attribute = this.getAttribute(name);
        if(attribute!=null)
            this.attributes.remove(attribute);
    }

    public Set<WikipediaCategoryAttribute> getAttributes(){
        return this.attributes;
    }


    public Set<WikipediaPage> getWikipediaPages() {
        return Collections.unmodifiableSet(this.pages);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WikipediaCategory)) return false;

        WikipediaCategory category = (WikipediaCategory) o;

        if (id != null ? !id.equals(category.id) : category.id != null) return false;
        if (!name.equals(category.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "WikipediaCategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
