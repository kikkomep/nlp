package it.uniroma1.dis.nlp.woie.wuweld.pipeline;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.services.RelationExpander;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.services.WordPathExpander;
import it.uniroma1.dis.nlp.woie.wuweld.woe.pattern.*;

import java.util.*;
import java.util.regex.Pattern;

/**
 *
 * @author lgu
 */
public class PlainTextPipeline extends Pipeline {
    
    private List<InformationTriple> triples = new LinkedList<>();
    private Collection<TypedDependency> tdl;
    private SemanticGraph sg;
    private GeneralizedCorePathsRepository pr;
    private PatternsStatistics ps;
    private GrammaticalStructure gs;
    
    public PlainTextPipeline(String text) {
        super(text);
        pr = GeneralizedCorePathsRepository.getInstance();
        ps = pr.getStatistics();
    }

    public List<String> getSentencesTitleFiltered(String title){
        Pattern titlePattern = WikipediaUtils.createSynonymsRegEx(title);
        LinkedList<String> result = new LinkedList<>();
        String[] sentences =super.splitIntoSentences(super.text);
        for(int i=0; i<sentences.length; i++){
            if(titlePattern.matcher(sentences[i]).find()){
                result.add(sentences[i]);
            }
        }

        return result;
    }

    public String[] getSentences(){
        return super.splitIntoSentences(super.text);
    }


    public Collection<TypedDependency> getCollectionOfCCProcessedTypedDependencies() {
        return tdl;
    }

    public Collection<TypedDependency> getDependecies() {
        return gs.typedDependencies(true);
    }

    @Override
    public void process() {
        
        //split and annotate sentences
        super.initializePipeline();

        for (int i = 0; i < parsedSentences.length; i++) {
            for (CoreMap c : parsedSentences[i].getAnnotation().get(CoreAnnotations.SentencesAnnotation.class)) {

                //Getting the semantic graph
                gs = grammaticalStructureFactory.newGrammaticalStructure(c.get(TreeCoreAnnotations.TreeAnnotation.class));
                this.tdl = gs.typedDependenciesCCprocessed(true);
                this.sg = new SemanticGraph(tdl);

                logger.info("The Graph: ");
                sg.prettyPrint();

                //Searching an subject edge
                for (SemanticGraphEdge edgeSubj : sg.getEdgeSet()) {
                    if (edgeSubj.getRelation().toString().matches(Pipeline.FIRST_EDGE_PATTERN)) {

                        logger.debug("Subject Rel found");

                        //GeneralizedCorePath matching with the sentence
                        List<WeightedPattern> matchingCorePath = findMatchingPathCandidates(edgeSubj);
                        logger.debug("Matching candidates: " + matchingCorePath.size());

                        //Extract the information triple
                        InformationTriple triple = findInformationTriple(matchingCorePath);

                        //Add the triple to result set and save it on the db
                        if(triple!=null){
                            this.triples.add(triple);

                            //TODO save triple
                            this.logger.debug("New Triple: " + triple.toString());
                        }
                    }
                }
            }
        }
    }

    private InformationTriple findInformationTriple(List<WeightedPattern> matchingCorePath){

        if (!matchingCorePath.isEmpty()) {

            logger.debug("There are dependencyPath candidates!!");

            WeightedPattern weightedPatternToBeApplied = matchingCorePath.get(0);

            for (WeightedPattern p : matchingCorePath) {
                if (p.weight > weightedPatternToBeApplied.weight) {
                    weightedPatternToBeApplied = p;
                }
            }

            logger.debug("DependencyPath choosen: " + weightedPatternToBeApplied.dependencyPath.getPath());

            SemanticGraph fe =new SemanticGraph(gs.typedDependencies(true));

            RelationExpander re = new RelationExpander(fe,
                    weightedPatternToBeApplied.rel,
                    weightedPatternToBeApplied.copula);
            String relationString = re.getExpandedWordString();

            WordPathExpander wpeSubj = new WordPathExpander(fe,weightedPatternToBeApplied.subj);
            String subjString = wpeSubj.getExpandedWordString();

            WordPathExpander wpeObj = new WordPathExpander(fe,weightedPatternToBeApplied.obj);
            String objString = wpeObj.getExpandedWordString();

            return new InformationTriple(subjString, relationString, objString,weightedPatternToBeApplied.dependencyPath.getPath() );

        } else {

            logger.debug("No dependencyPath matching\n ");
            return null;

        }
    }

    private List<WeightedPattern> findMatchingPathCandidates(SemanticGraphEdge edgeSubj){

        List<WeightedPattern> corePathMatched = new LinkedList();

        //Getting the generalized string for the edge edgeSubj
        String edgeString = Match.createStringGeneralizedRelation(edgeSubj);

        //Getting the generalized path starting with the generalized edge
        logger.debug("Looking for path: " + edgeString);
        List<GeneralizedCorePath> listOfDependencyPathRetrieved = pr.findAll();
        logger.debug("Number of found paths: " + listOfDependencyPathRetrieved.size());

        if (!listOfDependencyPathRetrieved.isEmpty()) {

            for (DependencyPath p : listOfDependencyPathRetrieved) {

                logger.debug("DependencyPath retrieved: " + p.getPath());

                if (p.getPathStrings().length == 1) {

                    logger.debug("DependencyPath Matched! " + p.getPath());

                    WeightedPattern w = new WeightedPattern();
                    w.dependencyPath = p;
                    w.weight = ps.getNormalizedLogFrequency(p.getPath());
                    w.edgeSubj = edgeSubj;
                    w.obj = edgeSubj.getGovernor();
                    w.subj = edgeSubj.getDependent();
                    w.edgeObj = null;
                    w.rel = edgeSubj.getGovernor();

                    if(!sg.getChildrenWithRelns(edgeSubj.getGovernor(), RelationModifiersSelector.getInstance().getGrammaticalRelationsForCopula()).isEmpty()){
                        w.copula = true;
                        corePathMatched.add(w);
                    }

                } else {

                    List<SemanticGraphEdge> govChildren = sg.getOutEdgesSorted(edgeSubj.getGovernor());

                    for (SemanticGraphEdge edgeObj : govChildren) {

                        logger.debug("Checking: " + edgeString + "#" + Match.createStringGeneralizedRelation(edgeObj) + " = " + p.getPath());

                        if (p.getPath().equals(edgeString + "#" + Match.createStringGeneralizedRelation(edgeObj))) {

                            logger.debug("DependencyPath Matched! " + p.getPath());

                            WeightedPattern w = new WeightedPattern();
                            w.dependencyPath = p;
                            w.weight = ps.getNormalizedLogFrequency(p.getPath());
                            w.edgeSubj = edgeSubj;
                            w.subj = edgeSubj.getDependent();
                            w.obj = edgeObj.getDependent();
                            w.edgeObj = edgeObj;
                            w.rel = edgeSubj.getGovernor();

                            corePathMatched.add(w);

                        }
                    }
                }
            }
        }

        return corePathMatched;

    }
    
    public List<InformationTriple> getTriples() {
        return triples;
    }
    


    public void printTriples(){
        
        logger.debug("\n***********************************");
        logger.debug("Extracted Triples\n");
        for(InformationTriple it : this.triples){
            logger.debug(it.toString());
        }
        logger.debug("\n***********************************\n");
    }
}
