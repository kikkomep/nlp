package it.uniroma1.dis.nlp.woie.lamepan;

import it.uniroma1.dis.nlp.woie.App;
import it.uniroma1.dis.nlp.woie.common.wikipedia.*;

import it.uniroma1.dis.nlp.woie.wuweld.WuWeldApp;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.FindAttributePipeline;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by lgu on 24/01/14.
 */
public class LaMepAnApp extends App {


    int attributeFound = 0;
    int skipped = 0;
    int pageContainingPrimaryEntity = 0;
    int pageContainingAnAttribute = 0;
    int pageContainingAtLeastAnAttributeCorePath = 0;

    private ProcessedPagesHistory history;


    @Override
    public void start() {

        try {

            history = ProcessedPagesHistory.getInstance();
            Set<String> processedPages = history.getProcessedPages();

            Wikipedia wikipedia = Wikipedia.getInstance();

            if (getArgumentNames().contains("page")) {

                WikipediaPage page = getPage(getArgumentValue("page"));
                processPage(page);


            } else if (getArgumentNames().contains("limit")) {

                int limit = Integer.parseInt(getArgumentValue("limit"));
                boolean forceUpdate = getArgumentNames().contains("force-update");


                List<WikipediaPage> pages = null;
                if (forceUpdate) pages = wikipedia.find(limit, 0, false);
                else pages = wikipedia.find(limit, history.countProcessedPages());


                logger.info("\n\nPages to process: " + limit);
                logger.info("Processed pages up to now: " + history.countProcessedPages());

                int counter = 0;
                for (WikipediaPage page : pages) {


                    if (!processedPages.contains(page.getTitle())) {
                        processPage(page, counter);
                        counter++;
                    }

                    if (counter == limit) break;
                }
            }


            history.save();

        } catch (it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.ParseException e) {
            e.printStackTrace();
        } catch (WikipediaPageInputStreamNotAvailableException e) {
            e.printStackTrace();
        } catch (WikipediaPageNotFoundException e) {
            e.printStackTrace();
        }

    }

    private WikipediaPage getPage(String pageTitle) throws it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.ParseException, WikipediaPageInputStreamNotAvailableException, WikipediaPageNotFoundException {

        Wikipedia wppr = Wikipedia.getInstance();

        WikipediaPage wp = wppr.findByTitle(pageTitle);
        if (wp == null) {
            wp = wppr.loadByTitle(pageTitle);
            wppr.parse(wp);
            wppr.save(wp);
        } else {
            wp = wppr.refresh(wp);
        }

        return wp;
    }

    public void processPage(WikipediaPage wp) {
        processPage(wp, -1);
    }

    private void processPage(WikipediaPage wp, int count) {


        if (count == -1)
            logger.info("\n\n------ Processing page: " + wp.getTitle() + " ------");
        else logger.info("\n\n------ Processing page #" + count + ": " + wp.getTitle() + " ------");

        Map<String, Set<String>> attributeMap = null;

        try {

            FindAttributePipeline fap = new FindAttributePipeline(wp);
            fap.process();

            if (fap.isAppearPrimaryEntity()) {
                pageContainingPrimaryEntity++;
            }

            if (fap.isBelongToACategoryContaingAtLeastAnAttribute()) {
                pageContainingAnAttribute++;
            }

            if (fap.isBelongToACategoryContaingAtLeastAnAttributeWithAttributeCorePath()) {
                pageContainingAtLeastAnAttributeCorePath++;
            }

            //printing result
            attributeMap = fap.getResults();
            int attributeCounter = 1;
            if (!attributeMap.isEmpty()) logger.info("\nAttribute Found: ");
            for (String key : attributeMap.keySet()) {
                for (String value : attributeMap.get(key)) {
                    logger.info(attributeCounter++ + ") " + key + " : " + value);
                }
            }

            history.addProcessedPage(wp.getTitle());

        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public void processPages(List<WikipediaPage> pages, int numOfPages, int skip) {

        int n = skip + 1;


        for (WikipediaPage wp : pages) {

            processPage(wp);

            int count = 1;
            logger.info("\nPage Infobox: ");
            boolean containInfobox = false;
            for (WikipediaTemplate wt : wp.getTemplates()) {
                if (wt != null && wt.getName() != null && wt.getName().startsWith("Infobox")) {
                    containInfobox = true;
                    for (WikipediaTemplateAttributeAssociation wtaa : wt.getAttributes()) {
                        logger.info(count++ + ") " + wtaa.getName() + " : " + wtaa.getAttribute().getValue());
                    }
                }
            }

            if (!containInfobox) {
                logger.info("The page does not contain Infoboxes!!");
            }
        }

        logger.info("\n# of Processed pages: " + (n - skip - skipped - 1) + "/" + (n - skip - 1));
        logger.info("# of skipped pages " + skipped + "/" + (n - skip - 1));
        logger.info("# of New attribute extracted " + attributeFound + "/" + (n - skip - 1));
        logger.info("# of page containing an attribute " + pageContainingAnAttribute + "/" + (n - skip - 1));
        logger.info("# of page containing at least a core path " + pageContainingAtLeastAnAttributeCorePath + "/" + (n - skip - 1));
        logger.info("# of page containing primary entity " + pageContainingPrimaryEntity + "/" + (n - skip - 1));

        logger.info("\n\nNew attribute found: " + attributeFound);
    }

    public static void main(String args[]) {
        init(new LaMepAnApp(), args);
    }
}
