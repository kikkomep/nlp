package it.uniroma1.dis.nlp.woie.wuweld.woe.pattern;

/**
 * Created by kikkomep on 12/11/13.
 */
public class PatternStatistics {

    private String path;
    private Long occurrences;

    public PatternStatistics() {
    }

    public PatternStatistics(String path, Long occurrences) {
        this.path = path;
        this.occurrences = occurrences;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getOccurrences() {
        return occurrences;
    }

    public void setOccurrences(Long occurrences) {
        this.occurrences = occurrences;
    }

    @Override
    public String toString() {
        return "PatternStatistics{" +
                "path='" + path + '\'' +
                ", occurrences=" + occurrences +
                '}';
    }
}
