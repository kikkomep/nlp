
package it.uniroma1.dis.nlp.woie.wuweld.pipeline;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 *
 * @author lgu
 */


public class AttributePattern {
    
    private Pattern pattern;
    private String attribute;
    private String attributeName;
    private String infoboxType;

    public AttributePattern(Pattern pattern, String attribute,String name, String infoboxType) {
        this.pattern = pattern;
        this.attribute = attribute;
        this.attributeName=name;
        this.infoboxType = infoboxType;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public String getAttribute() {
        return attribute;
    }

    public String getInfoboxType() {
        return infoboxType;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.attribute);
        return hash;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AttributePattern other = (AttributePattern) obj;
        if (!Objects.equals(this.attribute, other.attribute)) {
            return false;
        }
        return true;
    }
    
    
    
}
