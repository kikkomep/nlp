package it.uniroma1.dis.nlp.woie.common.wikipedia;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by kikkomep on 12/19/13.
 */
@Entity
public class WikipediaTemplateAttributeProperty implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    private String name;

    @ManyToOne(cascade = CascadeType.ALL)
    protected WikipediaTemplateAttribute value;

    @ManyToOne(cascade = CascadeType.ALL)
    private WikipediaTemplateAttributeAssociation attributeAssociation;


    public WikipediaTemplateAttributeProperty() {
    }


    public WikipediaTemplateAttributeProperty(String name, WikipediaTemplateAttribute value, WikipediaTemplateAttributeAssociation association) {
        this.name = name;
        this.value = value;
        this.attributeAssociation = association;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public WikipediaTemplateAttribute getValue() {
        return value;
    }

    public WikipediaTemplateAttributeAssociation getAttributeAssociation() {
        return attributeAssociation;
    }
}
