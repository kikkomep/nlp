package it.uniroma1.dis.nlp.woie.lamepan;

import it.uniroma1.dis.nlp.woie.App;

import java.io.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by kikkomep on 1/27/14.
 */
public class ProcessedPagesHistory {



    private final String PAGES_HISTORY;
    private HashSet<String> history = new HashSet<>();


    private static ProcessedPagesHistory instance = null;

    public static ProcessedPagesHistory getInstance(){
        if(instance==null)
            instance = new ProcessedPagesHistory();
        return instance;
    }

    private ProcessedPagesHistory(){
        PAGES_HISTORY = App.getInstance().getConfigurationProperties().getProperty("entity_pages_history");

        File data = new File(PAGES_HISTORY);
        if (data.exists()) {
            try {

                ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(data));

                history = (HashSet<String>) inputStream.readObject();

                inputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        } else {
            history = new HashSet<String>();
        }
    }


    public Set<String> getProcessedPages(){
        return Collections.unmodifiableSet(this.history);
    }



    public void addProcessedPage(String pageTitle){
        this.history.add(pageTitle);
    }


    public void save(){
        ObjectOutputStream outputStream = null;
        try {
            outputStream = new ObjectOutputStream(new FileOutputStream(PAGES_HISTORY));
            outputStream.writeObject(history);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int countProcessedPages() {
        return this.history.size();
    }
}
