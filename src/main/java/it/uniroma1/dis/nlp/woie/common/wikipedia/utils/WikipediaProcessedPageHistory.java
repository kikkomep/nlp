package it.uniroma1.dis.nlp.woie.common.wikipedia.utils;

import it.uniroma1.dis.nlp.woie.App;

import java.io.*;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by kikkomep on 1/27/14.
 */
public class WikipediaProcessedPageHistory {



    private final String PAGES_HISTORY;
    private HashMap<String, Integer> history = new HashMap<>();


    public static final int INFOBOX_NOT_AVAILABLE = 0;
    public static final int INFOBOXES_AVAILABLE = 1;
    public static final int INFOBOXES_ONLY = 2;

    private static WikipediaProcessedPageHistory instance = null;

    public static WikipediaProcessedPageHistory getInstance(){
        if(instance==null)
            instance = new WikipediaProcessedPageHistory();
        return instance;
    }

    private WikipediaProcessedPageHistory(){
        PAGES_HISTORY = App.getInstance().getConfigurationProperties().getProperty("parsed_pages_history");

        File data = new File(PAGES_HISTORY);
        if (data.exists()) {
            try {

                ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(data));

                history = (HashMap<String, Integer>) inputStream.readObject();

                inputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        } else {
            history = new HashMap<String, Integer>();
        }
    }


    public Set<String> getProcessedPages(){
        return this.history.keySet();
    }

    public boolean containsInfoboxes(String processedPageTitle){
        return this.history.containsKey(processedPageTitle) && this.history.get(processedPageTitle)==INFOBOXES_AVAILABLE;
    }

    public boolean containsOnlyInfoboxes(String processedPageTitle){
        return this.history.containsKey(processedPageTitle) && this.history.get(processedPageTitle)==INFOBOXES_ONLY;
    }


    public void addProcessedPage(String pageTitle, int containsInfoboxes){
        this.history.put(pageTitle, containsInfoboxes);

    }


    public void save(){
        ObjectOutputStream outputStream = null;
        try {
            outputStream = new ObjectOutputStream(new FileOutputStream(PAGES_HISTORY));
            outputStream.writeObject(history);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int countProcessedPages() {
        return this.history.size();
    }
}
