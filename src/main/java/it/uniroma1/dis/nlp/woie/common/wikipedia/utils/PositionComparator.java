package it.uniroma1.dis.nlp.woie.common.wikipedia.utils;

import java.util.Comparator;
import java.util.Map;

class PositionComparator implements Comparator<String> {

    Map<String, Integer> unsortedMap;

    public PositionComparator(Map<String, Integer> unsortedMap) {
        this.unsortedMap = unsortedMap;
    }

    @Override
    public int compare(String a, String b) {

        Integer x = unsortedMap.get(a);
        Integer y = unsortedMap.get(b);

        if (x.equals(y)) {
            return a.compareTo(b);
        }
        if (x >= y) {
            return -1;
        } else {
            return 1;
        }
    }
}