/*
 * Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
 * Mountain View, California, 94041, USA.
 */
package it.uniroma1.dis.nlp.woie.auerlehmann;

import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaTemplate;

import org.apache.log4j.Logger;

import java.util.regex.Pattern;

/**
 *
 * @author kikkomep
 */
public class WikipediaTemplatesToIgnoreFilter extends WikipediaTemplateFilter {

    private Logger logger = Logger.getLogger(WikipediaTemplatesToIgnoreFilter.class);

    /**
     * Rejects a template if its name is within a given set
     * 
     * @param template a template
     * @return <code>true</code> if the number of triples is less than 3;
     * <code>false</code> otherwise
     */
    @Override
    public boolean reject(WikipediaTemplate template) {

        String name = "" + template.getName();
        Pattern pattern = Pattern.compile("Cite|cite|quote|citation|convert|Reflist", Pattern.CASE_INSENSITIVE);
        return pattern.matcher(name).find();
    }
}
