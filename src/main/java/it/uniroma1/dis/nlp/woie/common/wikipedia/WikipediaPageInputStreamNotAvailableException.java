/*
 * Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
 * Mountain View, California, 94041, USA.
 */
package it.uniroma1.dis.nlp.woie.common.wikipedia;

/**
 * @author kikkomep
 */
public class WikipediaPageInputStreamNotAvailableException extends Exception {

    public WikipediaPageInputStreamNotAvailableException() {
        super("No InputStream is available for this WikipediaPage Page");
    }

    public WikipediaPageInputStreamNotAvailableException(String message) {
        super(message);
    }

    public WikipediaPageInputStreamNotAvailableException(String message, Throwable cause) {
        super(message, cause);
    }
}
