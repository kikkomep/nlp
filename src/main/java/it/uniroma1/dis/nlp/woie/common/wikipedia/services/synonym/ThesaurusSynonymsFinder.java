package it.uniroma1.dis.nlp.woie.common.wikipedia.services.synonym;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kikkomep on 12/30/13.
 */
public class ThesaurusSynonymsFinder implements SynonymsFinder{

    private static final String thesaurusAPI = "http://words.bighugelabs.com/api/2/1f7fd5bec65cd773ba173f3ca241b855/";

    public List<String> getSynonymsOnThesaurus(String term) {
        ArrayList<String> result = new ArrayList();
        return result;
    }

    private String constructThesaurusURL(String term) {
        return thesaurusAPI + term + "/json";
    }

    @Override
    public List<String> findSynonyms(String term) {
        ArrayList<String> list = new ArrayList<>();
        return list;
    }
}
