package it.uniroma1.dis.nlp.woie;

import edu.cmu.lti.jawjaw.pobj.POS;
import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.WS4J;
import edu.cmu.lti.ws4j.impl.*;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;
import it.uniroma1.dis.nlp.woie.auerlehmann.AuerLehmannTemplateProcessor;
import it.uniroma1.dis.nlp.woie.auerlehmann.relation.plain.TemplateRelationWriter;
import it.uniroma1.dis.nlp.woie.common.wikipedia.*;
import it.uniroma1.dis.nlp.woie.common.wikipedia.services.synonym.WikipediaSynonymsRepository;
import it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.preprocessor.TagFilter;
import it.uniroma1.dis.nlp.woie.utils.RelationsRepositoryFactory;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 * Created by kikkomep on 12/14/13.
 */
public class MyAppX extends App {


    private static ILexicalDatabase db = new NictWordNet();
    private static RelatednessCalculator[] rcs = {
            new HirstStOnge(db), new LeacockChodorow(db), new Lesk(db), new WuPalmer(db),
            new Resnik(db), new JiangConrath(db), new Lin(db), new Path(db)
    };

    private static void findSimilarTo(String word1, String word2) {
        WS4JConfiguration.getInstance().setMFS(true);
        for (RelatednessCalculator rc : rcs) {
            double s = rc.calcRelatednessOfWords(word1, word2);
            System.out.println(rc.getClass().getName() + "\t" + s);

        }
    }


    public boolean subsumptionByWordNet(String hypernym, String hyponym, POS pos) {


        for (String ws : (WS4J.findHypernyms(hyponym, pos))) {

            double score = WS4J.runWUP(hyponym, ws);
            logger.info("Word: " + ws + " -- Similarity: " + score);
            if (ws.equalsIgnoreCase(hypernym))
                return true;

            if(score>0.7){
                for(String w : WS4J.findSynonyms(ws, pos)){
                    System.out.println("Synset: " + w);
                }
            }
        }

        System.out.println("HYPO");
        for (String ws : (WS4J.findHypernyms(hyponym, pos))) {
            logger.info("Word: " + ws + " -- Similarity: " + WS4J.runWUP(hypernym, ws));
            if (ws.equalsIgnoreCase(hypernym))
                return true;
        }

        return false;
    }


    public boolean subsumptionBySynset(String hypernym, String hyponym, POS pos) {
        for(String synHyper : WS4J.findSynonyms(hypernym, pos)){
            for(String synHypo : WS4J.findSynonyms(hypernym, pos)){
                System.out.println("Comparing: " + synHyper + " " + synHypo);
                //if(synHyper.equalsIgnoreCase(synHypo)) return true;
                if(WS4J.findHyponyms(synHyper, pos).contains(hyponym)) return true;
            }
        }

        return false;
    }


    public boolean subsumptionByHyper(String hypernym, String hyponym, POS pos) {


        if(hypernym.equalsIgnoreCase(hyponym)){
            logger.debug("Identity relation!!!");
            return true;
        }


        //
        Set<String> hyponymSynonyms = WS4J.findSynonyms(hyponym, pos);
        for(String syn : hyponymSynonyms){

        }


        Set<String> hypernymSynonyms = WS4J.findSynonyms(hypernym, pos);







        Set<String> hyperSynsets = WS4J.findSynonyms(hypernym, pos);
        Set<String> hypoHypernyms = WS4J.findHypernyms(hyponym, pos);

        for(String hypoHypernym : hypoHypernyms){
            Set<String> hypoHypernymSynsets = WS4J.findSynonyms(hypoHypernym, pos);
            for(String hypoHypernymSynset :  hypoHypernymSynsets){
                for(String hyperSynset : hyperSynsets){
                    System.out.println("Comparing: " + hyperSynset + " with " + hypoHypernymSynset);
                    if(hyperSynset.equalsIgnoreCase(hypoHypernymSynset)){
                        System.out.println("Equals!!!");
                        return true;
                    }

                    double similarity = WS4J.runWUP(hyperSynset, hypoHypernymSynset);
                    if(similarity>0.7){
                        System.out.println("Similar: " + similarity + "!!!");
                        //return true;
                    }
                }
            }
        }


        return false;
    }



    public boolean subsumes(String hypernym, String hyponym, POS pos) {

        return subsumptionByHyper(hypernym, hyponym, pos);
        //return subsumptionBySynset(hypernym, hyponym, pos) || subsumptionByWordNet(hypernym, hyponym, pos);
    }

    @Override
    public void start() {


        try {

            Wikipedia wp = Wikipedia.getInstance();


            System.out.println("Subsumption relation: " + subsumes("person", "writer", POS.n));


            if (true) return;


            System.out.println(WS4J.runWUP("person", "human"));


            String wt = "person";

            for (String ws : (WS4J.findHypernyms(wt, POS.n))) {

                logger.info("Word: " + ws);
            }


            if (true) return;


            WikipediaPage p = wp.findByTitle("Gary_Webb");

            logger.info("P: " + (p == null));

            if (p != null)
                wp.delete(p);
            else {
                p = wp.refresh(p);
            }

            if (true) return;


            for (String s : RelationsRepositoryFactory.getInstance().getAuerlehmannRepository().findAllSubjects()) {
                System.out.println(s);
            }


            String x1 = "166,000,000,000.00";
            String x2 = "1.66E11";


            BigDecimal n1 = BigDecimal.valueOf(Double.parseDouble(x1.replaceAll(",", "")));
            BigDecimal n2 = BigDecimal.valueOf(Double.parseDouble(x2));


            logger.info("N1: " + n1);
            logger.info("N1: " + n2);

            logger.info("EQU: " + n1.equals(n2));

            if (true) return;


            AuerLehmannTemplateProcessor processor = new AuerLehmannTemplateProcessor();
            processor.addTemplateWriter(new TemplateRelationWriter());


            WikipediaPageLoaderFactory.getIntance().getPageLoader();


            Wikipedia wikipedia = Wikipedia.getInstance();


            WikipediaPage px = wikipedia.parse("Victoria_Harbour");
            logger.info(px.getRawText());
            logger.info(px.getTemplateFilteredRawText());
            logger.info(px.getPlainText());

            if (true) return;


            int skip = wikipedia.findAll().size() - 5;
            int numOfPages = 100;


            List<WikipediaPage> xpages = wikipedia.find(numOfPages, skip, true);

            logger.debug("PAGES: " + xpages.size());


            if (true) return;


            logger.debug(TagFilter.toSkip("<ref name=Levy/> It is one of three r") + "");


            logger.debug(wikipedia.parse("Autism").getPlainText());

            if (true) return;


            List<WikipediaPage> pages = wikipedia.loadPages(10, 10);

            logger.debug("\n");

            for (WikipediaPage pg : pages) {
                logger.info("===> " + pg.getTitle());
                //processor.process(pg);
            }


            pages = wikipedia.loadPages(10, 10);

            logger.debug("\n");

            for (WikipediaPage pg : pages) {
                logger.info("===> " + pg.getTitle());
                //processor.process(pg);
            }


            if (true) return;


            WikipediaPage page = Wikipedia.getInstance().findByTitle("Archie_Hunter");
            if (page != null)
                wikipedia.delete(page);

            page = wikipedia.loadByTitle("Archie_Hunter");
            wikipedia.parse(page);
            wikipedia.save(page);


            processor.process(page);


            if (true) return;


            if (true) return;


            org.w3c.dom.ElementTraversal el;

            page = Wikipedia.getInstance().loadByTitle("pippo");


            //if(true) return;


            for (String syn : WikipediaSynonymsRepository.getInstance().findSynonyms("Gary_Webb")) {

                logger.info("SYN: " + syn);

            }


            //if(true) return;


            page = Wikipedia.getInstance().findByTitle("pippo");
            if (page == null) {
                page = Wikipedia.getInstance().loadByTitle("pippo");
                Wikipedia.getInstance().parse(page);
                Wikipedia.getInstance().save(page);
            }

            WikipediaCategory category = Wikipedia.getInstance().getPageCategory("Pippo");
            WikipediaCategoryAttribute attribute = category.getAttribute("prova");

            logger.info("ID: " + attribute.getId());


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Entry for the WuWeldApp
     *
     * @param args
     */
    public static void main(String args[]) {
        init(new MyAppX(), args);
    }
}
