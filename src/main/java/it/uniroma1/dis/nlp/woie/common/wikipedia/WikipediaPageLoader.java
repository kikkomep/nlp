/*
 * Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
 * Mountain View, California, 94041, USA.
 */
package it.uniroma1.dis.nlp.woie.common.wikipedia;

import java.io.InputStream;
import java.util.List;

/**
 *
 * @author kikkomep
 */
public interface WikipediaPageLoader {

    //public boolean isUpToDate(WikipediaPage page) throws WikipediaPageNotFoundException;
    public InputStream newInputStream(WikipediaPage page) throws WikipediaPageInputStreamNotAvailableException;

    public WikipediaPage loadByTitle(String pageTitle) throws WikipediaPageNotFoundException;

    List<WikipediaPage> loadPages(int numberOfPages, int skipFirstPages);

    public List<WikipediaPage> loadPages(int numberOfPages);

    public List<WikipediaPage> loadPages(int numberOfPages, int skipFirstPages, boolean printSkips);
}
