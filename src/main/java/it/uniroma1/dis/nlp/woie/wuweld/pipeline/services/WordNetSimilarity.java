package it.uniroma1.dis.nlp.woie.wuweld.pipeline.services;

import edu.cmu.lti.ws4j.WS4J;

/**
 * Created by lgu on 05/02/14.
 */
public class WordNetSimilarity {

    public static boolean areSimilar(String word1, String word2){
        return WS4J.runWUP(word1,word2)>0.7;
    }

}
