package it.uniroma1.dis.nlp.woie.wuweld.pipeline.services;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.ParseException;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.RelationModifiersSelector;
import it.uniroma1.lcl.jlt.ling.Word;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by lgu on 03/02/14.
 */
public class RelationExpander extends Expander{

    private final Logger logger = LoggerFactory.getLogger(RelationExpander.class);
    private Collection<IndexedWord> relation;
    private boolean searchCopula;

    public RelationExpander(SemanticGraph semanticGraph, IndexedWord principal,boolean searchCopula) {
        super(semanticGraph,principal);
        this.relation = null;
        this.searchCopula= searchCopula;
    }


    private void calculateRelation() {

        if(this.relation!=null) return;

        RelationModifiersSelector selector = RelationModifiersSelector.getInstance();
        List<IndexedWord> relation = new LinkedList();

        if (searchCopula) {
            logger.debug(principalWord.toString());

            List<IndexedWord> tba =semanticGraph.getChildrenWithRelns(principalWord, selector.getGrammaticalRelationsForCopula());
            if(tba!=null){
                relation.addAll(tba);
            }


        } else {

            relation.add(principalWord);
            relation.addAll(semanticGraph.getChildrenWithRelns(principalWord, selector.getGrammaticalRelation()));

        }

        Collections.sort(relation, new Comparator<IndexedWord>() {
            @Override
            public int compare(IndexedWord o1, IndexedWord o2) {
                return o1.index() - o2.index();
            }
        });

        this.relation = relation;
    }

    public String getExpandedWordString(){

        calculateRelation();

        StringBuilder sb = new StringBuilder();
        for (IndexedWord w : relation) {

            sb.append(w.word());
            sb.append(" ");
        }

        return sb.toString();
    }

    public Collection<IndexedWord> getIndexedWordCollection(){
        calculateRelation();
        return relation;
    }

    public Collection<Word> getWordCollection() {
        calculateRelation();
        HashSet<Word> result = new HashSet<>();
        for(IndexedWord word: relation){
            try {
                result.add(transformIndexedWord(word));
            } catch (ParseException e) {

            }
        }
        return result;
    }
}
