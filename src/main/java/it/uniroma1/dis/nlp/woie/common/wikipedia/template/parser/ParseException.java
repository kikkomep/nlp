package it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser;

/**
 * Created by kikkomep on 12/14/13.
 */
public class ParseException extends Exception{

    public ParseException(Throwable cause) {
        super(cause);
    }

    public ParseException(String message, Throwable cause) {
        super(message, cause);
    }
}
