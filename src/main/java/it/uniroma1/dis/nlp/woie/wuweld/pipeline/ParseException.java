package it.uniroma1.dis.nlp.woie.wuweld.pipeline;

/**
 *
 * @author lgu
 */
public class ParseException extends Exception{

    public ParseException(String message){
        super(message);
    }
}
