/*
 * Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
 * Mountain View, California, 94041, USA.
 */
package it.uniroma1.dis.nlp.woie.common.wikipedia;

import it.uniroma1.dis.nlp.woie.App;
import org.apache.log4j.Logger;

/**
 *
 * @author kikkomep
 */
public class WikipediaPageLoaderFactory {

    private static final WikipediaPageLoaderFactory instance = new WikipediaPageLoaderFactory();

    public static WikipediaPageLoaderFactory getIntance() {
        return instance;
    }

    private WikipediaPageLoaderFactory() {
    }
    
    private WikipediaPageLoader pageLoader;

    public WikipediaPageLoader getPageLoader() {
        if (pageLoader == null) {

            App app = App.getInstance();

            Logger logger = Logger.getLogger(WikipediaPageLoaderFactory.class);

            String finder = (String) app.getConfigurationProperties().get(
                    "page_loader");

            if (finder == null || finder.isEmpty()) {
                logger.error("WikipediaLoader not properly configured: property 'pageLoader' not found!");
                throw new RuntimeException("WikipediaLoader not properly configured: property 'pageLoader' not found!");
                //System.exit(0);
            }

            try {

                logger.debug("Loading the WikipediaPageLoader: " + finder);
                this.pageLoader = (WikipediaPageLoader) Class.forName(finder).newInstance();
                logger.info("WikipediaPageLoader '" + this.pageLoader.getClass().getSimpleName() + "' initialized ...");

            } catch (ClassNotFoundException ex) {
                logger.error("WikipediaLoader not properly configured: class " + finder + "not found not found!", ex);
                System.exit(0);

            } catch (InstantiationException ex) {
                logger.error("Error during WikipediaLoader instantiation!", ex);
                System.exit(0);

            } catch (IllegalAccessException ex) {
                logger.error("Error during WikipediaLoader instantiation!", ex);
                System.exit(0);
            }
        }

        return this.pageLoader;
    }
}
