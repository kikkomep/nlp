package it.uniroma1.dis.nlp.woie.common.dbpedia;

import it.uniroma1.dis.nlp.woie.App;
import it.uniroma1.dis.nlp.woie.common.relation.RelationsRepository;
import it.uniroma1.dis.nlp.woie.utils.RelationsRepositoryFactory;
import org.apache.commons.compress.compressors.CompressorException;

import java.io.*;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kikkomep on 12/14/13.
 * <p/>
 * RESOURCE,
 * STRING,
 * DOUBLE,
 * INTEGER,
 * IMAGE,
 * INTERNAL_LINK, URL,
 * RANK,
 * DATE,
 * DATE_DAY,
 * DATE_MONTH,
 * DATE_YEAR,
 * MONEY,
 * LARGE_NUMBER,
 * YEAR_RANGE,
 * BIG_MONEY,
 * PERCENT,
 * UNIT,
 * UNDEFINED;
 */
public class DbPediaTripleLoaderApp extends App {


    @Override
    public void start() {


        try {

            Properties prop = App.getInstance().getConfigurationProperties();
            String dbPediaDump = prop.getProperty("dbpedia_dump");
            System.out.println("FILE: " + dbPediaDump);

            String limitStr = App.getInstance().getArgumentValue("limit");

            try{

                int limit = Integer.parseInt(limitStr);
                readFile(dbPediaDump, limit);

            }catch (NumberFormatException ne){
                System.out.println("\nERROR: You have to specify the --limit parameter !!!");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void readFile(String file, int limit) throws IOException, CompressorException {


        FileInputStream fin = new FileInputStream(file);
        BufferedInputStream bfin = new BufferedInputStream(fin);

        BufferedReader br = new BufferedReader(new InputStreamReader((bfin)));
        RelationsRepository<DbPediaTriple> repository = RelationsRepositoryFactory.getInstance().getDbPediaRepository();


        String line, subject, predicate, object, type;
        int i = 0;
        String pattern = "((?i)(<http://dbpedia.org/(.+?)/(.*?))(>))";

        String patternInit = "(.+?) (.+?) (((\"(.+?)\")(\\^\\^<(.+)>)?)|((?i)(<http://dbpedia.org/(.+?)/(.*?))(>)))";

        int startFrom = 0;

        do{

            line = br.readLine();

            logger.debug("Line: " + i + ", " + (startFrom+ limit));

            if (i < startFrom) {
                logger.debug("Skip: " + line);
                i++;
                continue;
            }


            Pattern p = Pattern.compile(patternInit);
            Matcher m = p.matcher(line);

            if (m.find()) {
                DbPediaTriple tripla = new DbPediaTriple();

                subject = m.group(1).replaceAll(pattern, "$4");
                predicate = m.group(2).replaceAll(pattern, "$4");

                String myObject = m.group(3);

                Pattern pp = Pattern.compile("((\"(.+?)\")(\\^\\^<http://www.w3.org/2001/XMLSchema#(.+)>)?)");
                Matcher mm = pp.matcher(myObject);

                if (mm.find()) {
                    object = mm.group(3);
                    type = mm.group(5);

                    if (type == null)
                        type = "STRING";

                } else {
                    type = "RESOURCE";
                    object = myObject.replaceAll(pattern, "$4");

                }

                if (type.matches(".*(DAY|day|Day|MONTH|month|Month|YEAR|year|Year|DATE|date|Date).*")) {
                    type = "DATE";
                }
                if (type.equalsIgnoreCase("double")) {
                    type = "DOUBLE";
                }

                type = type.toUpperCase();

                tripla.setSubject(subject);
                tripla.setPredicate(predicate);
                tripla.setObject(object);
                tripla.setType(getType(type));

                System.out.println("[" + subject + " - " + predicate + " - " + object + " - " + type + "]");
                System.out.println(line);
                repository.save(tripla);
            }

            i++;

        }while (line!=null && i < (startFrom + limit));


        br.close();
    }


    public static DbPediaTriple.OBJECTY_TYPE getType(String type) {
        return DbPediaTriple.OBJECTY_TYPE.valueOf(type);
    }


    /**
     * Entry for the WuWeldApp
     *
     * @param args
     */
    public static void main(String args[]) {
        init(new DbPediaTripleLoaderApp(), args);
    }
}
