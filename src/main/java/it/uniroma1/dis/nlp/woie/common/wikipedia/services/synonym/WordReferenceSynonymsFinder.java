package it.uniroma1.dis.nlp.woie.common.wikipedia.services.synonym;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by kikkomep on 12/30/13.
 */
public class WordReferenceSynonymsFinder implements SynonymsFinder {

    private static final String wordReferenceAPI = "http://api.wordreference.com/0.8/93ca0/json/thesaurus/";

    private final Logger logger = Logger.getLogger(SynonymsFinder.class);


    public List<String> findSynonyms(String term) {

        ArrayList<String> result = new ArrayList();
        result.add(term);

        try {

            logger.debug("Connecting... " + wordReferenceAPI + term);

            URL apiUrl = new URL(wordReferenceAPI + term);
            HttpURLConnection conn = (HttpURLConnection) apiUrl.openConnection();

            String str;
            StringBuilder text = new StringBuilder();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
                while (reader.ready()) {
                    str = reader.readLine();
                    text.append(str);
                }
            }

            JSONObject jsonResponse = (JSONObject) JSONValue.parse(text.toString());
            JSONObject term0 = (JSONObject) jsonResponse.get("term0");
            JSONObject senses = (JSONObject) term0.get("senses");
            JSONObject mainSense = (JSONObject) senses.get("0");
            JSONObject synonyms = (JSONObject) mainSense.get("synonyms");
            Set synoSet = synonyms.keySet();

            for(Object obj:synoSet){
                JSONObject temp = (JSONObject) synonyms.get(obj.toString());
                String syn = (String) temp.get("synonym");
                result.add(syn.toLowerCase());
            }

            logger.debug("Connection to " + wordReferenceAPI + " OK");

        } catch (Exception ex) {
            logger.debug("Unable to find the synonyms in word reference for "+term);
        }

        return result;
    }
}
