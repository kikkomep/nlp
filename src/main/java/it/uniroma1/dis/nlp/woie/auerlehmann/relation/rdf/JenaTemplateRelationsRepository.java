package it.uniroma1.dis.nlp.woie.auerlehmann.relation.rdf;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.tdb.TDBFactory;
import it.uniroma1.dis.nlp.woie.App;
import it.uniroma1.dis.nlp.woie.common.relation.IRelationsRepository;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by kikkomep on 1/2/14.
 */
public class JenaTemplateRelationsRepository implements IRelationsRepository<Statement>{
    private static JenaTemplateRelationsRepository ourInstance = new JenaTemplateRelationsRepository();

    public static JenaTemplateRelationsRepository getInstance() {
        return ourInstance;
    }

    private JenaTemplateRelationsRepository() {

        Properties properties = App.getInstance().getConfigurationProperties();

        datasetPath = properties.getProperty("jena_dataset_path");
        dataset = TDBFactory.createDataset(datasetPath);
        model = dataset.getDefaultModel();
    }



    private Logger logger = Logger.getLogger(JenaTemplateRelationsRepository.class);
    private String datasetPath = "datasets/wikipedia" ;
    private Dataset dataset;
    private Model model;

    public Model getModel() {
        return model;
    }

    public Dataset newDataset(){
        return TDBFactory.createDataset(datasetPath);
    }

    public String getDatasetPath() {
        return datasetPath;
    }

    public Dataset getDataset() {
        return dataset;
    }

    @Override
    public List<String> findAllSubjects() {
        return null;
    }

    @Override
    public List<Statement> findAll() {
        ArrayList<Statement> list = new ArrayList<>();
        dataset.begin(ReadWrite.READ);
        Model m = model;
        StmtIterator it = m.listStatements();
        while(it.hasNext())
            list.add(it.nextStatement());

        dataset.end();
        return list;
    }

    @Override
    public List<Statement> findBySubject(String subject) {
        ArrayList<Statement> list = new ArrayList<>();
        dataset.begin(ReadWrite.READ);
        Model m = model;
        Resource r = m.getResource(subject);
        if(r!=null){
            StmtIterator it = r.listProperties();
            while(it.hasNext())
                list.add(it.nextStatement());
        }
        dataset.end();
        return list;
    }

    @Override
    public void save(Statement relation) {
        dataset.begin(ReadWrite.WRITE);
        model.add(relation);
        dataset.commit();
        dataset.end();
    }

    @Override
    public void delete(Statement relation) {
        dataset.begin(ReadWrite.WRITE);
        model.remove(relation);
        dataset.commit();
        dataset.end();
    }

    @Override
    public void deleteBySubject(String subject) {
        Resource r = model.getResource(subject);
        dataset.begin(ReadWrite.WRITE);
        if(r!=null)
            r.removeProperties();
        dataset.commit();
        dataset.end();
    }
}
