package it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.preprocessor;

import it.uniroma1.dis.nlp.woie.common.wikipedia.Wikipedia;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaCategory;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by kikkomep on 12/10/13.
 */
public class WikipediaTemplatePreprocessorParserResult {

    private StringBuilder templateBuffer = new StringBuilder();
    private StringBuilder textBuffer = new StringBuilder();
    private LinkedList<String> templates = new LinkedList<>();
    private HashSet<WikipediaCategory> categories = new HashSet<>();

    void appendToText(String text) {
        this.textBuffer.append(text);
    }

    void appendToTemplate(String template) {
        this.templateBuffer.append(template);
    }

    void addCategory(String fullyQualifiedName){
        this.categories.add(Wikipedia.getInstance().getPageCategory(fullyQualifiedName));
    }


    void closeTemplate() {
        if (this.templateBuffer.length() > 0) {
            this.templates.add(this.templateBuffer.toString());
            this.templateBuffer.setLength(0); // set length of buffer to 0
            this.templateBuffer.trimToSize(); // trim the underlying buffer
        }
    }

    public String getText() {
        return this.textBuffer.toString();
    }

    public List<String> getTemplates() {
        return this.templates;
    }

    public Set<WikipediaCategory> getCategories(){
        return this.categories;
    }
}
