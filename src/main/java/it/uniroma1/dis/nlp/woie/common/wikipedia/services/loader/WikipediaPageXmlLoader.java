package it.uniroma1.dis.nlp.woie.common.wikipedia.services.loader;


import java.io.*;
import java.sql.Date;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import info.bliki.wiki.dump.IArticleFilter;
import info.bliki.wiki.dump.Siteinfo;
import info.bliki.wiki.dump.WikiArticle;
import info.bliki.wiki.dump.WikiXMLParser;
import info.bliki.wiki.filter.Encoder;
import info.bliki.wiki.model.WikiModel;
import it.uniroma1.dis.nlp.woie.App;
import org.apache.log4j.Logger;

import it.uniroma1.dis.nlp.woie.common.wikipedia.*;
import org.xml.sax.SAXException;

public class WikipediaPageXmlLoader implements WikipediaPageLoader {


    private Logger logger = Logger.getLogger(WikipediaPageXmlLoader.class);

    private final String WIKI_DUMP_PATH;


    private HashMap<String, InputStream> inputStreamHashMap = new HashMap<>();

    public WikipediaPageXmlLoader() {
        Properties prop = App.getInstance().getConfigurationProperties();
        WIKI_DUMP_PATH = (String) prop.get("wiki_dump");
        logger.info("XmlDump Folder: " + WIKI_DUMP_PATH);
    }


    public void parse(IArticleFilter filter) throws IOException, SAXException {

        try {

            WikiXMLParser wxp = new WikiXMLParser(WIKI_DUMP_PATH, filter);
            wxp.parse();

        } catch (Exception e) {
            //e.printStackTrace();
        }
    }


    @Override
    public InputStream newInputStream(WikipediaPage page) throws WikipediaPageInputStreamNotAvailableException {
        if (page == null) throw new IllegalArgumentException();

        InputStream in = this.inputStreamHashMap.get(page.getTitle());
        if (in == null) {
            SinglePageFilter filter = new SinglePageFilter(this, page.getTitle());
            try {
                
                parse(filter);
                if (filter.getPage() == null)
                    throw new WikipediaPageInputStreamNotAvailableException();
                else in = this.inputStreamHashMap.get(page.getTitle());

            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                //e.printStackTrace();
            }
        }
        return in;
    }

    @Override
    public WikipediaPage loadByTitle(String pageTitle) throws WikipediaPageNotFoundException {

        logger.debug("Searching " + pageTitle + " ...");

        SinglePageFilter filter = new SinglePageFilter(this, pageTitle);
        try {
            parse(filter);
            if (filter.getPage() == null)
                throw new WikipediaPageNotFoundException();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            //e.printStackTrace();
        }

        return filter.getPage();
    }




    @Override
    public List<WikipediaPage> loadPages(int numberOfPages, int skipFirstPages) {

        ListPageFilter filter = new ListPageFilter(this, numberOfPages, skipFirstPages);
        logger.debug("PAges to Skip: " + skipFirstPages + " " + numberOfPages);

        try {
            parse(filter);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            //e.printStackTrace();
        }

        return filter.getPages();
    }

    @Override
    public List<WikipediaPage> loadPages(int numberOfPages, int skipFirstPages, boolean printSkips) {

        ListPageFilter filter = new ListPageFilter(this, numberOfPages, skipFirstPages, printSkips);
        logger.debug("PAges to Skip: " + skipFirstPages + " " + numberOfPages);

        try {
            parse(filter);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            //e.printStackTrace();
        }

        return filter.getPages();
    }

    @Override
    public List<WikipediaPage> loadPages(int numberOfPages) {

        ListPageFilter filter = new ListPageFilter(this, numberOfPages);
        try {
            logger.debug("Loading pages from dump file '" + WIKI_DUMP_PATH + "'");
            parse(filter);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            //e.printStackTrace();
        }

        return filter.getPages();
    }


    static class SinglePageFilter implements IArticleFilter {

        private Logger logger;
        private WikipediaPageXmlLoader loader;
        private String title;
        private WikipediaPage page;

        SinglePageFilter(WikipediaPageXmlLoader loader, String title) {
            this.title = title;
            this.loader = loader;
            this.logger = loader.logger;
        }

        public WikipediaPage getPage() {
            return page;
        }

        // Convert to plain text
        WikiModel wikiModel = new WikiModel("${image}", "${title}");


        public void process(WikiArticle page, Siteinfo siteinfo) throws SAXException {

            if (page != null) {

                logger.debug("Current Page: " + title);

                String title = Encoder.encodeTitleToUrl(page.getTitle(), true);
                if (page.getText() != null
                        && (this.title == null || title.equals(this.title))) {


                    try {


                        Long lastModified = Calendar.getInstance().getTime().getTime();
                        Pattern pattern = Pattern.compile("([^T])+");
                        Matcher matcher = pattern.matcher(page.getTimeStamp());
                        if (matcher.find()) {
                            String d = matcher.group(0);
                            lastModified = Date.valueOf(d).getTime();
                        }


                        this.page = new WikipediaPage(title, "http://en.wikipedia.org/wiki/", lastModified);
                        this.page.setPageLoader(this.loader);


                        InputStream in = new ByteArrayInputStream(page.getText().getBytes());
                        this.loader.inputStreamHashMap.put(title, in);


                    } catch (Exception ex) {
                    }

                    throw new SAXException("FINISHED");
                }
            }
        }
    }


    static class ListPageFilter implements IArticleFilter {

        private Logger logger;
        private int count = 0;
        private int skipFirst, limit;
        private boolean printSkips=false;
        private WikipediaPageXmlLoader loader;
        private List<WikipediaPage> pages = new ArrayList<>();

        ListPageFilter(WikipediaPageXmlLoader loader, int limit) {
            this.loader = loader;
            this.limit = limit;
            this.skipFirst = 0;
            this.logger = loader.logger;
        }

        ListPageFilter(WikipediaPageXmlLoader loader, int limit, int skipFirst) {
            this.loader = loader;
            this.limit = limit;
            this.skipFirst = skipFirst;
            this.logger = loader.logger;
        }

        ListPageFilter(WikipediaPageXmlLoader loader, int limit, int skipFirst, boolean printSkips) {
            this.loader = loader;
            this.limit = limit;
            this.skipFirst = skipFirst;
            this.printSkips = printSkips;
            this.logger = loader.logger;
        }

        public List<WikipediaPage> getPages() {
            return pages;
        }

        // Convert to plain text
        WikiModel wikiModel = new WikiModel("${image}", "${title}");

        public void process(WikiArticle page, Siteinfo siteinfo) throws SAXException {

            if(page==null) return;

            String title = Encoder.encodeTitleToUrl(page.getTitle(), true);

            if (count < skipFirst) {

                count++;
                if(printSkips) logger.info("\t - Skipped page [" + count + "]: " + title);
                else logger.debug("\t - Skipped page [" + count + "]: " + title);

            } else {

                if (page.getText() != null) {

                    Long lastModified = Calendar.getInstance().getTime().getTime();
                    Pattern pattern = Pattern.compile("([^T])+");
                    Matcher matcher = pattern.matcher(page.getTimeStamp());
                    if (matcher.find()) {
                        String d = matcher.group(0);
                        lastModified = Date.valueOf(d).getTime();
                    }


                    WikipediaPage pg = new WikipediaPage(title, "http://en.wikipedia.org/wiki/" + title, lastModified);

                    InputStream in = new ByteArrayInputStream(page.getText().getBytes());
                    this.loader.inputStreamHashMap.put(title, in);

                    pg.setPageLoader(this.loader);

                    this.pages.add(pg);

                    count++;

                    if ((count - skipFirst + 1) > this.limit)
                        throw new SAXException("FINISHED");
                }
            }
        }
    }
}
