package it.uniroma1.dis.nlp.woie.wuweld.pipeline.filters;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.trees.*;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.Match;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.ParsedSentence;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.WikipediaPagePipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by lgu on 06/02/14.
 */
public class ThirdFilter {

    private static final Logger logger = LoggerFactory.getLogger(it.uniroma1.dis.nlp.woie.wuweld.pipeline.Pipeline.class);

    public static boolean isApplicable(ParsedSentence sentence,GrammaticalStructureFactory grammaticalStructureFactory,Match matchCandidate){
        logger.debug("Third Filtering");
        SemanticGraph sg = sentence.getSemanticGraphTypedCCprocessed(grammaticalStructureFactory);

        boolean applyThirdFilter = true;

        for (SemanticGraphEdge o : sg.getEdgeSet()) {
            if (verifyClause(sg, o, matchCandidate.getSubject(), matchCandidate.getObject())
                    || verifyClause(sg, o, matchCandidate.getObject(), matchCandidate.getSubject())) {

                logger.debug("Primary Entity (" + matchCandidate.getSubject() + ") and attribute value (" + matchCandidate.getObject() + ")");
                applyThirdFilter = false;
                break;
            }
        }

        if(applyThirdFilter) logger.debug("Third Filter Applied!!");

        return applyThirdFilter;
    }

    private static boolean verifyClause(SemanticGraph sg, SemanticGraphEdge o, IndexedWord subj, IndexedWord obj) {

        return o.getRelation().toString().matches(WikipediaPagePipeline.FIRST_EDGE_PATTERN)
                && subj.equals(o.getDependent())
                && (obj.equals(o.getSource()) || sg.getChildren(o.getSource()).contains(obj));

    }
}
