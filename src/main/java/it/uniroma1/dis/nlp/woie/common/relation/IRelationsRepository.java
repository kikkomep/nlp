package it.uniroma1.dis.nlp.woie.common.relation;

import java.util.List;

/**
 * Created by kikkomep on 1/2/14.
 */
public interface IRelationsRepository<T> {

    public List<String> findAllSubjects();

    List<T> findAll();

    List<T> findBySubject(String subject);

    void save(T relation);

    void delete(T relation);

    void deleteBySubject(String subject);
}
