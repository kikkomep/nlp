package it.uniroma1.dis.nlp.woie.common.wikipedia.services.synonym;

import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaWord;
import it.uniroma1.dis.nlp.woie.utils.AppEntityManagerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by kikkomep on 12/14/13.
 */
public class WikipediaWordRepository {


    private final EntityManager em;

    private final static WikipediaWordRepository instance = new WikipediaWordRepository();

    private WikipediaWordRepository(){
        this.em = AppEntityManagerFactory.getInstance().getEntityManager(AppEntityManagerFactory.NAME.wws);
    }


    public static WikipediaWordRepository getInstance(){
        return instance;
    }

    public List<WikipediaWord> findAll() {

        List<WikipediaWord> list = null;


        try {

            Query q = em.createQuery(
                    "SELECT w FROM  WikipediaWord w");
            list = q.getResultList();

        } catch (Exception e) {
        }

        return list;
    }


    public WikipediaWord findByText(String text) {

        WikipediaWord result = null;

        try {

            Query q = em.createQuery(
                    "SELECT w FROM WikipediaWord w WHERE w.text = :text");
            q.setParameter("text", text);
            result = (WikipediaWord) q.getSingleResult();

        } catch (Exception e) {
        }

        return result;
    }


    public void save(WikipediaWord word) {

        em.getTransaction().begin();

        try {

            if(word.getId()==null)
                em.persist(word);
            else { em.merge(word);}

            em.flush();
            em.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
    }

    public void delete(WikipediaWord word) {

        em.getTransaction().begin();

        try {
            em.remove(word);
            em.flush();
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
    }


    public void delete(String wordText) {

        try {

            WikipediaWord word = findByText(wordText);
            delete(word);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}