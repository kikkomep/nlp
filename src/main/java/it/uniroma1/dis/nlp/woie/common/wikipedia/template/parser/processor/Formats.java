package it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.processor;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by kikkomep on 1/27/14.
 */
public class Formats {


    public static String toInteger(String value) {
        return toLargeNumber(value);
    }

    public static String toDouble(String value) {
        return toLargeNumber(value);
    }

    public static String toMoney(String value) {
        String d = value.trim();

        d = d.replace("$", "");

        boolean million = d.matches("(?i)(.*?)million");
        boolean billion = d.matches("(?i)(.*?)billion");

        if (million || billion) {

            d = d.replaceAll("((?i)([[)?[mb]illion(]])?)", "").trim();

            NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
            try {
                Double dv = nf.parse(d).doubleValue();
                if (million)
                    dv *= 1000000;
                else if (billion)
                    dv *= 1000000000;

                d = String.format(Locale.ENGLISH, "%1$,.2f", dv);
            } catch (java.text.ParseException ex) {
                ex.printStackTrace();
            }
        }

        return d;
    }


    private static String toLargeNumber(String value) {
        String d = value.trim();

        boolean million = d.matches("(?i)(.*?)million");
        boolean billion = d.matches("(?i)(.*?)billion");

        if (million || billion) {

            d = d.replaceAll("((?i)([[)?[mb]illion(]])?)", "").trim();

            NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
            try {

                Long i = nf.parse(d).longValue();
                if (million)
                    i *= 1000000;
                else if (billion)
                    i *= 1000000000;

                d = String.format(Locale.ENGLISH, "%d", i);

            } catch (java.text.ParseException ex) {
                ex.printStackTrace();
            }
        }

        return d;
    }
}
