package it.uniroma1.dis.nlp.woie.common.relation;

/**
 * Created by kikkomep on 12/27/13.
 */
public interface RelationWriter<T> {

    public void open();

    public T write(Relation relation);

    public void close();

    public IRelationsRepository getRepository();
}
