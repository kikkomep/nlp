package it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.preprocessor;

import java.util.regex.Pattern;

/**
 * Created by kikkomep on 12/31/13.
 */
public class TagFilter {


    public static boolean toSkip(String tag){

        boolean result = false;

        Pattern p = Pattern.compile("ref|br|p");

        if(tag!=null && p.matcher(tag).find())
            result = true;

        return result;
    }
}
