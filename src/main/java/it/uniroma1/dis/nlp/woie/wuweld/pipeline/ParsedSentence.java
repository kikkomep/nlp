package it.uniroma1.dis.nlp.woie.wuweld.pipeline;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;

import java.util.*;

import it.uniroma1.dis.nlp.woie.wuweld.woe.pattern.GeneralizedCorePath;
import it.uniroma1.dis.nlp.woie.wuweld.woe.pattern.GeneralizedCorePathsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Antonio
 */
public class ParsedSentence {

    private final Logger logger = LoggerFactory.getLogger(ParsedSentence.class.getName());
    private Annotation annotation;
    private String sentence;
    private Set<Entity> primaryEntityMatchings;
    private Set<Match> matchings;

    public ParsedSentence(Annotation annotation, String sentence) {
        this.annotation = annotation;
        this.sentence = sentence;
        this.primaryEntityMatchings = new HashSet<>();
        this.matchings = new HashSet<>();
    }

    public Set<Entity> getPrimaryEntityMatchings() {
        return primaryEntityMatchings;
    }

    public Set<Match> getAttributeMatchings() {
        return matchings;
    }

    public Annotation getAnnotation() {
        return annotation;
    }

    public String getSentence() {
        return sentence;
    }

    public void addTitleMatch(Entity m) {
        this.primaryEntityMatchings.add(m);
    }

    public void addAttributeMatch(Match m) {
        this.matchings.add(m);
    }

    public boolean matched() {
        return !this.matchings.isEmpty();
    }

    public void saveMatchings() {

        if (this.matchings.isEmpty()) {

            return;
        }

        GeneralizedCorePathsRepository pr = GeneralizedCorePathsRepository.getInstance();

        for (Match m : this.matchings) {

            pr.save(new GeneralizedCorePath(m.getGeneralizedCorePath()));
            logger.debug("");

        }
    }

    public List<String> getMatchings() {
        LinkedList<String> result = new LinkedList<>();

        for(Match m : this.matchings){
            result.add(m.getGeneralizedCorePath());
        }

        return result;
    }

    private boolean sgCCProcessedCalculated = false;
    private SemanticGraph sgCCProcessed;

    public SemanticGraph getSemanticGraphTypedCCprocessed(GrammaticalStructureFactory grammaticalStructureFactory){

        if(sgCCProcessedCalculated) return sgCCProcessed;

        GrammaticalStructure gs = grammaticalStructureFactory.newGrammaticalStructure(annotation.get(CoreAnnotations.SentencesAnnotation.class).get(0).get(TreeCoreAnnotations.TreeAnnotation.class));
        Collection<TypedDependency> tdl = gs.typedDependenciesCCprocessed(true);
        sgCCProcessed = new SemanticGraph(tdl);
        sgCCProcessedCalculated = true;

        return sgCCProcessed;
    }

    private boolean sgCalculated = false;
    private SemanticGraph sg;

    public SemanticGraph getSemanticGraph(GrammaticalStructureFactory grammaticalStructureFactory){

        if(sgCalculated) return sg;

        GrammaticalStructure gs = grammaticalStructureFactory.newGrammaticalStructure(annotation.get(CoreAnnotations.SentencesAnnotation.class).get(0).get(TreeCoreAnnotations.TreeAnnotation.class));
        Collection<TypedDependency> tdl = gs.typedDependencies(true);
        sg = new SemanticGraph(tdl);
        sgCalculated = true;

        return sg;
    }

    public CoreMap getPrincipalCoreMap(){
        return annotation.get(CoreAnnotations.SentencesAnnotation.class).get(0);
    }
}
