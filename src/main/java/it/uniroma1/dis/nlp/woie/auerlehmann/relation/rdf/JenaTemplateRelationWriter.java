package it.uniroma1.dis.nlp.woie.auerlehmann.relation.rdf;

import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.datatypes.xsd.impl.*;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.*;
import it.uniroma1.dis.nlp.woie.auerlehmann.relation.plain.TemplateRelation;
import it.uniroma1.dis.nlp.woie.common.relation.Relation;
import it.uniroma1.dis.nlp.woie.common.relation.RelationWriter;
import it.uniroma1.dis.nlp.woie.common.wikipedia.utils.WikipediaUtils;
import org.apache.log4j.Logger;

/**
 * Created by kikkomep on 1/2/14.
 */
public class JenaTemplateRelationWriter implements RelationWriter<Statement> {

    private Logger logger = Logger.getLogger(JenaTemplateRelationWriter.class);

    private Dataset dataset;
    private Model model;


    @Override
    public void open() {
        dataset = JenaTemplateRelationsRepository.getInstance().getDataset();
        model = JenaTemplateRelationsRepository.getInstance().getModel();
    }

    @Override
    public void close() {
        this.model.commit();
    }



    public Model getModel() {
        return model;
    }

    @Override
    public Statement write(Relation relation) {

        String resourceName = "http://en.wikipedia.org/wiki/" + relation.getSubject();

        dataset.begin(ReadWrite.WRITE);

        Resource subj = getResource(resourceName);


        Property p = model.createProperty(relation.getPredicate());

        RDFNode obj = null;


        logger.debug("\t- OBJTYPE: " + ((TemplateRelation) relation).getObjectType() + " --- " + getObjectType(((TemplateRelation) relation).getObjectType()));


        TemplateRelation.OBJECTY_TYPE type = ((TemplateRelation) relation).getObjectType();

        if(type.equals(TemplateRelation.OBJECTY_TYPE.INTERNAL_LINK)){

            String objResource = WikipediaUtils.resolveInterwikiLink(relation.getObject());
            obj = getResource(objResource);

            logger.debug("\t- INTER_LINK: " + obj + " " + objResource);

        }else if(type.equals(TemplateRelation.OBJECTY_TYPE.IMAGE)){

            String objResource = WikipediaUtils.resolveImageLink(relation.getObject());
            obj = getResource(objResource);

            logger.debug("\t- IMAGE: " + obj + " " + objResource);

        }else{

            obj = model.createTypedLiteral(relation.getObject(), getObjectType(type));
        }


        Resource r = subj.addProperty(p, obj);




        model.commit();

        dataset.end();

        Statement result = null;
        StmtIterator iterator = subj.listProperties();
        while(iterator.hasNext()){
            Statement statement = iterator.nextStatement();
            if(statement.getObject().equals(obj)){
                result = statement;
                break;
            }
        }

        return result;
    }

    private Resource getResource(String uri){
        Resource resource = model.getResource(uri);
        if(resource==null)
            resource = model.createResource(uri);
        return resource;
    }


    @Override
    public JenaTemplateRelationsRepository getRepository() {
        return JenaTemplateRelationsRepository.getInstance();
    }

    private RDFDatatype getObjectType(TemplateRelation.OBJECTY_TYPE objectType){
        RDFDatatype type = null;

        switch (objectType){
            case RESOURCE: type = XSDDatatype.XSDanyURI; break;
            case STRING: type = XSDDatatype.XSDstring; break;
            case DOUBLE: type = XSDDatatype.XSDdouble; break;
            case LARGE_DOUBLE: type = XSDDatatype.XSDdouble; break;
            case INTEGER: type = XSDDatatype.XSDinteger; break;
            case LARGE_INTEGER: type = XSDDatatype.XSDinteger; break;
            case URL: type = XSDDatatype.XSDanyURI; break;
            case INTERNAL_LINK: type = XSDDatatype.XSDanyURI; break;
            case IMAGE: type = XSDDatatype.XSDanyURI; break;
            case RANK: type = XSDDatatype.XSDstring; break;
            case DATE: type = XSDDatatype.XSDdate; break;
            case DATE_DAY: type = XSDDayType.XSDinteger; break;
            case DATE_MONTH: type = XSDMonthType.XSDinteger; break;
            case DATE_YEAR: type = XSDYearType.XSDinteger; break;
            case MONEY: type = XSDDatatype.XSDdecimal; break;
            case YEAR_RANGE: type = XSDDatatype.XSDduration; break;
            case LARGE_NUMBER: type = XSDBaseNumericType.XSDinteger; break;
            case BIG_MONEY: type = XSDDatatype.XSDdecimal; break;
            case PERCENT: type = XSDDouble.XSDdecimal; break;
            case UNIT: type = XSDDatatype.XSDtoken; break;
            default: type = XSDDatatype.XSDtoken; break;
        }

        return type;
    }
}
