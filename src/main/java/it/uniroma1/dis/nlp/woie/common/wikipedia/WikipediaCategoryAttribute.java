package it.uniroma1.dis.nlp.woie.common.wikipedia;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * Created by kikkomep on 12/18/13.
 */
@Entity
public class WikipediaCategoryAttribute implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @Lob
    protected String name;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    protected WikipediaCategory category;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoryAttribute")
    protected Set<WikipediaCategoryAttributeCorePath> corePaths = new HashSet<>();

    protected WikipediaCategoryAttribute() {
    }

    WikipediaCategoryAttribute(String name, WikipediaCategory category) {
        this.name = name;
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WikipediaCategory getCategory() {
        return category;
    }

    public WikipediaCategoryAttributeCorePath addCorePath(String path){
        WikipediaCategoryAttributeCorePath attributeCorePath = null;
        for(WikipediaCategoryAttributeCorePath attr : this.getCorePaths()){
            if(attr.getPath().equals(path)){
                attributeCorePath = attr;
                break;
            }
        }

        if(attributeCorePath == null){
            attributeCorePath = new WikipediaCategoryAttributeCorePath(this, path);
            this.corePaths.add(attributeCorePath);
        }

        attributeCorePath.increaseFrequencyByOne();

        return attributeCorePath;
    }


    public void removeCorePath(WikipediaCategoryAttributeCorePath corePath){
        if(corePath!=null && this.corePaths.contains(corePath)){
            this.corePaths.remove(corePath);
            corePath.setCategoryAttribute(null);
        }
    }


    public Set<WikipediaCategoryAttributeCorePath> getCorePaths(){
        return Collections.unmodifiableSet(this.corePaths);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WikipediaCategoryAttribute)) return false;

        WikipediaCategoryAttribute attribute = (WikipediaCategoryAttribute) o;

        if (id != null ? !id.equals(attribute.id) : attribute.id != null) return false;
        if (!name.equals(attribute.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "WikipediaCategoryAttribute{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category=" + category +
                ", corePaths=" + corePaths +
                '}';
    }
}
