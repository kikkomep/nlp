package it.uniroma1.dis.nlp.woie.wuweld.pipeline;

/**
 *
 * @author lgu
 */
public class InformationTriple {

    private String subject;
    private String relation;
    private String object;
    private String dependencyPath;

    public InformationTriple(String s, String r, String o) {
        this.subject = s;
        this.object = o;
        this.relation = r;
    }

    public InformationTriple(String s, String r, String o,String d) {
        this.subject = s;
        this.object = o;
        this.relation = r;
        this.dependencyPath = d;
    }

    public String getSubject() {
        return subject;
    }

    public String getRelation() {
        return relation;
    }

    public String getObject() {
        return object;
    }

    @Override
    public String toString() {
        //return "InformationTriple{" + "subject=" + subject + ", relation=" + relation + ", object=" + object + '}';
        if(dependencyPath==null){
            return subject+" , "+relation+" , "+object;
        }else{
            return subject+" , "+relation+" , "+object+"\n"+dependencyPath;
        }

    }
}
