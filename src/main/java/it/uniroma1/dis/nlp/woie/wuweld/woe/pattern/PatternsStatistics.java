package it.uniroma1.dis.nlp.woie.wuweld.woe.pattern;

import java.util.List;

/**
 * Created by kikkomep on 12/11/13.
 */
public class PatternsStatistics {

    private List<PatternStatistics> statistics;

    PatternsStatistics(List<PatternStatistics> statistics) {
        this.statistics = statistics;
    }

    public List<PatternStatistics> getStatistics() {
        return statistics;
    }

    public int getNumberOfPatterns(){
        return this.statistics.size();
    }

    public PatternStatistics getMostFrequentPattern(){
        if(this.statistics.size()>0)
            return this.statistics.get(0);
        return null;
    }

    public Long getNumberOfPatternOccurencies(){
        Long result = 0L;
        for(PatternStatistics p : this.statistics)
            result+= p.getOccurrences();
        return result;
    }

    public PatternStatistics getPatternStatistics(String pattern){
        PatternStatistics result = null;
        for(PatternStatistics p : this.statistics){
            if(p.getPath().equals(pattern)){
                result = p;
                break;
            }
        }
        return result;
    }

    public double getNormalizedLogFrequency(String pattern){

        double result = -1;

        PatternStatistics mostFrequent = this.getMostFrequentPattern();
        PatternStatistics p = this.getPatternStatistics(pattern);

        double fp = p.getOccurrences();

        double fmin = 1;

        double fmax = mostFrequent.getOccurrences();


        double num = max(Math.log(fp) - Math.log(fmin), 0);
        double den = Math.log(fmax) - Math.log(fmin);

        result = num / den;

        return result;
    }

    @Override
    public String toString() {
        return "PatternsStatistics{" +
                "number of patterns: " + this.getNumberOfPatterns() + ", " +
                "most frequent: " + this.getMostFrequentPattern() +
                "}";
    }


    private static double max(double x, double y){
        return x>=y ? x : y;
    }
}
