package it.uniroma1.dis.nlp.woie.common.wikipedia.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Antonio
 */
public class PronounCounter {

    private static final String[] pronouns = {"i","you","it", "he","she","we","they"};

    private static Map<String, Integer> maps = new HashMap<>();

    public static String countMatch(String regEx, String Txt) {

        Txt = Txt.toLowerCase();
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(Txt);
        int max = 0;

        initMap();

        List<String> entity = new ArrayList<>();

        while (m.find()) {
            if (maps.get(m.group(0)) != null) {
                //System.out.println(m.group(0));
                maps.put(m.group(0), maps.get(m.group(0)) + 1);
            } else {
                maps.put(m.group(0), 1);
            }
        }

        sortMaps(maps);
        //System.out.println("----------");


        
        for (String key : maps.keySet()) {
            if (maps.get(key) > max) {
                max = maps.get(key);
            }

            if (maps.get(key) == max) {

                entity.add(key);

            }
        }
        if (entity.size() == 1) {
            entity.get(0);
            if (!entity.get(0).equals("it")) {
                //System.out.println(maps.keySet());
                return entity.get(0).toString();
            } else {
                return "";
            }
        }

        return "";
    }

    public static void sortMaps(Map<String, Integer> map) {

        PositionComparator pc = new PositionComparator(map);
        TreeMap<String, Integer> sorted_map = new TreeMap<>(pc);
        sorted_map.putAll(map);

        maps = sorted_map;

        //System.out.println(maps);
    }

    public static String pronounCount(String plainText) {
        int count = 0;

        //maps.clear();
        maps = new HashMap<>();

        StringBuilder regEx = new StringBuilder();
        for(String prn : pronouns){
            count++;
            regEx.append("\\b" + prn + "\\b");
            if(count<pronouns.length) regEx.append("|");
        }
        //String regEx = "\\bi\\b|\\byou\\b|\\bit\\b|\\bhe\\b|\\bshe\\b|\\bwe\\b|\\bthey\\b";
        return countMatch(regEx.toString(), plainText);
    }

    private static void initMap(){

        for(String prn : pronouns)
            maps.put(prn, 0);
    }
}
