package it.uniroma1.dis.nlp.woie.lamepan;

import it.uniroma1.dis.nlp.woie.App;
import it.uniroma1.dis.nlp.woie.common.wikipedia.*;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.*;

import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by kikkomep on 11/02/14.
 */
public class LaMepAnTestReportApp extends App {


    @Override
    public void start() {

        Wikipedia wikipedia = Wikipedia.getInstance();

        try {

            boolean onlyWithResults = true;
            int extracted = 0, processed = 0;
            int limit = Integer.parseInt(getArgumentValue("limit"));
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd@hh.mm.ss");
            String dateString = dt.format(new Date());
            String pathName = "logs" + File.separator + "lamepan_results" + File.separator + dateString;

            File path = new File(pathName);
            if (!path.exists()) path.mkdirs();

            List<WikipediaPage> pages = wikipedia.findAll();
            for (WikipediaPage page : pages) {

                try {

                    FindAttributePipeline fap = new FindAttributePipeline(page);
                    fap.process();

                    processed++;
                    logger.info("\n\n *** Checking page #" + processed + ":" + page.getTitle() + " *** \n\n");

                    Map<String, Set<String>> result = fap.getResults();

                    if (result != null) {

                        if ((!onlyWithResults || result.size() > 0)) {



                            StringBuilder builder = new StringBuilder();



                            int foundAttributes = 0;

                            builder.append("- Page: " + page.getTitle() + "\n");
                            builder.append("- EntityType: " + fap.getPrimaryEntityType() + "\n");

                            for (String name : result.keySet()) {
                                for (String value : result.get(name)) {
                                    builder.append("\t -> " + name + ": " + value + "\n");
                                    foundAttributes++;
                                }
                            }


                            if (foundAttributes>0) {

                                extracted++;

                                logger.info("Selected with #" + foundAttributes + " attributes");

                                path = new File(pathName + File.separator + extracted);
                                if (!path.exists()) path.mkdirs();

                                File file = new File(pathName + File.separator + extracted + File.separator + "extraction.txt");

                                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));

                                logger.info(builder.toString());
                                out.write(builder.toString());

                                WikipediaEntityCategory category = null;
                                if (fap.isPrimaryEntityTypeMatched()) {
                                    category = wikipedia.findEntityCategory(fap.getPrimaryEntityType());
                                    out.write("\n\n\n");
                                    out.write("\n ------------- ---------------- ----------------------------");
                                    out.write("\n -------------   Primary Type   ----------------------------");
                                    out.write("\n ------------- ---------------- ----------------------------");
                                    out.write("\n\n\n");

                                    writeCategoryInfo(out, category);
                                }

                                out.write("\n\n\n");
                                out.write("\n ------------- ---------------- ----------------------------");
                                out.write("\n ------------- Other categories ----------------------------");
                                out.write("\n ------------- ---------------- ----------------------------");
                                out.write("\n\n\n");

                                for (WikipediaEntityCategory category1 : page.getEntityCategories()) {
                                    if (category == null || category.getId() != category1.getId())
                                        writeCategoryInfo(out, category1);
                                }

                                out.close();

                                file = new File(pathName + File.separator + extracted + File.separator + page.getTitle() + ".txt");
                                Files.copy(page.getInputStream(), file.toPath());

                            }

                            if (extracted == limit) break;

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        } catch (Exception ex) {
            logger.info("You have to specify the number of pages to extract: --limit [NUM_OF_PAGES]");
            ex.printStackTrace();
        }
    }


    private void writeCategoryInfo(Writer out, WikipediaEntityCategory category) throws IOException {
        out.write("\n\n *** Category Info: " + category.getName() + "****\n\n");
        for (WikipediaCategoryAttribute attribute : category.getAttributes()) {
            out.write("\n\n\t - ATTRIBUTE: " + attribute.getName());
            out.write("\n\t\t * CORE PATHs *");
            for (WikipediaCategoryAttributeCorePath corePath : attribute.getCorePaths()) {
                out.write("\n\t\t   - " + corePath.getPath());
                out.write("\n\t\t   - SENSEs: ");
                for (String sense : corePath.getGovernorSenses()) {
                    out.write("\n\t\t\t -> " + sense + ": LEMMAs: " + corePath.getGovernorSenseLemmas(sense));
                }
            }
        }
    }


    public static void main(String args[]) {
        init(new LaMepAnTestReportApp(), args);
    }
}
