package it.uniroma1.dis.nlp.woie.wuweld;

import it.uniroma1.dis.nlp.woie.App;

import java.io.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by kikkomep on 1/27/14.
 */
public class TrainingSetHistory {



    private final String TRAINING_HISTORY;
    private HashSet<String> history = new HashSet<>();


    private static TrainingSetHistory instance = null;

    public static TrainingSetHistory getInstance(){
        if(instance==null)
            instance = new TrainingSetHistory();
        return instance;
    }

    private TrainingSetHistory(){
        TRAINING_HISTORY = App.getInstance().getConfigurationProperties().getProperty("training_pages_history");

        File data = new File(TRAINING_HISTORY);
        if (data.exists()) {
            try {

                ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(data));

                history = (HashSet<String>) inputStream.readObject();

                inputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        } else {
            history = new HashSet<String>();
        }
    }


    public Set<String> getProcessedPages(){
        return Collections.unmodifiableSet(this.history);
    }



    public void addProcessedPage(String pageTitle){
        this.history.add(pageTitle);
    }


    public void save(){
        ObjectOutputStream outputStream = null;
        try {
            outputStream = new ObjectOutputStream(new FileOutputStream(TRAINING_HISTORY));
            outputStream.writeObject(history);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int countProcessedPages() {
        return this.history.size();
    }
}
