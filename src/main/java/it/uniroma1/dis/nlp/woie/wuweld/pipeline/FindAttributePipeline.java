package it.uniroma1.dis.nlp.woie.wuweld.pipeline;

import edu.stanford.nlp.semgraph.SemanticGraph;
import it.uniroma1.dis.nlp.woie.common.wikipedia.*;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.services.*;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaCategoryAttributeCorePath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;


/**
 * @author lgu
 */
public class FindAttributePipeline extends WikipediaPagePipeline {

    protected final Logger logger = LoggerFactory.getLogger(FindAttributePipeline.class);
    private HashMap<String, Set<String>> result;
    private boolean appearPrimaryEntity = false;
    private boolean belongToACategoryContaingAtLeastAnAttribute = false;
    private boolean belongToACategoryContaingAtLeastAnAttributeWithAttributeCorePath = false;

    public FindAttributePipeline(WikipediaPage wikipediaPage) throws WikipediaPageInputStreamNotAvailableException, IOException, ParseException {
        super(wikipediaPage);
        result = new HashMap<>();
    }

    @Override
    public void process() {

        //split and annotate sentences
        super.initializePipeline(false);

        //create the dependencyPath for finding title words and its synonyms
        Pattern titleSynonymsPattern = WikipediaUtils.createSynonymsRegEx(title);

        logger.debug("Title: " + title);
        logger.debug("Title Pattern: " + titleSynonymsPattern.pattern());

        //getting the sentence that contains the primary entity
        LinkedList<ParsedSentence> primaryEntitySentences = new LinkedList<>();
        for (ParsedSentence sentence : super.parsedSentences) {
            if (super.primaryEntitiesMatching(sentence, titleSynonymsPattern)) {
                appearPrimaryEntity = true;
                primaryEntitySentences.add(sentence);
            }
        }

        System.out.println(primaryEntityTypeMatched + " -- " + primaryEntityType);

        //add recognized category
        if (primaryEntityTypeMatched && primaryEntityType != null) {
            page.addCategory(Wikipedia.getInstance().getEntityCategory(primaryEntityType));
        }

        //get the class of the page
        for (WikipediaCategory wc : super.page.getEntityCategories()) {

            logger.debug("\n\n************************************* ");
            logger.debug("\n\n***** Page Category: " + wc.getName());
            logger.debug("\n\n************************************* ");


            //get the list of attributePaths associated with category wc
            for (WikipediaCategoryAttribute wca : wc.getAttributes()) {

                belongToACategoryContaingAtLeastAnAttribute = true;
                logger.debug("\n\n===> Attribute: " + wca.getName() + "\n");

                for (WikipediaCategoryAttributeCorePath acp : wca.getCorePaths()) {

                    logger.debug("Path: " + acp.getPath());
                    belongToACategoryContaingAtLeastAnAttributeWithAttributeCorePath = true;
                    for (ParsedSentence sentence : primaryEntitySentences) {

                        //try to apply the attribute path and get words associated with the attribute
                        Set<String> attributeValues = applyAttributePath(sentence, acp);

                        //Adding the result
                        if (this.result.containsKey(wca.getName())) {
                            this.result.get(wca.getName()).addAll(attributeValues);
                        } else {
                            this.result.put(wca.getName(), attributeValues);
                        }

                    }
                }
                logger.debug("");
            }
        }
    }

    private Set<String> applyAttributePath(ParsedSentence sentence, WikipediaCategoryAttributeCorePath attributeCorePath) {

        SemanticGraph semanticGraph = sentence.getSemanticGraphTypedCCprocessed(grammaticalStructureFactory);
        Set<PathCandidate> pathCandidates = PathMatcher.findMatchingPathCandidates(attributeCorePath.getPath(), semanticGraph);
        Set<String> result = new HashSet<>();

        for (PathCandidate pathCandidate : pathCandidates) {
            if (checkCandidate(pathCandidate, attributeCorePath, semanticGraph)) {
                WordPathExpander wpe =
                        new WordPathExpander(sentence.getSemanticGraph(grammaticalStructureFactory),pathCandidate.getObj());
                result.add(wpe.getExpandedWordString());
            }
        }

        return result;
    }

    private static boolean checkCandidate(PathCandidate pathCandidate, WikipediaCategoryAttributeCorePath attributeCorePath, SemanticGraph semanticGraph) {

        boolean existsSimilarity = false;

        ClauseRelationDisambiguation clauseRelationDisambiguation =
                new ClauseRelationDisambiguation(semanticGraph, pathCandidate);

        try {
            String candidateRelationSense = clauseRelationDisambiguation.disambiguateRelation();

            if (attributeCorePath.isDisambiguated() && attributeCorePath.containsGovernorSense(candidateRelationSense)) {
                existsSimilarity = true;
            }
        } catch (ParseException e) {
        }

        // If BabelNet doesn't find similarity
        if (!existsSimilarity) {
            for (String governorSense : attributeCorePath.getGovernorSenses()) {
                for (String lemma : attributeCorePath.getGovernorSenseLemmas(governorSense)) {
                    if (WordNetSimilarity.areSimilar(pathCandidate.getRelation().lemma(), lemma)) {
                        existsSimilarity = true;
                        break;
                    }
                }

                if (existsSimilarity) break;
            }
        }

        return existsSimilarity;
    }


    public HashMap<String, Set<String>> getResults() {
        return this.result;
    }

    public boolean isAppearPrimaryEntity() {
        return appearPrimaryEntity;
    }

    public boolean isBelongToACategoryContaingAtLeastAnAttribute() {
        return belongToACategoryContaingAtLeastAnAttribute;
    }

    public boolean isBelongToACategoryContaingAtLeastAnAttributeWithAttributeCorePath() {
        return belongToACategoryContaingAtLeastAnAttributeWithAttributeCorePath;
    }
}
