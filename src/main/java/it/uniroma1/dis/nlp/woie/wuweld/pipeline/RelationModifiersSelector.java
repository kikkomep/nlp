package it.uniroma1.dis.nlp.woie.wuweld.pipeline;

import edu.stanford.nlp.trees.EnglishGrammaticalRelations;
import edu.stanford.nlp.trees.GrammaticalRelation;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author lgu
 */
public class RelationModifiersSelector {

    private static RelationModifiersSelector instance = new RelationModifiersSelector();
    private HashSet<GrammaticalRelation> relationsToBeIncluded;
    private HashSet<GrammaticalRelation> relationsToBeIncludedForCopula;
    private HashSet<GrammaticalRelation> relationsToBeIncludedForNoun;

    private RelationModifiersSelector() {
        relationsToBeIncluded = new HashSet();
        relationsToBeIncludedForCopula = new HashSet();
        relationsToBeIncludedForNoun = new HashSet();
        initialize();
    }

    private void initialize() {
        relationsToBeIncluded.add(EnglishGrammaticalRelations.AUX_MODIFIER);
        relationsToBeIncluded.add(EnglishGrammaticalRelations.AUX_PASSIVE_MODIFIER);
        relationsToBeIncluded.add(EnglishGrammaticalRelations.NEGATION_MODIFIER);
        relationsToBeIncluded.add(EnglishGrammaticalRelations.ADVERBIAL_MODIFIER);
        relationsToBeIncluded.add(EnglishGrammaticalRelations.ADJECTIVAL_MODIFIER);
        relationsToBeIncluded.add(EnglishGrammaticalRelations.PREPOSITIONAL_MODIFIER);
        relationsToBeIncluded.add(EnglishGrammaticalRelations.COPULA);
        // ??? relationsToBeIncluded.add(EnglishGrammaticalRelations.DIRECT_OBJECT);

        relationsToBeIncludedForCopula.add(EnglishGrammaticalRelations.COPULA);
        relationsToBeIncludedForCopula.add(EnglishGrammaticalRelations.NEGATION_MODIFIER);

        relationsToBeIncludedForNoun.add(EnglishGrammaticalRelations.NOUN_COMPOUND_MODIFIER);
        relationsToBeIncludedForNoun.add(EnglishGrammaticalRelations.ADJECTIVAL_MODIFIER);
        relationsToBeIncludedForNoun.add(EnglishGrammaticalRelations.NUMBER_MODIFIER);
        relationsToBeIncludedForNoun.add(EnglishGrammaticalRelations.NUMERIC_MODIFIER);
        //relationsToBeIncludedForNoun.add(EnglishGrammaticalRelations.PREPOSITIONAL_MODIFIER);
        relationsToBeIncludedForNoun.add(EnglishGrammaticalRelations.PREPOSITIONAL_COMPLEMENT);


        relationsToBeIncludedForNoun.add(EnglishGrammaticalRelations.POSSESSION_MODIFIER);
    }

    public static RelationModifiersSelector getInstance() {
        return instance;
    }

    public boolean relationToBeIncluded(GrammaticalRelation g) {
        return relationsToBeIncluded.contains(g);
    }

    public Collection<GrammaticalRelation> getGrammaticalRelation() {
        return relationsToBeIncluded;
    }

    public Collection<GrammaticalRelation> getGrammaticalRelationsForCopula() {
        return relationsToBeIncludedForCopula;
    }

    public Collection<GrammaticalRelation> getRelationsToBeIncludedForNoun() {
        return relationsToBeIncludedForNoun;
    }
}
