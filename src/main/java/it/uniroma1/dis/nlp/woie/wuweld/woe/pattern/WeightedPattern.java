package it.uniroma1.dis.nlp.woie.wuweld.woe.pattern;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;

/**
 *
 * @author lgu
 */
public class WeightedPattern {

    public double weight;
    public DependencyPath dependencyPath;
    public SemanticGraphEdge edgeSubj;
    public SemanticGraphEdge edgeObj;
    public IndexedWord subj;
    public IndexedWord obj;
    public IndexedWord rel;
    public boolean copula = false;
}
