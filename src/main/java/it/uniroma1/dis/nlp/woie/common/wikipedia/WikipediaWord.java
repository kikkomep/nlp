package it.uniroma1.dis.nlp.woie.common.wikipedia;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by kikkomep on 12/27/13.
 */
@Entity
public class WikipediaWord implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;


    @NotNull
    @Column(unique = true)
    private String text;

    private boolean synonymsLoaded=false;

    @ManyToMany
    @JoinTable(name="WikipediaWord_Synonyms")
    private Set<WikipediaWord> synonyms = new HashSet<>();


    protected WikipediaWord() {
    }

    public WikipediaWord(String text) {
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void addSynonym(WikipediaWord word){
        if(word!=null && !this.synonyms.contains(word) && !this.getText().equals(word.getText()))
            this.synonyms.add(word);
    }

    public void removeSynonym(WikipediaWord word){
        if(word!=null && this.synonyms.contains(word))
            this.synonyms.remove(word);
    }

    public Set<WikipediaWord> getSynonyms(){
        return Collections.unmodifiableSet(this.synonyms);
    }

    public void removeAllSynonyms(){
        this.synonyms.clear();
    }

    public boolean isSynonymsLoaded() {
        return synonymsLoaded;
    }

    public void setSynonymsLoaded(boolean synonymsLoaded) {
        this.synonymsLoaded = synonymsLoaded;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WikipediaWord)) return false;

        WikipediaWord that = (WikipediaWord) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!text.equals(that.text)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + text.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "WikipediaWord{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", #synonyms=" + synonyms.size() +
                '}';
    }
}
