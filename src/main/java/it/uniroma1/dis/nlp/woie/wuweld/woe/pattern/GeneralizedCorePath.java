package it.uniroma1.dis.nlp.woie.wuweld.woe.pattern;

import javax.persistence.Entity;

/**
 * Created by kikkomep on 12/28/13.
 */
@Entity
public class GeneralizedCorePath extends DependencyPath {

    public GeneralizedCorePath() {
    }

    public GeneralizedCorePath(String path) {
        super(path);
    }
}
