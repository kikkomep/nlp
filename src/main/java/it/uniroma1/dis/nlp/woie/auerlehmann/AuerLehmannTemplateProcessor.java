package it.uniroma1.dis.nlp.woie.auerlehmann;

import it.uniroma1.dis.nlp.woie.auerlehmann.relation.plain.TemplateRelation;
import it.uniroma1.dis.nlp.woie.common.relation.RelationWriter;
import it.uniroma1.dis.nlp.woie.common.relation.RelationsRepository;
import it.uniroma1.dis.nlp.woie.common.wikipedia.*;
import it.uniroma1.dis.nlp.woie.utils.RelationsRepositoryFactory;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by kikkomep on 12/19/13.
 */
public class AuerLehmannTemplateProcessor {


    private Logger logger = Logger.getLogger(AuerLehmannTemplateProcessor.class);

    private final RelationsRepository<TemplateRelation> repository;

    private final List<RelationWriter> writers = new ArrayList<>();

    private final List<WikipediaTemplateFilter> filterList = new ArrayList<>();

    private int totalNumberOfProcessedPages = 0;
    private int totalNumberOfTemplates = 0;
    private int totalNumberOfSkippedTemplates = 0;
    private int totalNumberOfTriples = 0;

    private WikipediaPage page;
    private int numberOfTriples = 0;
    private int skippedTemplates = 0;


    private boolean onlyInfoboxes = false;
    private boolean verbose = true;


    public AuerLehmannTemplateProcessor() {
        this.repository = RelationsRepositoryFactory.getInstance().getAuerlehmannRepository();
    }


    public boolean isVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public void setOnlyInfoboxes(boolean onlyInfoboxes) {
        this.onlyInfoboxes = onlyInfoboxes;
    }

    public boolean isOnlyInfoboxes() {
        return onlyInfoboxes;
    }

    public void addFilter(WikipediaTemplateFilter filter) {
        this.filterList.add(filter);
    }

    public void addTemplateWriter(RelationWriter writer) {
        if (writer != null)
            this.writers.add(writer);
    }


    private void write(TemplateRelation relation) {
        if (numberOfTriples == 0)
            logger.info("\n\n***** DETECTED TRIPLES *****\n");

        if (verbose && writers.size() > 1) logger.info("");
        for (RelationWriter writer : writers) {
            Object triple = writer.write(relation);
            if (triple != null) {
                if (verbose)
                    logger.info("\t -" + triple.toString());
            }
            numberOfTriples++;
        }
    }


    public void startSession() {
        this.repository.startTransation();
    }

    public void closeSession() {
        this.repository.closeTransaction();
    }

    public void process(WikipediaPage page) {

        this.page = page;
        numberOfTriples = 0;
        skippedTemplates = 0;

        // Open writers
        for (RelationWriter w : this.writers) {
            w.open();
            w.getRepository().deleteBySubject(page.getTitle());
        }


        for (WikipediaTemplate template : page.getTemplates()) {
            process(template);
        }

        if (verbose && numberOfTriples == 0)
            logger.info("\n==> No Triples Detected");


        this.totalNumberOfProcessedPages++;
        this.totalNumberOfTemplates += page.numberOfMainTemplates();
        this.totalNumberOfSkippedTemplates += skippedTemplates;
        this.totalNumberOfTriples += numberOfTriples;


        // Close writers
        for (RelationWriter w : this.writers)
            w.close();

    }


    void resetStatistics() {
        totalNumberOfProcessedPages = 0;
        totalNumberOfTemplates = 0;
        totalNumberOfSkippedTemplates = 0;
        totalNumberOfTriples = 0;
    }

    void printPageStatistics() {
        char c = '*';
        int length = 40;
        char[] chars = new char[length];


        Arrays.fill(chars, c);
        this.logger.info("\n\n" + String.valueOf(chars));
        this.logger.info(StringUtils.center("------ " + page.getTitle() + " ------", length));
        this.logger.info(String.valueOf(chars));

        logger.info(StringUtils.center(String.format("%20s: %d", "Main templates detected", page.numberOfMainTemplates()), length));
        logger.info(StringUtils.center(String.format("%20s: %d", "Total templates detected", page.numberOfTotalTemplates()), length));
        logger.info(StringUtils.center(String.format("%20s: %d", "Total skipped templates", skippedTemplates), length));
        logger.info(StringUtils.center(String.format("%20s: %d", "Total triples produced", numberOfTriples), length));
        this.logger.info(String.valueOf(chars));
    }


    void printStatistics() {
        char c = '*';
        int length = 40;
        char[] chars = new char[length];


        Arrays.fill(chars, c);
        this.logger.info("\n\n" + String.valueOf(chars));
        this.logger.info(StringUtils.center("------ STATISTICS ------", length));
        this.logger.info(String.valueOf(chars));

        logger.info((String.format("%30s: %d", "Processed pages ", this.totalNumberOfProcessedPages)));
        logger.info((String.format("%30s: %d", "Total templates detected", this.totalNumberOfTemplates)));
        logger.info((String.format("%30s: %d", "Total skipped templates", this.totalNumberOfSkippedTemplates)));
        logger.info((String.format("%30s: %d", "Total triples produced", this.totalNumberOfTriples)));
        this.logger.info(String.valueOf(chars));
    }


    private void process(WikipediaTemplate template) {

        logger.debug("\n\n *** New Template " + ":" + template.getName());

        for (WikipediaTemplateFilter filter : this.filterList) {
            if (filter.reject(template)) {
                skippedTemplates++;
                logger.debug(" Template " + template.getName() + " rejected !!!");
                return;
            }
        }


        String subject = this.getSubjectNameOf(template);

        if (!onlyInfoboxes || template.getName().toUpperCase().contains("INFOBOX")) {

            // TODO: refine the mechanism for identifying the proper subject
//            if (!template.isNested())
//                subject = template.getPage().getTitle();


            for (WikipediaTemplateAttributeAssociation attributeAssociation : template.getAttributes()) {

                if (attributeAssociation.getName().contains("UNDEFINED")) continue;

                logger.debug("\n\t - New Attribute " + attributeAssociation.getName() + " " + attributeAssociation.getAttribute());

                WikipediaTemplateAttribute value = attributeAssociation.getAttribute();

                if (value instanceof WikipediaTemplate) {
                    WikipediaTemplate tm = (WikipediaTemplate) attributeAssociation.getAttribute();

                    if (!preprocessTemplate(tm)) {

                        boolean toReject = false;
                        for (WikipediaTemplateFilter filter : this.filterList) {
                            toReject = filter.reject(tm);
                            if (toReject) {
                                skippedTemplates++;
                                logger.debug(" Template " + tm.getName() + " rejected !!!");
                                break;
                            }
                        }

                        if (!toReject) {
                            TemplateRelation t = new TemplateRelation(subject, attributeAssociation.getName(), getSubjectNameOf(tm), TemplateRelation.OBJECTY_TYPE.RESOURCE);
                            write(t);
                            process(tm);
                        }
                    }


                } else if (value instanceof WikipediaTemplateLiteralAttribute) {

                    TemplateRelation.OBJECTY_TYPE type = TemplateRelation.OBJECTY_TYPE.valueOf(((WikipediaTemplateLiteralAttribute) value).getLiteralType().name());

                    TemplateRelation t = new TemplateRelation(subject, attributeAssociation.getName(), value.getValue(), type);
                    write(t);


                } else if (value instanceof WikipediaTemplateComplexAttribute) {


                    if (value instanceof WikipediaTemplateComplexAttribute) {

                        WikipediaTemplateComplexAttribute complexAttribute = (WikipediaTemplateComplexAttribute) value;

                        TemplateRelation t = new TemplateRelation(subject, attributeAssociation.getName(), value.getValue(), TemplateRelation.OBJECTY_TYPE.STRING);
                        write(t);

                        if (complexAttribute.getAttributes().size() > 1) {

                            String predicate = attributeAssociation.getName();

                            WikipediaTemplate tm = (WikipediaTemplate) attributeAssociation.getParent();
                            if (java.util.regex.Pattern.matches("\\blist\\b", tm.getName())) {
                                predicate = tm.getParentAssociation().getName();
                            }


                            for (WikipediaTemplateLiteralAttribute na : complexAttribute.getAttributes()) {

                                TemplateRelation ct = new TemplateRelation(subject, predicate, na.getValue(), na.getLiteralType().name());
                                write(ct);
                            }
                        }

                    }
                }
            }
        }
    }


    private boolean preprocessTemplate(WikipediaTemplate template) {

        boolean isSpecial = false;
        TemplateRelation triple;

        if (true) return isSpecial;

        String subject = getSubjectNameOf(template);

        if (template.getName().equalsIgnoreCase("Birth date")) {


            template.getAttributes().get(0).setName("birth year");
            WikipediaTemplateLiteralAttribute year = (WikipediaTemplateLiteralAttribute) template.getAttributes().get(0).getAttribute();
            year.setLiteralType(WikipediaTemplateLiteralAttribute.TYPE.DATE_YEAR);
            triple = new TemplateRelation(subject, template.getAttributes().get(0).getName(), year.getValue(), year.getLiteralType().name());
            write(triple);


            template.getAttributes().get(1).setName("birth month");
            WikipediaTemplateLiteralAttribute month = (WikipediaTemplateLiteralAttribute) template.getAttributes().get(1).getAttribute();
            month.setLiteralType(WikipediaTemplateLiteralAttribute.TYPE.DATE_MONTH);
            triple = new TemplateRelation(subject, template.getAttributes().get(1).getName(), month.getValue(), month.getLiteralType().name());
            write(triple);


            template.getAttributes().get(2).setName("birth day");
            WikipediaTemplateLiteralAttribute day = (WikipediaTemplateLiteralAttribute) template.getAttributes().get(2).getAttribute();
            day.setLiteralType(WikipediaTemplateLiteralAttribute.TYPE.DATE_DAY);
            triple = new TemplateRelation(subject, template.getAttributes().get(2).getName(), day.getValue(), day.getLiteralType().name());
            write(triple);


            //triple = new TemplateRelation(subject, template.getName(), day.getValue() + "/" + month.getValue() + "/" + year.getValue(), WikipediaTemplateLiteralAttribute.TYPE.DATE.name());
            triple = new TemplateRelation(subject, template.getName(), getSubjectNameOf(template), WikipediaTemplateLiteralAttribute.TYPE.DATE.name());
            write(triple);


            isSpecial = true;


        } else if (template.getName().equalsIgnoreCase("Death date and age")) {


            triple = new TemplateRelation(subject, "death date", getSubjectNameOf(template), WikipediaTemplateLiteralAttribute.TYPE.DATE.name());
            write(triple);


            template.getAttributes().get(0).setName("death year");
            WikipediaTemplateLiteralAttribute dyear = (WikipediaTemplateLiteralAttribute) template.getAttributes().get(0).getAttribute();
            dyear.setLiteralType(WikipediaTemplateLiteralAttribute.TYPE.DATE_YEAR);
            triple = new TemplateRelation(subject, template.getAttributes().get(0).getName(), dyear.getValue(), dyear.getLiteralType().name());
            write(triple);


            template.getAttributes().get(1).setName("death month");
            WikipediaTemplateLiteralAttribute dmont = (WikipediaTemplateLiteralAttribute) template.getAttributes().get(1).getAttribute();
            dmont.setLiteralType(WikipediaTemplateLiteralAttribute.TYPE.DATE_MONTH);
            triple = new TemplateRelation(subject, template.getAttributes().get(1).getName(), dmont.getValue(), dmont.getLiteralType().name());
            write(triple);


            template.getAttributes().get(2).setName("death day");
            WikipediaTemplateLiteralAttribute dday = (WikipediaTemplateLiteralAttribute) template.getAttributes().get(2).getAttribute();
            dday.setLiteralType(WikipediaTemplateLiteralAttribute.TYPE.DATE_DAY);
            triple = new TemplateRelation(subject, template.getAttributes().get(2).getName(), dday.getValue(), dday.getLiteralType().name());
            write(triple);


//            triple = new TemplateRelation(subject, "death date", dday.getValue() + "/" + dmont.getValue() + "/" + dyear.getValue(), WikipediaTemplateLiteralAttribute.TYPE.DATE.name());
//            write(triple);
//            logger.info("\t - " + triple.toString());
//            numberOfTriples++;


            template.getAttributes().get(3).setName("birth year");
            WikipediaTemplateLiteralAttribute byear = (WikipediaTemplateLiteralAttribute) template.getAttributes().get(3).getAttribute();
            byear.setLiteralType(WikipediaTemplateLiteralAttribute.TYPE.DATE_YEAR);
            triple = new TemplateRelation(subject, template.getAttributes().get(3).getName(), byear.getValue(), byear.getLiteralType().name());
            write(triple);


            template.getAttributes().get(4).setName("birth month");
            WikipediaTemplateLiteralAttribute bmonth = (WikipediaTemplateLiteralAttribute) template.getAttributes().get(4).getAttribute();
            bmonth.setLiteralType(WikipediaTemplateLiteralAttribute.TYPE.DATE_MONTH);
            triple = new TemplateRelation(subject, template.getAttributes().get(4).getName(), bmonth.getValue(), bmonth.getLiteralType().name());
            write(triple);


            template.getAttributes().get(5).setName("birth day");
            WikipediaTemplateLiteralAttribute bday = (WikipediaTemplateLiteralAttribute) template.getAttributes().get(5).getAttribute();
            bday.setLiteralType(WikipediaTemplateLiteralAttribute.TYPE.DATE_DAY);
            triple = new TemplateRelation(subject, template.getAttributes().get(5).getName(), bday.getValue(), bday.getLiteralType().name());
            write(triple);


//            triple = new TemplateRelation(subject, "birth day", bday.getValue() + "/" + bmonth.getValue() + "/" + byear.getValue(), WikipediaTemplateLiteralAttribute.TYPE.DATE.name());
//            write(triple);
//            logger.info("\t - " + triple.toString());
//            numberOfTriples++;


            isSpecial = true;

        }

        return isSpecial;
    }


    public String getSubjectNameOf(WikipediaTemplate template) {
        String result = template.getPage().getTitle();
        if (template.isNested())
            result = TemplateRelation.getBlankNode(result, template.getParentAssociation().getName()) + "";
        return result;
    }


}
