package it.uniroma1.dis.nlp.woie.wuweld.pipeline.services;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.ParseException;
import it.uniroma1.lcl.jlt.ling.Word;


import java.util.*;

/**
 * Created by lgu on 03/02/14.
 */
public class ClauseRelationDisambiguation {

    private SemanticGraph semanticGraph;
    private IndexedWord subj,obj,relation;
    private boolean isDisambiguated;
    private String relationSense;

    public ClauseRelationDisambiguation(SemanticGraph semanticGraph, IndexedWord subj, IndexedWord obj, IndexedWord relation) {
        this.semanticGraph = semanticGraph;
        this.subj = subj;
        this.obj = obj;
        this.relation = relation;
        isDisambiguated = false;
    }

    public ClauseRelationDisambiguation(SemanticGraph semanticGraph, PathCandidate pathCandidate){
        this.semanticGraph = semanticGraph;
        this.subj = pathCandidate.getSubj();
        this.obj = pathCandidate.getObj();
        this.relation = pathCandidate.getRelation();
        isDisambiguated = false;
    }

    public String disambiguateRelation() throws ParseException {

        if(!isDisambiguated){
            RelationExpander re = new RelationExpander(semanticGraph,relation,false);
            WordPathExpander wpeSubj = new WordPathExpander(semanticGraph,subj);
            WordPathExpander wpeObj = new WordPathExpander(semanticGraph,obj);

            Collection<IndexedWord> ws = re.getIndexedWordCollection();
            ws.addAll(wpeSubj.getIndexedWordCollection());
            ws.addAll(wpeObj.getIndexedWordCollection());

            Collections.sort((List<IndexedWord>) ws, new Comparator<IndexedWord>() {
                @Override
                public int compare(IndexedWord o1, IndexedWord o2) {
                    return o1.index() - o2.index();
                }
            });

            List<Word> wordClause = new LinkedList<>();
            for(IndexedWord w: ws){
                try{
                    wordClause.add(Expander.transformIndexedWordToLemma(w));
                }catch (ParseException e){

                }
            }


//            Collection<Word> wordClause = re.getWordCollection();
//            wordClause.addAll(wpeSubj.getWordCollection());
//            wordClause.addAll(wpeObj.getWordCollection());

            BabelNetSenseFinder babelNetSenseFinder = new BabelNetSenseFinder(wordClause);
            relationSense = babelNetSenseFinder.getOneBestSense(Expander.transformIndexedWordToLemma(relation));
            isDisambiguated=true;
        }

        return relationSense;
    }
}