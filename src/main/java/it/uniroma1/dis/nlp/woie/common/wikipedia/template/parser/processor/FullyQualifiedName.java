package it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.processor;

/**
 * Created by kikkomep on 12/19/13.
 */
public class FullyQualifiedName {


    private String startWith = null;

    private StringBuilder name = new StringBuilder();
    private StringBuilder prefix = new StringBuilder();

    private int type;

    public String getSimpleName() {
        return name.toString().trim();
    }

    public void addToken(Token t) {
        this.type = t.kind;
        this.addToken(t.toString());
    }

    public void addToken(String p) {
        if(p!=null){

            if(p.equals(":")){

                prefix.append(name.toString() + ":");
                name.delete(0, name.length());

            }else name.append(p);
        }
    }

    public void setStartWith(String str) {
        this.startWith = str;
    }

    public String getStartWith() {
        return startWith;
    }

    public String getPrefix() {
        return prefix.toString().trim();
    }

    public String getName() {
        return prefix.toString() + name.toString();
    }

    public int getType() {
        return type;
    }
}
