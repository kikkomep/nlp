package it.uniroma1.dis.nlp.woie.wuweld.woe.pattern;

import it.uniroma1.dis.nlp.woie.utils.AppEntityManagerFactory;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kikkomep on 12/11/13.
 */
public class DependencyPathsRepository<T extends DependencyPath> {

    protected final EntityManager em;

    protected final Logger logger;

    protected final Class type;

    public DependencyPathsRepository(Class type) {
        this.type = type;
        this.logger = Logger.getLogger(
                DependencyPathsRepository.class);
        this.em = AppEntityManagerFactory.getInstance().
                getEntityManager(AppEntityManagerFactory.NAME.wpr);
    }

    public DependencyPathsRepository(Class type, EntityManager em) {
        this.type = type;
        this.logger = Logger.getLogger(
                DependencyPathsRepository.class);
        this.em = em;
    }


    public T findById(Long id) {

        T result = null;

        try {

            result = (T) em.find(DependencyPath.class, id);

        } catch (javax.persistence.NoResultException e) {
        }

        return result;
    }

    public T findByPath(String path) {

        try {
            Query q;
            q = em.createQuery(
                    "SELECT p FROM " + type.getSimpleName() + " p WHERE p.path = \':path\'");
            q.setParameter("path", path);
            logger.debug("QUERY: " + "SELECT p FROM " + type.getSimpleName() + " p WHERE p.path = :path");
            return (T) q.getSingleResult();
        } catch (Exception ex) {
        }

        return null;
    }


    public PatternsStatistics getStatistics() {

        Query q;
        /* // OLD QUERY
        q = em.createQuery(
                "SELECT p.path, frequency(p) FROM " + type.getSimpleName() + " p GROUP BY p.path ORDER BY count(p) DESC ");*/
        q = em.createQuery(
                "SELECT p.path, p.frequency FROM " + type.getSimpleName() + " p GROUP BY p.path ORDER BY p.frequency DESC ");
        List<Object[]> list = q.getResultList();

        ArrayList<PatternStatistics> result = new ArrayList<>();

        for (Object[] res : list) {
            result.add(new PatternStatistics((String) res[0], (Long) res[1]));
        }

        return new PatternsStatistics(result);
    }

    public List<T> findAll() {

        Query q;
        q = em.createQuery(
                "SELECT p FROM " + type.getSimpleName() + " p");
        return q.getResultList();
    }

    public void save(T p) {
        if (p == null) {
            throw new RuntimeException("DependencyPath is NULL");
        }

        if (p.getPath() == null) throw new RuntimeException("Path is NULL");
        em.getTransaction().begin();

        try {

            Query q;
            q = em.createQuery(
                    "SELECT p FROM " + type.getSimpleName() + " p WHERE p.path = :path");
            q.setParameter("path", p.getPath());
            T path = (T) q.getSingleResult();
            logger.info("Path ID " + type.getSimpleName() + ": " + path.getId());
            if (path != null) {
                p.setId(path.getId());
                p.setFrequency(path.getFrequency() + 1);
            }

        } catch (Exception ex) {
        }


        try {

            if (p.getId() == null) {
                p.setFrequency(1L);
                em.persist(p);

            } else {
                em.merge(p);
            }

            em.flush();

            em.getTransaction().commit();


            logger.info("DependencyPath " + p + " saved!!!");

        } catch (javax.persistence.NoResultException e) {
            em.getTransaction().rollback();
        }
    }

    public void delete(T p) {
        if (p == null) {
            throw new RuntimeException("DependencyPath is NULL");
        }

        em.getTransaction().begin();

        try {

            em.remove(p);

            em.flush();

            em.getTransaction().commit();

            logger.info("DependencyPath " + p + " removed!!!");

        } catch (javax.persistence.NoResultException e) {
            em.getTransaction().rollback();
        }
    }
}
