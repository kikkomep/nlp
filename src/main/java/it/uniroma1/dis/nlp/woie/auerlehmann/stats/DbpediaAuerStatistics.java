package it.uniroma1.dis.nlp.woie.auerlehmann.stats;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import javax.persistence.Transient;


/**
 * Created by kikkomep on 12/21/13.
 */

public class DbpediaAuerStatistics {


    @Transient
    private Logger logger = LoggerFactory.getLogger(DbpediaAuerStatistics.class);


    private long totalPages = 0;
    private long meaningfulTriples = 0;
    private long partialMeaningfulMatchedTriples = 0;
    private long globalMeaningfulMatchedTriples = 0;
    private long fullMeaningfulMatchedTriples = 0;
    private long partialMatches = 0;
    private long fullMatches = 0;
    private long falseNegativeMatches = 0;
    private long falsePositiveMatches = 0;


    public long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }

    public void addPage() {
        this.totalPages++;
    }

    public void addPartialMeaningfulMatchedTriples(long meaningFulTriples){
        this.partialMeaningfulMatchedTriples += meaningFulTriples;
    }

    public long getFullMeaningfulMatchedTriples() {
        return fullMeaningfulMatchedTriples;
    }


    public void addFullMeaningfulMatchedTriples(long meaningFulTriples){
        this.fullMeaningfulMatchedTriples += meaningFulTriples;
    }

    public long getPartialMeaningfulMatchedTriples() {
        return partialMeaningfulMatchedTriples;
    }

    public void addMeaningfulTriples(long meaningfulTriples){
        this.meaningfulTriples+=meaningfulTriples;
    }

    public long getMeaningfulTriples() {
        return meaningfulTriples;
    }


    public long getTotalMatchedTriples(){
        return this.partialMatches + this.fullMatches;
    }

    public long getTotalTriples() {
        return this.partialMatches + this.fullMatches + this.falsePositiveMatches;
    }

    public long getPartialMatches() {
        return partialMatches;
    }

    public void addPartialMatches(int partialMatches) {
        this.partialMatches += partialMatches;
    }

    public void setPartialMatches(long partialMatches) {
        this.partialMatches = partialMatches;
    }

    public long getFullMatches() {
        return fullMatches;
    }

    public void addFullMatches(int fullMatches) {
        this.fullMatches += fullMatches;
    }

    public void setFullMatches(long fullMatches) {
        this.fullMatches = fullMatches;
    }

    public long getFalseNegativeMatches() {
        return falseNegativeMatches;
    }

    public void addFalseNegativeMatches(int falseNegativeMatches) {
        this.falseNegativeMatches += falseNegativeMatches;
    }

    public void addGlobalMeaningfulMatchedTriples(long meaningfulTriples){
        this.globalMeaningfulMatchedTriples += meaningfulTriples;
    }

    public long getGlobalMeaningfulMatchedTriples() {
        return globalMeaningfulMatchedTriples;
    }

    public void setFalseNegativeMatches(long falseNegativeMatches) {
        this.falseNegativeMatches = falseNegativeMatches;
    }

    public long getFalsePositiveMatches() {
        return falsePositiveMatches;
    }

    public void addFalsePositiveMatches(int falsePositiveMatches) {
        this.falsePositiveMatches += falsePositiveMatches;
    }

    public void setFalsePositiveMatches(long falsePositiveMatches) {
        this.falsePositiveMatches = falsePositiveMatches;
    }


    public double getFullPrecision() {
        return getTotalTriples()>0 ? (double) getFullMatches()/(getTotalTriples()) : 0;
    }


    public double getGlobalPrecision() {
        return getTotalTriples()>0 ? (double) getTotalMatchedTriples()/getTotalTriples() : 0;
    }


    public double getFullRecall() {
        return getMeaningfulTriples()>0 ? (double) getFullMeaningfulMatchedTriples()/getMeaningfulTriples() : 0;
    }

    public double getGlobalRecall() {
        return getMeaningfulTriples()>0 ? (double) getGlobalMeaningfulMatchedTriples()/getMeaningfulTriples() : 0;
    }


    public double getFullFScore() {
        double over = getFullPrecision() + getFullRecall();
        return over>0 ? 2 * ((double) (getFullPrecision() * getFullRecall())/over) : 0;
    }

    public double getGlobalFScore() {
        double over = getGlobalPrecision() + getGlobalRecall();
        return over>0 ? 2 * ((double) (getGlobalPrecision() * getGlobalRecall())/over) : 0;
    }

    public void printStatistcs() {


        logger.info("\n\n\n********** STATISTICs **********");

        logger.info("\n");
        logger.info(" - Processed pages: " + getTotalPages());
        logger.info(" - Total triples: " + getTotalTriples());
        logger.info(" - Total meaningful: " + getMeaningfulTriples());

        logger.info("\n");
        logger.info(" - Partial matched triples: " + getPartialMatches());
        logger.info(" - Full matched triples: " + getFullMatches());
        logger.info(" - Total matched triples: " + getTotalMatchedTriples());

        logger.info("\n");
        logger.info(" - Partial meaningful matched triples: " + getPartialMeaningfulMatchedTriples());
        logger.info(" - Full meaningful matched triples: " + getFullMeaningfulMatchedTriples());
        logger.info(" - Global meaningful matched triples: " + getGlobalMeaningfulMatchedTriples());



        logger.info("\n");
        logger.info(" - False positive triples: " + getFalsePositiveMatches());
        logger.info(" - False negative triples: " + getFalseNegativeMatches());

        logger.info("\n");
        logger.info(" - Precision full: " + getFullPrecision());
        logger.info(" - Precision global: " + getGlobalPrecision());

        logger.info("\n");
        logger.info(" - Recall full: " + getFullRecall());
        logger.info(" - Recall global: " + getGlobalRecall());

        logger.info("\n");
        logger.info(" - F-Score full: " + getFullFScore());
        logger.info(" - F-Score global: " + getGlobalFScore());
    }
}
