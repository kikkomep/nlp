package it.uniroma1.dis.nlp.woie.common.wikipedia;


import com.sun.istack.internal.Nullable;

import javax.persistence.*;

/**
 * Created by kikkomep on 12/20/13.
 */
@Entity
public class WikipediaTemplateAttribute extends WikipediaEntity {


    @Nullable
    @Lob
    protected String value;

    @Nullable
    @Lob
    protected String rawValue;


    @ManyToOne(optional = true)
    protected WikipediaTemplateAttributeAssociation parent=null;



    public WikipediaTemplateAttribute() {
    }

    public WikipediaTemplateAttribute(String value) {
        this.value = value;
    }

    public WikipediaTemplateAttribute(String value, String rawValue) {
        this.value = value;
        this.rawValue = rawValue;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRawValue() {
        return rawValue;
    }


    public void setRawValue(String rawValue) {
        this.rawValue = rawValue;
    }

    public WikipediaTemplateAttributeAssociation getParentAssociation() {
        return parent;
    }

    public void setParent(WikipediaTemplateAttributeAssociation parent) {
        this.parent = parent;
    }


}
