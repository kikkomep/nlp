/*
 * Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
 * Mountain View, California, 94041, USA.
 */
package it.uniroma1.dis.nlp.woie.auerlehmann;

import it.uniroma1.dis.nlp.woie.App;
import it.uniroma1.dis.nlp.woie.auerlehmann.relation.plain.TemplateRelation;
import it.uniroma1.dis.nlp.woie.common.relation.RelationWriter;
import it.uniroma1.dis.nlp.woie.common.wikipedia.*;
import it.uniroma1.dis.nlp.woie.common.wikipedia.Wikipedia;
import it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.ParseException;
import it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.WikipediaTemplateParser;
import it.uniroma1.dis.nlp.woie.common.wikipedia.utils.WikipediaProcessedPageHistory;
import it.uniroma1.dis.nlp.woie.utils.RelationsRepositoryFactory;


import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author kikkomep
 */
public class AuerLehmannApp extends App {

    private WikipediaTemplateParser parser = null;
    private Wikipedia wikipedia = null;

    private AuerLehmannTemplateProcessor processor = null;

    private WikipediaProcessedPageHistory history = null;


    private List<String> errors = new ArrayList<>();

    private void configure() {
        WikipediaTemplateFilter filter;
        Matcher matcher;
        String filterProperty, filterName, filterClassName;
        String writerProperty, writerName, writerClassName;

        // Configure the processed page wikipedia
        this.wikipedia = Wikipedia.getInstance();


        // Configure the parser        
        this.parser = wikipedia.getDefaultParser();

        this.processor = new AuerLehmannTemplateProcessor();
        //this.parser.addWikipediaTemplateProcessor();

        // History
        this.history = WikipediaProcessedPageHistory.getInstance();


        // Configure the filters from config.properties
        Pattern pattern = Pattern.compile("filter_(\\w)+");
        for (Object p : this.getConfigurationProperties().keySet()) {
            filterProperty = String.valueOf(p);
            matcher = pattern.matcher(filterProperty);
            if (matcher.find()) {
                filterName = matcher.group(1);
                filterClassName = this.getConfigurationProperties().getProperty(
                        filterProperty);
                try {
                    filter = (WikipediaTemplateFilter) Class.forName(
                            filterClassName).newInstance();
                    filter.setName(filterName);
                    this.processor.addFilter(filter);
                    this.logger.info("Filter " + filterName + " (" + filter.
                            getClass().getSimpleName() + ") registered");
                } catch (ClassNotFoundException ex) {
                    this.logger.error(
                            "Impossible to loadByTitle the class '" + filterClassName + "' for the filter " + filterName);
                } catch (Exception ex) {
                    this.logger.error(
                            "Impossible to instantiate the class '" + filterClassName + "' for the filter " + filterName);
                    System.err.printf(ex.getMessage());
                    ex.printStackTrace();
                }
            }
        }


        // Configure the filters from config.properties
        pattern = Pattern.compile("triple_writer_(\\w)+");
        for (Object p : this.getConfigurationProperties().keySet()) {
            writerProperty = String.valueOf(p);
            matcher = pattern.matcher(writerProperty);
            if (matcher.find()) {
                writerName = matcher.group(1);
                writerClassName = this.getConfigurationProperties().getProperty(
                        writerProperty);
                try {

                    RelationWriter writer = (RelationWriter) Class.forName(
                            writerClassName).newInstance();
                    processor.addTemplateWriter(writer);
                    this.logger.info("Writer " + writerName + " (" + writer.
                            getClass().getSimpleName() + ") registered");

                } catch (ClassNotFoundException ex) {
                    this.logger.error(
                            "Impossible to loadByTitle the class '" + writerClassName + "' for the writer " + writerName);

                } catch (Exception ex) {
                    this.logger.error(
                            "Impossible to instantiate the class '" + writerClassName + "' for the writer " + writerName);
                    System.err.printf(ex.getMessage());
                    ex.printStackTrace();
                }
            }
        }
    }

    @Override
    public void start() {


        this.logger.info("\n***** AuerLehmannApp started!!!\n");


        this.configure();


        String pageTitle = this.getArgumentValue("page");
        boolean forceUpdate = this.getArgumentNames().contains("force-update");
        boolean checkUpdate = this.getArgumentNames().contains("check-update");
        boolean saveObjects = this.getArgumentNames().contains("save-objects");
        boolean showTriples = this.getArgumentNames().contains("show-triples");
        boolean onlyInfoboxes = this.getArgumentNames().contains("only-infoboxes");
        boolean showErrors = this.getArgumentNames().contains("show-errors");

        logger.info("Force update: " + forceUpdate);
        logger.info("Check update: " + checkUpdate);
        logger.info("Save objects: " + saveObjects);
        logger.info("Show triples: " + showTriples);
        logger.info("Only Infoboxes: " + onlyInfoboxes);

        this.processor.setVerbose(showTriples);
        this.processor.setOnlyInfoboxes(onlyInfoboxes);

        this.processor.startSession();
        this.wikipedia.startSession();

        int numberOfPages = -1;
        if (this.getArgumentValue("limit") != null) {

            try {
                numberOfPages = Integer.parseInt(this.getArgumentValue(
                        "limit"));
            } catch (NumberFormatException ex) {
            }
        }

        if (pageTitle != null) {
            try {

                this.processPage(pageTitle, checkUpdate, forceUpdate, saveObjects);

            } catch (WikipediaPageNotFoundException ex) {
                logger.error(ex.getMessage(), ex);
            } catch (WikipediaPageInputStreamNotAvailableException ex) {
                logger.error(ex.getMessage(), ex);
            }
        } else if (numberOfPages >= 0) {
            this.processPages(numberOfPages, checkUpdate, forceUpdate, saveObjects, showTriples);

        } else {
            System.out.println(
                    "\n You must specify either a wikipedia page (i.e., --page-title [title]) or a number of pages (i.e., --pages-number [number] (0=all available)) to process");
            System.exit(0);
        }

        this.processor.closeSession();
        this.wikipedia.closeSession();

        if (showErrors) {
            logger.info("\n\n *** ERRORS: " + errors.size() + " ***");
            for (String error : errors)
                logger.info("Error in: " + error);
            logger.info("\n\n");
        }

    }

    public void processPage(String pageTitle, boolean checkUpdates,
                            boolean forceUpdates, boolean saveObjects) throws WikipediaPageNotFoundException, WikipediaPageInputStreamNotAvailableException {
        WikipediaPage page = wikipedia.loadByTitle(pageTitle);

        this.processPage(page, checkUpdates, forceUpdates, saveObjects);
    }


    /**
     * @param page
     */
    private boolean processPage(WikipediaPage page, boolean checkUpdates, boolean forceUpdates, boolean saveObjects) throws WikipediaPageNotFoundException, WikipediaPageInputStreamNotAvailableException {
        return processPage(page, checkUpdates, forceUpdates, saveObjects, true);
    }

    /**
     * @param page
     */
    private boolean processPage(WikipediaPage page, boolean checkUpdates, boolean forceUpdates, boolean saveObjects, boolean print) throws WikipediaPageNotFoundException, WikipediaPageInputStreamNotAvailableException {


        boolean toParse = false;

        char c = '-';
        int length = 120;
        char[] chars = new char[length];


        Arrays.fill(chars, c);
        this.logger.info("\n" + String.valueOf(chars));
        this.logger.info("\t PAGE: " + page.getTitle());
        this.logger.info(String.valueOf(chars));

        this.logger.info(
                "\nStarting to process the page " + page.getTitle() + " ... ");


        WikipediaPage storedPage = wikipedia.findByTitle(page.getTitle());

        if (storedPage != null) {
            logger.info(
                    "The page " + page.getTitle() + " already processed (lastUpdate: " +
                            page.getLastModified() + ", currentStored: " + storedPage.getLastModified() + ")");

            toParse = forceUpdates || (checkUpdates && storedPage.getLastModified() < page.
                    getLastModified());

            if (toParse && saveObjects)
                wikipedia.delete(storedPage);

        } else toParse = true;


        logger.info("To Parse: " + toParse);


        if (toParse) {


            logger.info(
                    "Updating triples of the page " + page.getTitle() + "... ");

            page = wikipedia.loadByTitle(page.getTitle());

            try {

                parser.parse(page);

            } catch (ParseException e) {
                logger.debug("An error during the parse phase: " + e.getMessage());
                errors.add(page.getTitle());

            } finally {
                if(!saveObjects)
                    WikipediaProcessedPageHistory.getInstance().addProcessedPage(page.getTitle(),
                            page.hasOnlyInfoboxes() ? history.INFOBOXES_ONLY : page.containsTemplate("Infobox") ? history.INFOBOXES_AVAILABLE : history.INFOBOX_NOT_AVAILABLE);
            }

            // Save the parsed page
            if (saveObjects)
                wikipedia.save(page);

            // Save templates related to the processed page
            processor.process(page);

            if (print)
                processor.printPageStatistics();


        } else {

            logger.info(
                    "The page " + page.getTitle() + " is up to date" + " (lastModified: " + page.getLastModified() + ")");

            if (print) {

                logger.info("Triples stored for the subject: " + page.getTitle());
                List<TemplateRelation> list = RelationsRepositoryFactory.getInstance().getAuerlehmannRepository().findBySubject(page.getTitle());
                for (TemplateRelation triple : list)
                    logger.info("\t - " + triple);
                logger.info("\n");
            }
        }

        this.logger.info(String.valueOf(chars) + "\n");

        return toParse;
    }


    /**
     * @param numberOfPagesToProcess
     */
    public void processPages(int numberOfPagesToProcess,
                             boolean checkUpdates, boolean forceUpdates, boolean saveObjects, boolean print) {

        processor.resetStatistics();

        logger.
                info(
                        "Processing of " + numberOfPagesToProcess + " pages is starting ... ");


        List<WikipediaPage> list = null;


        int skipFirst = 0;
        int numberOfProcessedPages = 0;


        if(!forceUpdates) skipFirst = history.countProcessedPages();


        logger.info("Processed pages up to now: " + history.countProcessedPages());

        do {

            list = wikipedia.loadPages(numberOfPagesToProcess-numberOfProcessedPages, skipFirst);

            logger.debug("Found pages after the last processed page: " + skipFirst);


            for (WikipediaPage page : list) {

                logger.debug("Processing the page (" + (numberOfProcessedPages+1) +"): " + page.getTitle());

                try {

                    logger.info("\n\nProcessing the (" + (numberOfProcessedPages+1) +") page: " + page.getTitle() + " starting .... ");


                    if (this.processPage(page, checkUpdates, forceUpdates, saveObjects, print))
                        numberOfProcessedPages++;

                } catch (WikipediaPageNotFoundException | WikipediaPageInputStreamNotAvailableException ex) {
                    logger.error(
                            "Error during the processing of the page " + page.
                                    getTitle(), ex);
                }
            }

            skipFirst += list.size();

            history.save();

        } while (list.size() > 0 && numberOfProcessedPages < numberOfPagesToProcess);

        processor.printStatistics();
    }

    /**
     * Entry point for AuerLehmannApp
     *
     * @param args
     */
    public static void main(String args[]) {
        AuerLehmannApp app = new AuerLehmannApp();
        init(app, args);
    }
}
