package it.uniroma1.dis.nlp.woie.wuweld.pipeline.services;

import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.Match;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by lgu on 03/02/14.
 */
public class PathMatcher {



    public static Set<PathCandidate> findGeneralizedMatchingPathCandidates(String path, SemanticGraph semanticGraph){

        String[] edges = path.split("#");
        int numOfEdges = edges.length;

        Set<PathCandidate> result = new LinkedHashSet<>();

        for (SemanticGraphEdge edgeSubj : semanticGraph.getEdgeSet()) {
            if (Match.createStringGeneralizedRelation(edgeSubj).equals(edges[0])) {
                if(numOfEdges==1){

                    result.add(new PathCandidate(edgeSubj));

                }else{

                    for(SemanticGraphEdge edgeObj: semanticGraph.getOutEdgesSorted(edgeSubj.getGovernor())){
                        if (Match.createStringGeneralizedRelation(edgeObj).equals(edges[1])){
                            result.add(new PathCandidate(edgeSubj,edgeObj));
                        }
                    }

                }
            }
        }

        return result;
    }


    public static Set<PathCandidate> findMatchingPathCandidates(String path, SemanticGraph semanticGraph){

        String[] edges = path.split("#");
        int numOfEdges = edges.length;

        Set<PathCandidate> result = new LinkedHashSet<>();

        for (SemanticGraphEdge edgeSubj : semanticGraph.getEdgeSet()) {
            if (Match.createStringRelation(edgeSubj).equals(edges[0])) {
                if(numOfEdges==1){

                    result.add(new PathCandidate(edgeSubj));

                }else{

                    for(SemanticGraphEdge edgeObj: semanticGraph.getOutEdgesSorted(edgeSubj.getGovernor())){
                        if (Match.createStringRelation(edgeObj).equals(edges[1])){
                            result.add(new PathCandidate(edgeSubj,edgeObj));
                        }
                    }

                }
            }
        }

        return result;
    }
}

