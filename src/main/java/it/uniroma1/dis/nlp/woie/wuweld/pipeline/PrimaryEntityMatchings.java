package it.uniroma1.dis.nlp.woie.wuweld.pipeline;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.trees.EnglishGrammaticalRelations;
import edu.stanford.nlp.util.CoreMap;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author lgu
 */
public class PrimaryEntityMatchings {
    
    protected static boolean typeMatch(String type, ParsedSentence sentence) {

        boolean result = false;
        for (CoreMap c : sentence.getAnnotation().get(CoreAnnotations.SentencesAnnotation.class)) {

            SemanticGraph sg = c.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);
            List<IndexedWord> l = sg.getAllNodesByWordPattern("(?i)" + type);

            for (IndexedWord w : l) {
                for (SemanticGraphEdge e : sg.outgoingEdgeIterable(w)) {
                    if (e.getRelation().equals(EnglishGrammaticalRelations.DETERMINER)) {
                        if (e.getDependent().lemma().equalsIgnoreCase("the")) {

                            List<IndexedWord> dep = new LinkedList<>();
                            dep.add(e.getGovernor());
                            sentence.addTitleMatch(new Entity(w, dep));

                            System.out.println("\n\n\n *** " + e.getSource() + " " + e.getGovernor() + " " + w.lemma()  + " *** \n\n\n");

                            result = true;
                        }
                    }
                }
            }
        }

        return result;
    }

    /*
     * Given a sentece fullSynomymMatch checks the presence of the primary entity matching the title dependencyPath.
     * The title dependencyPath is a regex composed by the title of the page and its synomyms.
     * 
     */
    protected static boolean fullSynonymMatch(Pattern title, ParsedSentence sentence) {

        //Search the title dependencyPath in the sentence
        boolean result = false;

        Matcher m = Pattern.compile("(?i)" + title.pattern()).matcher(sentence.getSentence());

        while (m.find()) {

            //Since a Match can be composed by more terms, we get tokens composing the current primary entities matching
            String[] match = sentence.getSentence().substring(m.start(), m.end()).split(" ");

            //We associate each found token with the corresponding node in the semantic graph
            //We build a regex that matches these nodes 
            StringBuilder pattern = new StringBuilder();
            for (int i = 0; i < match.length; i++) {
                pattern.append(match[i]);
                if (i != match.length - 1) {
                    pattern.append("|");
                }
            }

            for (CoreMap c : sentence.getAnnotation().get(CoreAnnotations.SentencesAnnotation.class)) {

                SemanticGraph sg = c.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);

                //getting nodes
                List<IndexedWord> l = sg.getAllNodesByWordPattern("(?i)" + pattern.toString());

                //We verify that found node fit with the token
                for (IndexedWord w : l) {

                    //we get the relative position of word w w.r.t. the token
                    List<IndexedWord> pe = new LinkedList<>();
                    int index = -1;
                    for (int i = 0; i < match.length; i++) {
                        if (match[i].equals(w.lemma())) {
                            index = i;
                        }
                    }

                    //we ckeck that the nodes are correctly ordered
                    boolean matched = true;
                    for (int i = 0; i < match.length; i++) {

                        IndexedWord nodeCandidate = sg.getNodeByIndexSafe(w.index() - index + i);

                        if (nodeCandidate == null || !nodeCandidate.lemma().equals(match[i])) {
                            matched = false;
                            break;
                        }
                        pe.add(nodeCandidate);
                    }

                    if (matched) {
                        result = true;
                        sentence.addTitleMatch(new Entity(w, pe));
                        break;
                    }
                }
            }
        }
        return result;
    }

    protected static boolean partialMatch(String title, ParsedSentence sentence) {

        boolean result = false;
        Matcher m = WikipediaUtils.partialMatchingRegEx(title).matcher(sentence.getSentence());

        while (m.find()) {

            String[] match = sentence.getSentence().substring(m.start(), m.end()).split(" ");
            StringBuilder pattern = new StringBuilder();
            for (int i = 0; i < match.length; i++) {
                pattern.append(match[i]);
                if (i != match.length - 1) {
                    pattern.append("|");
                }
            }

            for (CoreMap c : sentence.getAnnotation().get(CoreAnnotations.SentencesAnnotation.class)) {

                SemanticGraph sg = c.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);
                List<IndexedWord> l = sg.getAllNodesByWordPattern("(?i)" + pattern.toString().replaceAll("\\p{Punct}", " "));

                for (IndexedWord w : l) {
                    List<IndexedWord> pe = new LinkedList<>();

                    int index = -1;
                    for (int i = 0; i < match.length; i++) {
                        if (match[i].equals(w.lemma())) {
                            index = i;
                        }
                    }
                    //we ckeck that the nodes are correctly ordered
                    boolean matched = true;
                    for (int i = 0; i < match.length; i++) {

                        IndexedWord nodeCandidate = sg.getNodeByIndexSafe(w.index() - index + i);

                        if (nodeCandidate == null || !nodeCandidate.lemma().equals(match[i])) {
                            matched = false;
                            break;
                        }
                        pe.add(nodeCandidate);
                    }
                    if (matched) {
                        result = true;
                        sentence.addTitleMatch(new Entity(w, pe));
                    }
                }
            }
        }
        return result;
    }

    protected static boolean mostFrequentPronoum(String pron, ParsedSentence sentence) {

        if (pron.length() == 0) {
            return false;
        }

        boolean result = false;
        for (CoreMap c : sentence.getAnnotation().get(CoreAnnotations.SentencesAnnotation.class)) {
            SemanticGraph sg = c.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);
            List<IndexedWord> l = sg.getAllNodesByWordPattern("(?i)" + pron);
            for (IndexedWord w : l) {
                List<IndexedWord> a = new LinkedList();
                a.add(w);
                sentence.addTitleMatch(new Entity(w, a));
                result = true;
            }
        }
        return result;
    }

}
