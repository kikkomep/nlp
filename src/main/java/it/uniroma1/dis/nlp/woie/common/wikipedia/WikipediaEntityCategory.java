package it.uniroma1.dis.nlp.woie.common.wikipedia;

import javax.persistence.Entity;

/**
 * Created by kikkomep on 1/28/14.
 */
@Entity
public class WikipediaEntityCategory extends WikipediaCategory{

    public WikipediaEntityCategory() {
    }

    public WikipediaEntityCategory(String name) {
        super(name);
    }


    @Override
    public String toString() {
        return "WikipediaEntityCategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
