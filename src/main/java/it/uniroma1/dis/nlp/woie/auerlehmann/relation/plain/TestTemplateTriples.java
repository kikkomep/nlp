package it.uniroma1.dis.nlp.woie.auerlehmann.relation.plain;

import it.uniroma1.dis.nlp.woie.App;
import it.uniroma1.dis.nlp.woie.auerlehmann.AuerLehmannTemplateProcessor;
import it.uniroma1.dis.nlp.woie.common.wikipedia.*;
import it.uniroma1.dis.nlp.woie.utils.RelationsRepositoryFactory;

/**
 * Created by kikkomep on 12/14/13.
 */
public class TestTemplateTriples extends App {


    @Override
    public void start() {



        try {

            Wikipedia wiki = Wikipedia.getInstance();
            WikipediaPage page = wiki.findByTitle("pippo");

            AuerLehmannTemplateProcessor processor = new AuerLehmannTemplateProcessor();


            RelationsRepositoryFactory.getInstance().getAuerlehmannRepository().deleteBySubject("turing");

            page = wiki.loadByTitle("K-C3-A4rs-C3-A4m-C3-A4ki");
            processor.process(page);

            for(WikipediaTemplate t: page.getTemplates()){

                for(WikipediaTemplateAttributeAssociation a : t.getAttributes()){

                    if(a.getAttribute() instanceof WikipediaTemplate){

                        WikipediaTemplate tt = (WikipediaTemplate) a.getAttribute();

                        if(tt.getName().equals("collapsible list")){
                            logger.debug(t.getRawText());

                        }
                    }
                }
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Entry for the WuWeldApp
     *
     * @param args
     */
    public static void main(String args[]) {
        init(new TestTemplateTriples(), args);
    }
}
