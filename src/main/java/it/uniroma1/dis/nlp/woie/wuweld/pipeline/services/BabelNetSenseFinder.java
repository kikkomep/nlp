package it.uniroma1.dis.nlp.woie.wuweld.pipeline.services;

import it.uniroma1.dis.nlp.woie.wuweld.pipeline.ParseException;
import it.uniroma1.lcl.jlt.ling.Word;
import it.uniroma1.lcl.jlt.util.ScoredItem;
import it.uniroma1.lcl.knowledge.KnowledgeBase;
import it.uniroma1.lcl.knowledge.graph.KnowledgeGraph;
import it.uniroma1.lcl.knowledge.graph.KnowledgeGraphFactory;
import it.uniroma1.lcl.knowledge.graph.KnowledgeGraphScorer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by lgu on 03/02/14.
 */
public class BabelNetSenseFinder {

    private Collection<Word> words;
    private Map<Word,Set<ScoredItem<String>>> disambiguation;
    private boolean isDisambiguated;
    protected final Logger logger = LoggerFactory.getLogger(BabelNetSenseFinder.class);

    public BabelNetSenseFinder (Collection<Word> words){
        this.words = words;
        disambiguation = new HashMap<>();
        isDisambiguated = false;
    }

    private void disambiguate() throws ParseException {

        if(isDisambiguated){
           return;
        }

        try {

            KnowledgeGraphFactory factory = KnowledgeGraphFactory.getInstance(KnowledgeBase.BABELNET);
            KnowledgeGraph kGraph = factory.getKnowledgeGraph(words);

            StringBuilder str = new StringBuilder();
            str.append("Trying to disambiguate { ");
            Iterator<Word> iterator = words.iterator();
            while (iterator.hasNext()){
                Word word = iterator.next();
                str.append("<" + word.getWord() + ", " + word.getPOS() + ">");
                if(iterator.hasNext()) str.append(" && ");
            }
            logger.debug( str.toString() + " }");

            Map<String, Double> scores = KnowledgeGraphScorer.DEGREE.score(kGraph);

            for (String concept : scores.keySet()) {
                double score = scores.get(concept);
                for (Word word : kGraph.wordsForConcept(concept)){
                    word.addLabel(concept, score);
                }
            }

            for (Word word : words) {
                Set<ScoredItem<String>> bestSenses = word.getBestLabels();
                if(!bestSenses.isEmpty()){
                    disambiguation.put(word,bestSenses);
                }
            }

            isDisambiguated = true;

        } catch (Exception e) {
            throw new ParseException("Cannot disambiguate");
        }

    }

    public Set<ScoredItem<String>> getBestSenses(Word w) throws ParseException {
        disambiguate();
        if(disambiguation.containsKey(w)){
            return disambiguation.get(w);
        }
        throw new ParseException("Cannot disambiguate word "+w.getWord());
    }

    public String getOneBestSense(Word w) throws ParseException {
        disambiguate();
        if(disambiguation.containsKey(w)){
            return disambiguation.get(w).iterator().next().getItem();
        }
        throw new ParseException("Cannot disambiguate word "+w.getWord());
    }

    public void printDebugSense(){
        logger.debug("Printing Senses.. ");
        if(!isDisambiguated){
            return;
        }

        for(Word word:disambiguation.keySet()){
            logger.debug("Word: " + word.getWord());
            for(ScoredItem<String> stringScoredItem: disambiguation.get(word)){
                logger.debug("Sense: " + stringScoredItem.getItem());
            }
        }
    }
}

