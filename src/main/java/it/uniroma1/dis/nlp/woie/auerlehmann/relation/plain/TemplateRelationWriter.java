package it.uniroma1.dis.nlp.woie.auerlehmann.relation.plain;

import it.uniroma1.dis.nlp.woie.common.relation.Relation;
import it.uniroma1.dis.nlp.woie.common.relation.RelationWriter;
import it.uniroma1.dis.nlp.woie.common.relation.RelationsRepository;
import it.uniroma1.dis.nlp.woie.utils.RelationsRepositoryFactory;

/**
 * Created by kikkomep on 12/27/13.
 */
public class TemplateRelationWriter implements RelationWriter<Relation> {

    private final RelationsRepository repository;

    public TemplateRelationWriter() {
        this.repository = RelationsRepositoryFactory.getInstance().getAuerlehmannRepository();
    }


    @Override
    public void open() {

    }

    @Override
    public void close() {

    }

    @Override
    public Relation write(Relation relation) {
        repository.save(relation);
        return relation;
    }

    @Override
    public RelationsRepository<TemplateRelation> getRepository() {
        return repository;
    }
}
