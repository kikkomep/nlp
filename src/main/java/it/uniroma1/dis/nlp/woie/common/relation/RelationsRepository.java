package it.uniroma1.dis.nlp.woie.common.relation;

import it.uniroma1.dis.nlp.woie.auerlehmann.relation.plain.TemplateRelation;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kikkomep on 12/14/13.
 */
public class RelationsRepository<T extends Relation> implements IRelationsRepository<T> {


    private Class<T> type;

    private final EntityManager em;

    private EntityTransaction transaction = null;

    public RelationsRepository(EntityManager em, Class<T> type) {
        this.em = em;
        this.type = type;
    }


    public void startTransation() {
        this.transaction = this.em.getTransaction();
        this.transaction.begin();
    }

    public void closeTransaction() {
        try {
            this.em.flush();
            this.transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            this.transaction.rollback();
        }
    }

    @Override
    public List<String> findAllSubjects() {

        List<String> list = null;


        try {

            String typeName = type.getSimpleName();

            Query q = em.createQuery(
                    "SELECT DISTINCT t.subject FROM " + typeName + " t WHERE t.subject NOT LIKE '%:%' GROUP BY t.subject");
            list = q.getResultList();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }


    @Override
    public List<T> findAll() {

        List<T> list = null;


        try {

            String typeName = type.getSimpleName();

            Query q = em.createQuery(
                    "SELECT t FROM " + typeName + " t");
            list = q.getResultList();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }


    @Override
    public List<T> findBySubject(String subject) {

        List<T> list = null;

        try {

            String typeName = type.getSimpleName();

            /** Correct but with performance problems
             Query q = em.createQuery(
             "SELECT t FROM " + typeName + " t " +
             "WHERE t.subject = :subject " +
             "OR t.id in (SELECT d1.id FROM " + typeName +" d1, " + typeName + " d2 WHERE d1.subject = d2.object AND d2.subject = :subject)");
             */

            Query q = em.createQuery(
                    "SELECT t FROM " + typeName + " t " +
                            "WHERE t.subject = :subject ");

            q.setParameter("subject", subject);
            list = q.getResultList();

            for (T t : new ArrayList<T>(list)) {
                if (TemplateRelation.isBlankNode(t.getSubject(), t.getPredicate(), t.getObject()))
                    list.addAll(findBySubject(t.getObject()));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }


    @Override
    public void save(T relation) {

        if (this.transaction == null) {
            em.getTransaction().begin();
        }

        try {

            if (relation.getId() == null)
                em.persist(relation);
            else em.merge(relation);

            if (this.transaction == null) {
                em.flush();
                em.getTransaction().commit();
            }

        } catch (Exception e) {
            e.printStackTrace();
            if (this.transaction == null)
                em.getTransaction().rollback();
            else this.transaction.rollback();
        }
    }

    @Override
    public void delete(T relation) {

        if (this.transaction == null) {
            em.getTransaction().begin();
        }


        try {
            em.remove(relation);
            if (this.transaction == null) {
                em.flush();
                em.getTransaction().commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
    }


    @Override
    public void deleteBySubject(String subject) {


        if (this.transaction == null) {
            em.getTransaction().begin();
        }


        try {

            String typeName = type.getSimpleName();

            List<T> list = findBySubject(subject);
            for (T t : list)
                em.remove(t);

            if (this.transaction == null) {
                em.flush();
                em.getTransaction().commit();
            }

        } catch (Exception e) {
            e.printStackTrace();
            if (this.transaction == null)
                em.getTransaction().rollback();
            else this.transaction.rollback();
        }
    }
}