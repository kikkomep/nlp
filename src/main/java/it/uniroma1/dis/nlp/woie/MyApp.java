package it.uniroma1.dis.nlp.woie;


import com.google.common.collect.Multimap;
import edu.cmu.lti.ws4j.WS4J;
import edu.mit.jwi.item.IPointer;
import it.uniroma1.dis.nlp.woie.auerlehmann.relation.plain.TemplateRelation;
import it.uniroma1.dis.nlp.woie.common.relation.RelationsRepository;
import it.uniroma1.dis.nlp.woie.common.wikipedia.*;
import it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.ParseException;
import it.uniroma1.dis.nlp.woie.common.wikipedia.utils.WikipediaUtils;
import it.uniroma1.dis.nlp.woie.utils.RelationsRepositoryFactory;


import it.uniroma1.dis.nlp.woie.wuweld.pipeline.services.BabelNetSenseFinder;
import it.uniroma1.lcl.babelnet.*;
import it.uniroma1.lcl.jlt.ling.Word;
import it.uniroma1.lcl.jlt.util.Language;
import it.uniroma1.lcl.jlt.util.ScoredItem;
import it.uniroma1.lcl.jlt.util.Strings;
import it.uniroma1.lcl.knowledge.KnowledgeBase;
import it.uniroma1.lcl.knowledge.graph.KnowledgeGraph;
import it.uniroma1.lcl.knowledge.graph.KnowledgeGraphFactory;
import it.uniroma1.lcl.knowledge.graph.KnowledgeGraphPath;
import it.uniroma1.lcl.knowledge.graph.KnowledgeGraphScorer;


import java.io.File;
import java.io.IOException;
import java.util.*;


/**
 * Created by kikkomep on 12/14/13.
 */
public class MyApp extends App {

    @Override
    public void start() {
//
//
//        ProcessedPagesHistory history = ProcessedPagesHistory.getInstance();
//
//        logger.info("Number of pages: " + history.countProcessedPages());
//
//
//        int count = 0;
//        for(WikipediaPage page : Wikipedia.getInstance().findAll()){
//
//            history.addProcessedPage(page.getTitle());
//            logger.info("Adding: " + page.getTitle());
//
//            count++;
//            if(count==4804) break;
//        }
//
//        history.save();
//
//        if(true) return;


        Set<Word> words = new HashSet<>();

        Word verb = new Word("die", "v", Language.EN);

        words.add(new Word("Gary", "n", Language.EN));
        words.add(verb);
        words.add(new Word("London", "n", Language.EN));


        BabelNetSenseFinder finder = new BabelNetSenseFinder(words);

        try {
            logger.info("Sense: " + finder.getOneBestSense(verb));
        } catch (it.uniroma1.dis.nlp.woie.wuweld.pipeline.ParseException e) {
            e.printStackTrace();
        }


        if(true) return;


        //processPages();

        String WORD = "action";

        System.out.println(WS4J.runWUP("calculate","ask"));


        if(true) return;

        Wikipedia wikipedia = Wikipedia.getInstance();
        wikipedia.startSession();

        WikipediaCategory ct = wikipedia.getEntityCategory("kikkoX------------");

        logger.info(ct + " " + ct.getId());






        wikipedia.closeSession();

        logger.info(ct + " " + ct.getId());

        if(true) return;




        BabelNet babelNet = BabelNet.getInstance();



        edu.cmu.lti.jawjaw.pobj.POS pos = edu.cmu.lti.jawjaw.pobj.POS.n;



        for(String str : WS4J.findSynonyms(WORD, pos)){
            logger.info("Synonym: " + str);
        }

        for(String str : WS4J.findCauses(WORD, pos)){
            logger.info("Cause: " + str);
        }

        for(String str : WS4J.findAntonyms(WORD, pos)){
            logger.info("Antonym: " + str);
        }

        for(String str : WS4J.findMeronyms(WORD, pos)){
            logger.info("Meronym: " + str);
        }

        for(String str : WS4J.findHolonyms(WORD, pos)){
            logger.info("Holonym: " + str);
        }








        if (true) return;


        try {

            File f = new File("");


            RelationsRepository<TemplateRelation> repository = RelationsRepositoryFactory.getInstance().getAuerlehmannRepository();

            for (TemplateRelation relation : repository.findBySubject("Aaron")) {

                System.out.println(relation.toString());
            }


            repository.deleteBySubject("Aaron");


            if (true) return;

            WikipediaPage page = wikipedia.parse("Gary_Webb");

            for (WikipediaEntityCategory cat : page.getEntityCategories()) {
                System.out.println(cat.toString());
            }


            WikipediaCategory category = wikipedia.getPageCategory("Person");
            //wikipedia.getPageCategory("person");

            System.out.println(category);

            System.out.println("EntityCategory: " + wikipedia.getEntityCategory("PERSON"));


            if (true) return;


            for (WikipediaTemplate template : page.getTemplates()) {
                for (WikipediaTemplateAttributeAssociation attribute : template.getAttributes()) {

                    System.out.println(attribute.getName() + " " + attribute.getAttribute().getValue());

                    System.out.println("\n\n");

                    for (String str : WikipediaUtils.getLiteralAttributeValues(attribute.getAttribute()))
                        System.out.println(str);

                }
            }

        } catch (WikipediaPageNotFoundException e) {
            e.printStackTrace();
        } catch (WikipediaPageInputStreamNotAvailableException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (true) return;

    }


    public static void disambiguate(Collection<Word> words,
                                    KnowledgeBase kb, KnowledgeGraphScorer scorer) {

        try {
            KnowledgeGraphFactory factory = KnowledgeGraphFactory.getInstance(kb);
            System.out.println("X1");
            KnowledgeGraph kGraph = factory.getKnowledgeGraph(words);
            System.out.println("X2");

            Multimap<String, KnowledgeGraphPath> graph = kGraph.getConcept2paths ();
            for(String node : graph.keys()){
                System.out.println("- " + node + ": " + graph.get(node));
            }


            if(true) return;

            Map<String, Double> scores = scorer.score(kGraph);



            System.out.println("X3");
            for (String concept : scores.keySet()) {
                double score = scores.get(concept);
                for (Word word : kGraph.wordsForConcept(concept)){
                    word.addLabel(concept, score);
                }
//                BabelSynset synset = BabelNet.getInstance().getSynsetFromId(concept);
//                System.out.println("Synset: " + synset.getId() + " " + synset.getPOS());
//
//                for(BabelSense sense : synset.getSenses())
//                    System.out.println("\t + " + sense);
            }
            for (Word word : words) {
                System.out.println("\n\t" + word.getWord() + " -- ID " + word.getId() +
                        " => SENSE DISTRIBUTION: ");
                for (ScoredItem<String> label : word.getLabels()) {
                    System.out.println("\t  [" + label.getItem() + "]:" +
                            Strings.format(label.getScore()));
                }
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void test() {

        String sentenceAsString = "Gary Webb lives in New York";

        //List<Word> sentence = new ArrayList<>();







        List<Word> sentence = Arrays.asList(

                new Word[]{new Word("I", 'n', Language.EN), new Word("live", 'v', Language.EN),
                        new Word("in", 'p', Language.EN), new Word("New York", 'n', Language.EN)});


        System.out.println("XXXXXXXXXX");

        disambiguate(sentence, KnowledgeBase.BABELNET, KnowledgeGraphScorer.DEGREE);
    }



    /**
     * The snippet contained in our WWW-12 demo paper
     *
     */
    public static void www12Test() throws IOException
    {
        BabelNet bn = BabelNet.getInstance();
        System.out.println("SYNSETS WITH English word: \"bank\"");
        List<BabelSynset> synsets = bn.getSynsets(Language.EN, "bank");
        Collections.sort(synsets, new BabelSynsetComparator("bank"));
        for (BabelSynset synset : synsets)
        {
            System.out.print("  =>(" + synset.getId() + ") SOURCE: " + synset.getSynsetSource() +
                    "; TYPE: " + synset.getSynsetType() +
                    "; WN SYNSET: " + synset.getWordNetOffsets() + ";\n" +
                    "  MAIN LEMMA: " + synset.getMainSense() +
                    ";\n  IMAGES: " + synset.getImages() +
                    ";\n  CATEGORIES: " + synset.getCategories() +
                    ";\n  SENSES (German): { ");
            for (BabelSense sense : synset.getSenses(Language.DE))
                System.out.print(sense.toString()+" ");
            System.out.println("}\n  -----");
            Map<IPointer, List<BabelSynset>> relatedSynsets = synset.getRelatedMap();
            for (IPointer relationType : relatedSynsets.keySet())
            {
                List<BabelSynset> relationSynsets = relatedSynsets.get(relationType);
                for (BabelSynset relationSynset : relationSynsets)
                {
                    System.out.println("    EDGE " + relationType.getSymbol() +
                            " " + relationSynset.getId() +
                            " " + relationSynset.toString(Language.EN));
                }
            }
            System.out.println("  -----");
        }
    }

    /**
     * Entry for the WuWeldApp
     *
     * @param args
     */
    public static void main(String args[]) {
        init(new MyApp(), args);
    }
}