/*
 * Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
 * Mountain View, California, 94041, USA.
 */
package it.uniroma1.dis.nlp.woie.utils;

import java.util.HashMap;
import java.util.Properties;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author kikkomep
 */
public class AppEntityManagerFactory {

    public static enum NAME {pwr, wpr, atr, wtr, dtr, wws}

    /**
     *
     */
    private static AppEntityManagerFactory emf = null;
    /**
     *
     */
    private final Properties configProperties;
    /**
     *
     */
    private javax.persistence.EntityManagerFactory emFactory;
    /**
     *
     */
    private final HashMap<String, EntityManager> ems = new HashMap<>();


    /**
     * Initialize the EntityManegerFactory instance for the app
     *
     * @param config app instance
     */
    public static void init(Properties config) {
        if (emf == null) {
            emf = new AppEntityManagerFactory(config);
        }
    }

    /**
     * Return the current instance of EntityManager
     *
     * @return AppEntityManagerFactory the current configured instance
     */
    public static AppEntityManagerFactory getInstance() {
        if(emf==null) init(null);
        return emf;
    }

    private AppEntityManagerFactory(Properties config) {
        this.configProperties = config;
    }





    /**
     * Return a proper EntityManager
     *
     * @return EntityManeger the proper entity manager for the current app
     * instance
     */
    public EntityManager getEntityManager(NAME name) {

        EntityManager em = ems.get(name.name());
        if(em==null){
            em = buildEntityManager(name);
            if(em==null)
                throw new RuntimeException("Unable to create the entity manager wrt '" + name.name() + "'");

            ems.put(name.name(), em);
        }

        return em;
    }


    /**
     *
     * @param name
     * @return
     */

    private EntityManager buildEntityManager(NAME name){

        Properties prop = this.configProperties;

        String puName = "it.uniroma1.dis.nlp_lamepan_jar_1.0_" + name;

        String type = name.name();

        String url = "jdbc:mysql://" + prop.getProperty("host_" + type) + ":" + prop.
                getProperty("port_" + type) + "/" + prop.getProperty("db_" + type) + "?zeroDateTimeBehavior=convertToNull";
        String username = prop.getProperty("user_" + type);
        String password = prop.getProperty("passwd_" + type);



        Properties properties = new Properties();
        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
        properties.put("hibernate.connection.url", url);
        properties.put("hibernate.connection.username",username);
        properties.put("hibernate.connection.password",password);
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.cache.provider_class",
                "org.hibernate.cache.NoCacheProvider");

        Configuration config = new Configuration();
        config.setProperties(properties);

        emFactory = Persistence.
                createEntityManagerFactory(puName, properties);

        return (EntityManager) emFactory.createEntityManager();
    }
}
