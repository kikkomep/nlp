package it.uniroma1.dis.nlp.woie.common.wikipedia.services.synonym;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kikkomep on 12/30/13.
 */
public class WikipediaSynonymsFinder implements SynonymsFinder{

    private static final String wikiSynonymsAPI = "http://wikisynonyms.ipeirotis.com/api/";


    private final Logger logger = Logger.getLogger(SynonymsFinder.class);


    public List<String> findSynonyms(String term) {
        ArrayList<String> result = new ArrayList();
        result.add(term);

        try {

            logger.debug("Connecting... " + wikiSynonymsAPI + term);

            URL apiUrl = new URL(wikiSynonymsAPI + term);
            HttpURLConnection conn = (HttpURLConnection) apiUrl.openConnection();

            String str;
            StringBuilder text = new StringBuilder();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
                while (reader.ready()) {
                    str = reader.readLine();
                    text.append(str);
                }
            }


            Object data = null;
            JSONObject jsonResponse = (JSONObject) JSONValue.parse(text.toString());

            data = jsonResponse.get("terms");
            if(data!=null && data instanceof JSONArray){

                JSONArray synTerms = (JSONArray) data;

                for (int i = 0; i < synTerms.size(); i++) {

                    JSONObject synTermObject = (JSONObject)synTerms.get(i);
                    String synTerm = (String) synTermObject.get("term");
                    result.add(synTerm.toLowerCase());
                }
            }

            data = jsonResponse.get("term");
            if(data != null && data instanceof String){
                result.add((String) data);
            }


            logger.debug("Connection to " + wikiSynonymsAPI + " OK "+result.size()+" synonyms found");

        } catch (Exception ex) {
            logger.debug("Unable to find the synonyms for " + term);
        }

        return result;
    }

}
