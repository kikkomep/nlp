package it.uniroma1.dis.nlp.woie.wuweld.pipeline.filters;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.trees.SemanticHeadFinder;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.Match;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.ParsedSentence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by lgu on 05/02/14.
 */
public class SecondFilter {

    private static final Logger logger = LoggerFactory.getLogger(it.uniroma1.dis.nlp.woie.wuweld.pipeline.Pipeline.class);

    public static boolean isApplicable(ParsedSentence sentence, List<IndexedWord> attributeNodes, Match matchCandidate, SemanticGraph sg){

        //it rejects the sentence if the subject and/or attribute value are
        //not heads of the noun phrases containing them
        logger.debug("Second Filter");

        Tree tree = sentence.getPrincipalCoreMap().get(TreeCoreAnnotations.TreeAnnotation.class);
        SemanticHeadFinder shf = new SemanticHeadFinder();
        LinkedList<Tree> npheads = new LinkedList();

        //Getting NP heads
        for (Tree t : tree.preOrderNodeList()) {
            if (t.label().toString().equals("NP")) {

                Tree np = shf.determineHead(t);

                //root is the POS tag, leaf is the word
                npheads.addAll(np.getLeaves());
            }
        }

        //Checking NP heads
        boolean applySecondFilter = true;
        for (Tree root : npheads) {

            for (IndexedWord attr : attributeNodes) {
                if (attr.word().equalsIgnoreCase(root.label().toString())) {
                    logger.debug("I've found an head for the current sentence");

                    if (!attr.word().equalsIgnoreCase(matchCandidate.getObject().word())) {

                        logger.debug("Add path: " + matchCandidate.getSubject().word() + " " + attr.word());
                        matchCandidate.setShortestPath(sg.getShortestUndirectedPathEdges(matchCandidate.getSubject(), attr));
                        matchCandidate.setObject(attr);

                    }
                    applySecondFilter = false;
                    break;
                }
            }
        }

        if (applySecondFilter) {
            for (Tree root : npheads) {
//                                            logger.debug("Check " + matchCandidate.getSubject().word() + " == " + root.label());
                if (matchCandidate.getSubject().word().equalsIgnoreCase(root.label().toString())) {
                    logger.debug("I've found an head for the current sentence");
                    applySecondFilter = false;
                    break;
                }
            }
        }

        if(applySecondFilter) logger.debug("SecondFilter Applied!!");

        return applySecondFilter;

    }
}
