package it.uniroma1.dis.nlp.woie.auerlehmann.relation.plain;

import it.uniroma1.dis.nlp.woie.common.relation.Relation;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * Created by kikkomep on 12/21/13.
 */
@Entity
public class TemplateRelation extends Relation {


    public static enum OBJECTY_TYPE {

        RESOURCE,
        STRING,
        DOUBLE, LARGE_DOUBLE,
        INTEGER, LARGE_INTEGER,
        IMAGE, INTERNAL_LINK, URL,
        RANK,
        DATE,
        DATE_DAY,
        DATE_MONTH,
        DATE_YEAR,
        MONEY, BIG_MONEY,
        LARGE_NUMBER,
        YEAR_RANGE,
        PERCENT,
        UNIT,
        UNDEFINED;
        /*

        public String getValue(){

            switch (this){
                case RESOURCE: return "entity";
                case STRING: return  "xsd:string";
                case DOUBLE: return "xsd:double";
                case INTEGER: return "xsd:integer";
                case LARGE_INTEGER: return "xsd:integer";
                case IMAGE: return "Resource";
                case INTERNAL_LINK: return "Resource";
                case URL: return "Resource";
                case RANK: return "u:rank";
                case DATE: return "xsd:date";
                case DATE_DAY: return "xsd:date";
                case DATE_MONTH: return "xsd:date";
                case DATE_YEAR: return "xsd:date";
                case MONEY: return "u:Dollar";
                case YEAR_RANGE: return "u:range";
                case LARGE_NUMBER: return "xsd:Integer";
                case BIG_MONEY: return "u:Money";
                case PERCENT: return "u:Percent";
                case UNIT: return "u:Gram";
                default: return "undefined";
            }
        }
        */
    }


    @NotNull
    protected OBJECTY_TYPE objectType;

    protected String objectTypeName;

    public TemplateRelation() {
    }


    public TemplateRelation(String subject, String predicate, String object, OBJECTY_TYPE objectType) {
        super(subject, predicate, object);
        this.setObjectType(objectType);
    }


    public TemplateRelation(String subject, String predicate, String object, String objectType) {
        super(subject, predicate, object);
        setObjectType(OBJECTY_TYPE.valueOf(objectType));
    }

    public String getObjectTypeAsString() {
        return objectType != null ? objectType.toString() : null;
    }


    public OBJECTY_TYPE getObjectType() {
        return objectType;
    }

    public void setObjectType(OBJECTY_TYPE objectType) {
        this.objectType = objectType;
        this.objectTypeName = this.objectType.name();
    }


    public static long getBlankNode(String subject, String predicate) {
        return (predicate + "_@@@_" + subject).hashCode();
    }


    public static boolean isBlankNode(String subject, String predicate, String value) {
        try {
            return getBlankNode(subject, predicate) == Long.parseLong(value);
        } catch (Exception e) {
        }

        return false;
    }


    @Override
    public String toString() {
        return "TemplateRelation {" +
                "id=" + id +
                ", subject='" + subject + '\'' +
                ", predicate='" + predicate + '\'' +
                ", object='" + object + '\'' +
                ", objectType='" + objectType + '\'' +
                '}';
    }
}
