package it.uniroma1.dis.nlp.woie.utils;

import it.uniroma1.dis.nlp.woie.auerlehmann.relation.plain.TemplateRelation;
import it.uniroma1.dis.nlp.woie.common.dbpedia.DbPediaTriple;
import it.uniroma1.dis.nlp.woie.common.relation.RelationsRepository;

/**
 * Created by kikkomep on 12/21/13.
 */
public class RelationsRepositoryFactory {

    private final RelationsRepository dbPediaRepository;
    private final RelationsRepository wuweldRepository;
    private final RelationsRepository auerlehmannRepository;


    private static RelationsRepositoryFactory ourInstance = new RelationsRepositoryFactory();

    public static RelationsRepositoryFactory getInstance() {
        return ourInstance;
    }

    private RelationsRepositoryFactory() {
        AppEntityManagerFactory emf = AppEntityManagerFactory.getInstance();
        this.dbPediaRepository = new RelationsRepository<>(emf.getEntityManager(AppEntityManagerFactory.NAME.dtr), DbPediaTriple.class);
        this.wuweldRepository = new RelationsRepository<>(emf.getEntityManager(AppEntityManagerFactory.NAME.wtr), DbPediaTriple.class);
        this.auerlehmannRepository = new RelationsRepository<>(emf.getEntityManager(AppEntityManagerFactory.NAME.atr), TemplateRelation.class);
    }


    public RelationsRepository<DbPediaTriple> getDbPediaRepository() {
        return dbPediaRepository;
    }

    public RelationsRepository getWuweldRepository() {
        return wuweldRepository;
    }

    public RelationsRepository<TemplateRelation> getAuerlehmannRepository() {
        return auerlehmannRepository;
    }
}
