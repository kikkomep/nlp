package it.uniroma1.dis.nlp.woie.common.wikipedia;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by kikkomep on 12/18/13.
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class WikipediaEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;


    public WikipediaEntity() {
    }

    public Long getId() {
        return id;
    }


}
