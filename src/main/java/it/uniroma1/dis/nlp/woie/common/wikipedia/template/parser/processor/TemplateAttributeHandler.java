package it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.processor;

import it.uniroma1.dis.nlp.woie.common.wikipedia.*;
import it.uniroma1.dis.nlp.woie.common.wikipedia.utils.WikipediaUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by kikkomep on 12/18/13.
 */
class TemplateAttributeHandler {

    private Logger logger = Logger.getLogger(TemplateAttributeHandler.class);

    //enum TYPE {Literal, Link}


    private boolean specialType = true;
    private boolean valid = true;
    private String name;
    private HashMap<WikipediaTemplateLiteralAttribute.TYPE, Integer> conts = new HashMap<>();
    private WikipediaTemplateLiteralAttribute.TYPE type;
    private boolean disableNesting = false;
    private StringBuilder rawText = new StringBuilder();
    private ArrayList<WikipediaTemplateLiteralAttribute> values = new ArrayList<>();
    private HashMap<String, WikipediaTemplateLiteralAttribute> properties = new HashMap<>();


    public boolean isValid() {
        return valid;
    }

    public void invalidate() {
        this.valid = false;
    }

    String getName() {
        return name;
    }

    ArrayList<WikipediaTemplateLiteralAttribute> getValues() {
        return values;
    }

    void setName(String name) {
        this.name = name.trim();
    }


    void setDisableNesting(boolean disableNesting) {
        this.disableNesting = disableNesting;
    }

    void addSpecialToken(String token) {
        this.rawText.append(token);
    }


    WikipediaTemplateValueAttribute getHead() {
        if (this.values.size() > 0) {
            return this.values.remove(0);
        } else {
            return null;
        }
    }


    void addValue(WikipediaTemplateLiteralAttribute.TYPE type, String value) {
        if (value != null) {

            String rawValue = "" + value;

            if (value.startsWith(","))
                value = value.substring(1, value.length());

            if (value.endsWith(","))
                value = value.substring(0, value.length() - 1);

            value = value.trim();

            if (value.isEmpty()) return;

            Integer count = 0;
            if (this.conts.containsKey(type))
                count = this.conts.get(type);

            count += 1;
            this.conts.put(type, count);

            if (!disableNesting) {
                WikipediaTemplateLiteralAttribute v = new WikipediaTemplateLiteralAttribute(type, value);
                v.setRawValue(rawValue);
                this.values.add(v);
            }

            this.rawText.append(rawValue);
        }
    }

    void addProperty(String name, WikipediaTemplateLiteralAttribute.TYPE type, String value) {
        if (value != null) {


            WikipediaTemplateLiteralAttribute v = new WikipediaTemplateLiteralAttribute(type, value.trim());
            this.properties.put(name, v);


            this.rawText.append(value);
        }
    }


    void clear() {
        this.specialType = true;
        this.valid = true;
        this.name = null;
        this.conts.clear();
        this.type = WikipediaTemplateLiteralAttribute.TYPE.STRING;
        this.disableNesting = false;
        this.rawText.delete(0, rawText.length());
        this.values.clear();
        this.properties.clear();
    }


    WikipediaTemplateAttributeAssociation getAttribute() {


        Object[] mf = findMostFrequent();
        WikipediaTemplateLiteralAttribute.TYPE mostFrequent = (WikipediaTemplateLiteralAttribute.TYPE) mf[0];


        logger.debug("\n *** Building the wikipediaAttribute: " + name + " TYPE: " + mostFrequent);
        logger.debug("\t- Number of values: " + this.values.size());
        logger.debug("\t- Values: " + this.values);
        logger.debug("\t- Number of properties: " + this.properties.size());
        logger.debug("***\n");


        WikipediaTemplateAttributeAssociation attr = null;
        WikipediaTemplateAttribute value = null;


        if (this.values.size() > 0) {

            double TRESHOLD = 0.8;
            double frequency = (double) mf[1];


            if (this.values.size() == 1) {

                value = values.get(0);
                attr = new WikipediaTemplateAttributeAssociation(name, value);

            } else {

                String text = this.rawText.toString();
                text = text.replaceAll("([^\\s])(\\[\\[)", "$1 $2");
                text = text.replaceAll("(\\]\\])([^\\s])", "$1 $2");
                text = text.trim();

                text = WikipediaUtils.toPlainText(text);

                if (!text.isEmpty()) {
                    if (mf[0] != WikipediaTemplateLiteralAttribute.TYPE.STRING) {

                        List<WikipediaTemplateLiteralAttribute> filteredList = new ArrayList<>();

                        if (frequency >= TRESHOLD) {

                            for (WikipediaTemplateLiteralAttribute candidate : this.values)
                                if (candidate.getLiteralType() == mostFrequent)
                                    filteredList.add(candidate);

                        } else {

                            for (WikipediaTemplateLiteralAttribute candidate : this.values)
                                if (candidate.getLiteralType() != WikipediaTemplateLiteralAttribute.TYPE.STRING)
                                    filteredList.add(candidate);
                        }

                        value = new WikipediaTemplateComplexAttribute(filteredList);
                        value.setValue(text);

                    } else {
                        value = new WikipediaTemplateLiteralAttribute(WikipediaTemplateLiteralAttribute.TYPE.STRING, text);
                    }
                }

                if (value != null)
                    attr = new WikipediaTemplateAttributeAssociation(name, value);
            }
        }

        if (attr != null) {

            attr.getAttribute().setRawValue(this.rawText.toString().trim());

            for (String name : this.properties.keySet()) {
                attr.addProperty(name, this.properties.get(name).getValue());
            }
        }

        return attr;
    }


    private Object[] findMostFrequent() {

        int max = -1;
        int tot = 0;

        WikipediaTemplateLiteralAttribute.TYPE mostFrequentType = WikipediaTemplateLiteralAttribute.TYPE.STRING;


        HashMap<WikipediaTemplateLiteralAttribute.TYPE, Integer> counts = new HashMap<>();


        for (WikipediaTemplateLiteralAttribute value : this.values) {
            if (value.getLiteralType() == WikipediaTemplateLiteralAttribute.TYPE.STRING && value.getValue().trim().length() < 3)
                continue;

            Integer c = 0;
            if (counts.containsKey(value.getLiteralType()))
                c = counts.get(value.getLiteralType());

            c++;
            tot++;
            counts.put(value.getLiteralType(), c);

            if (max < c || (max == c && value.getLiteralType() != WikipediaTemplateLiteralAttribute.TYPE.STRING)) {
                mostFrequentType = value.getLiteralType();
                max = c;
            }
        }

        double frequency = 0;
        if (tot > 0) frequency = (double) max / tot;

        return new Object[]{mostFrequentType, frequency};
    }

    private Object[] findMostFrequent_() {

        int max = -1;
        int tot = 0;
        WikipediaTemplateLiteralAttribute.TYPE frq = WikipediaTemplateLiteralAttribute.TYPE.STRING;
        for (WikipediaTemplateLiteralAttribute.TYPE t : this.conts.keySet()) {
            Integer c = this.conts.get(t);
            tot += c;
            if (c > max) {
                max = c;
                frq = t;
            }
        }

        double fr = 0;
        if (tot > 0) fr = (double) max / tot;

        return new Object[]{frq, fr};
    }
}
