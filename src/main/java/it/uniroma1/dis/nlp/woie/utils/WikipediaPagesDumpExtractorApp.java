package it.uniroma1.dis.nlp.woie.utils;


import info.bliki.wiki.dump.IArticleFilter;
import info.bliki.wiki.dump.Siteinfo;
import info.bliki.wiki.dump.WikiArticle;
import info.bliki.wiki.dump.WikiXMLParser;
import info.bliki.wiki.filter.Encoder;
import info.bliki.wiki.model.WikiModel;
import it.uniroma1.dis.nlp.woie.App;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by kikkomep on 12/14/13.
 */
public class WikipediaPagesDumpExtractorApp extends App {

    private static Logger logger = Logger.getLogger(WikipediaPagesDumpExtractorApp.class);

    private String WIKI_DUMP_PATH;
    private String WIKI_DATA_PATH;


    @Override
    public void start() {

        Properties prop = App.getInstance().getConfigurationProperties();
        WIKI_DUMP_PATH = (String) prop.get("wiki_dump");
        WIKI_DATA_PATH = (String) prop.get("wiki_pages_folder");
        File path = new File(WIKI_DATA_PATH);
        if (!path.exists())
            path.mkdirs();

        logger.info("XmlDump  Folder: " + WIKI_DUMP_PATH);
        logger.info("WikiData Folder: " + WIKI_DATA_PATH);

        logger.info("Articles ......: ");

        try {

            int limit = Integer.parseInt(getArgumentValue("limit"));
            boolean verbose = getArgumentNames().contains("verbose");


            ListPageFilter filter = new ListPageFilter(WIKI_DATA_PATH, limit, verbose);

            WikiXMLParser wxp = null;
            try {

                wxp = new WikiXMLParser(WIKI_DUMP_PATH, filter);
                wxp.parse();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
            }

        } catch (NumberFormatException e) {
            System.out.println("\nERROR: you have to specify the '--limit' parameter!!!\n");
        }
    }

    static class ListPageFilter implements IArticleFilter {


        private final String outputPath;
        private int count = 0;
        private int limit;
        private boolean printSkips = false;


        ListPageFilter(String outputPath, int limit, boolean printSkips) {
            this.outputPath = outputPath;
            this.limit = limit;
            this.printSkips = printSkips;
        }


        // Convert to plain text
        WikiModel wikiModel = new WikiModel("${image}", "${title}");

        public void process(WikiArticle page, Siteinfo siteinfo) throws SAXException {

            if (page == null) return;

            String title = Encoder.encodeTitleToUrl(page.getTitle(), true);

            //String title = page.getTitle();

            String dir = outputPath+"";

            String[] subPaths = title.split("/");
            for(int i=0; i<subPaths.length-1; i++){
                String subPath = subPaths[i];
                dir += File.separator + subPath;
            }


            title = subPaths[subPaths.length-1];


            File outputFile = new File(dir);

            if(!outputFile.exists())
                outputFile.mkdirs();


            logger.debug(outputFile.getAbsolutePath() + File.separator + title + ".txt");



            outputFile = new File(outputFile.getAbsolutePath() + File.separator + title + ".txt");

            if (!outputFile.exists()) {

                logger.info("  -> Extracting " + title + " ... OK");


                if (page.getText() != null) {

                    Long lastModified = Calendar.getInstance().getTime().getTime();
                    Pattern pattern = Pattern.compile("([^T])+");
                    Matcher matcher = pattern.matcher(page.getTimeStamp());
                    if (matcher.find()) {
                        String d = matcher.group(0);
                        lastModified = java.sql.Date.valueOf(d).getTime();
                    }


                    InputStream in =
                            new BufferedInputStream(new ByteArrayInputStream(page.getText().getBytes()));
                    try {

                        OutputStream out = new FileOutputStream(outputFile);
                        IOUtils.copy(in, out);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    count++;

                    if ((count) > this.limit)
                        throw new SAXException("FINISHED");

                }

            }else{

                if(this.printSkips)
                    logger.info("  -> Skipped file: " + title);
            }
        }
    }


    /**
     * Entry for the WuWeldApp
     *
     * @param args
     */
    public static void main(String args[]) {
        init(new WikipediaPagesDumpExtractorApp(), args);
    }
}
