package it.uniroma1.dis.nlp.woie.wuweld.pipeline.services;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.Generalizator;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.ParseException;
import it.uniroma1.lcl.jlt.ling.Word;
import it.uniroma1.lcl.jlt.util.Language;

import java.util.Collection;

/**
 * Created by lgu on 03/02/14.
 */
public abstract class Expander {

    protected SemanticGraph semanticGraph;
    protected IndexedWord principalWord;

    protected Expander(SemanticGraph semanticGraph, IndexedWord principalWord) {
        this.semanticGraph = semanticGraph;
        this.principalWord = principalWord;

    }

    public abstract String getExpandedWordString();

    public abstract Collection<IndexedWord> getIndexedWordCollection();

    public abstract Collection<Word> getWordCollection();

    public static Word transformIndexedWord(IndexedWord word) throws ParseException{
        Generalizator generalizator = Generalizator.getInstance();

        return new Word(word.lemma(), generalizator.transtormPOSForBabelNet(word.tag()), Language.EN);
    }


    public static Word transformIndexedWordToLemma(IndexedWord word) throws ParseException{
        Generalizator generalizator = Generalizator.getInstance();

        return new Word(word.lemma(), generalizator.transtormPOSForBabelNet(word.tag()), Language.EN);
    }
}
