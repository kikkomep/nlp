package it.uniroma1.dis.nlp.woie.wuweld.woe.pattern;

/**
 * Created by kikkomep on 12/28/13.
 */
public class GeneralizedCorePathsRepository extends DependencyPathsRepository<GeneralizedCorePath> {

    private static final GeneralizedCorePathsRepository instance = new GeneralizedCorePathsRepository();

    private GeneralizedCorePathsRepository() {
        super(GeneralizedCorePath.class);
    }

    public static GeneralizedCorePathsRepository getInstance(){
        return instance;
    }
}
