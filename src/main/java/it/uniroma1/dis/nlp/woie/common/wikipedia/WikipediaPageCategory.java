package it.uniroma1.dis.nlp.woie.common.wikipedia;

import javax.persistence.Entity;

/**
 * Created by kikkomep on 1/28/14.
 */
@Entity
public class WikipediaPageCategory extends WikipediaCategory{



    public WikipediaPageCategory() {
    }

    public WikipediaPageCategory(String name) {
        super(name);
    }


    @Override
    public String toString() {
        return "WikipediaEntityCategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
