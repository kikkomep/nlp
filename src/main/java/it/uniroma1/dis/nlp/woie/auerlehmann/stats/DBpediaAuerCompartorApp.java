package it.uniroma1.dis.nlp.woie.auerlehmann.stats;

import it.uniroma1.dis.nlp.woie.App;
import it.uniroma1.dis.nlp.woie.auerlehmann.relation.plain.TemplateRelation;
import it.uniroma1.dis.nlp.woie.common.relation.Relation;
import it.uniroma1.dis.nlp.woie.common.relation.RelationsRepository;
import it.uniroma1.dis.nlp.woie.common.wikipedia.utils.WikipediaProcessedPageHistory;
import it.uniroma1.dis.nlp.woie.common.dbpedia.DbPediaTriple;
import it.uniroma1.dis.nlp.woie.utils.RelationsRepositoryFactory;
import org.hibernate.tool.hbm2x.StringUtils;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Antonio on 09/01/14.
 */
public class DBpediaAuerCompartorApp extends App {


    private RelationsRepository<TemplateRelation> repositoryAuer;
    private RelationsRepository<DbPediaTriple> repositoryDBpedia;


    private DbpediaAuerStatistics statistics;


    @Override
    public void start() {

        repositoryAuer = RelationsRepositoryFactory.getInstance().getAuerlehmannRepository();
        repositoryDBpedia = RelationsRepositoryFactory.getInstance().getDbPediaRepository();

        statistics = new DbpediaAuerStatistics();


        boolean verbose = getArgumentNames().contains("verbose");
        boolean withInfoboxes = getArgumentNames().contains("with-infoboxes");
        boolean onlyInfoboxes = getArgumentNames().contains("only-infoboxes");
        boolean listFile = getArgumentNames().contains("file-list");

        logger.info("Verbose mode: " + verbose);
        logger.info("With Infoboxes: " + withInfoboxes);
        logger.info("File list: " + getArgumentValue("file-list"));

        logger.info("\n");

        if (getArgumentNames().contains("subject")) {

            String subject = getArgumentValue("subject");
            compareSubject(subject, verbose);

            statistics.printStatistcs();


        } else if (listFile || getArgumentNames().contains("limit") || getArgumentNames().contains("all")) {


            try {


                int limit = 0;

                WikipediaProcessedPageHistory history = WikipediaProcessedPageHistory.getInstance();

                Set<String> subjects = history.getProcessedPages();

                if (listFile) {
                    String fileName = getArgumentValue("file-list");
                    subjects = getListOfSubjectsFromFile(fileName);
                    limit = subjects.size();

                } else if (getArgumentNames().contains("all"))
                    limit = subjects.size();

                else

                    limit = Integer.parseInt(App.getInstance().getArgumentValue("limit"));


                logger.info("\nNumber of pages to process: " + limit + "\n");

                int count = 0;
                for (String subject : subjects) {


                    if (onlyInfoboxes && !history.containsOnlyInfoboxes(subject)) {
                        logger.info(" -> Skipping " + subject + ": it doesn't contain only infoboxes !!!");
                        continue;
                    } else if (withInfoboxes && !history.containsInfoboxes(subject)) {
                        logger.info(" -> Skipping " + subject + ": it doesn't contain infoboxes !!!");
                        continue;
                    }


                    if (compareSubject(subject, verbose)) {

                        count++;
                        if (count == limit)
                            break;
                    }
                }


                statistics.printStatistcs();


            } catch (NumberFormatException e) {
                printUsage();
            }
        } else {
            printUsage();
        }
    }

    private Set<String> getListOfSubjectsFromFile(String listFile) {
        Set<String> set = new HashSet<>();

        BufferedReader in = null;

        try {

            String line = null;
            File path = new File(listFile.startsWith("/") ? listFile : "." + File.separator + listFile);
            in = new BufferedReader(new InputStreamReader(new FileInputStream(path)));

            do {

                line = in.readLine();
                if (line != null) {
                    String[] subjects = line.split(",");
                    for (String subject : subjects)
                        set.add(subject);
                }

            } while (line != null);


        } catch (FileNotFoundException e) {
            logger.info("ERROR: you have to specify a valida file name !!!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return set;
    }

    private void printUsage() {
        System.out.println("\nERROR: you have to specify a parameter: \n" +
                "\n       [ --subject + <SUBJECT> ]: e.g., --subject Aristotle" +
                "\n       [ --limit + <NUMBER_OF_SUBJECTS> ]: e.g., --limit 100 " +
                "\n       [ --all ]: for all subjects");
    }


    private boolean compareSubject(String subject, boolean verbose) {

        Set<TemplateRelation> fullMatches = new HashSet<>();
        Set<TemplateRelation> partialMatches = new HashSet<>();
        Set<TemplateRelation> falsePositive;
        Set<DbPediaTriple> falseNegative;

        List<DbPediaTriple> dbPediaTriples = repositoryDBpedia.findBySubject(subject);


        if (dbPediaTriples.size() == 0) {

            logger.info(" -> Skipping " + subject + ": its DbPedia Triples are not available !!!");
            return false;
        }


        logger.info(" ** Processing: " + subject);

        List<TemplateRelation> auerLehamannTriples = repositoryAuer.findBySubject(subject);


        falsePositive = new HashSet<>(auerLehamannTriples);
        falseNegative = new HashSet<>(dbPediaTriples);


        int totalDbPediaTriples = dbPediaTriples.size();

        logger.debug(subject + ", Auer -> " + auerLehamannTriples.size() + ", DBpedia -> " + dbPediaTriples.size());

        HashSet<DbPediaTriple> fullMeaningful = new HashSet<>();
        HashSet<DbPediaTriple> partialMeaningful = new HashSet<>();
        HashSet<DbPediaTriple> totalMeaningful = new HashSet<>();
        HashMap<TemplateRelation, Set<DbPediaTriple>> partial = new HashMap<>();

        for (TemplateRelation templateRelation : auerLehamannTriples) {


            logger.debug("\n ===> Auer: " + templateRelation);


            for (DbPediaTriple dbPediaTriple : dbPediaTriples) {


                if (toCamelCase(templateRelation.getPredicate()).equalsIgnoreCase(dbPediaTriple.getPredicate())) {

                    logger.debug("\t DBPEDIA: " + dbPediaTriple);

                    logger.debug("\t\t--> predicate OK!!!");

                    String object = templateRelation.getObject().toUpperCase();


                    logger.debug("\t\t--> Checking object: " + object + " " + normalizeLinks(object).toUpperCase());
                    if (compareObjects(object, dbPediaTriple.getObject(), templateRelation.getObjectType())) {

                        logger.debug("\t\t--> object OK!!!");


                        if (templateRelation.getObjectType().name().equals(dbPediaTriple.getType().name())
                                ||
                                (
                                        (templateRelation.getObjectType().name().equalsIgnoreCase("INTERNAL_LINK") ||
                                                templateRelation.getObjectType().name().equalsIgnoreCase("IMAGE")
                                        ) && dbPediaTriple.getType().name().equalsIgnoreCase("RESOURCE")
                                )) {


                            if (!fullMeaningful.contains(dbPediaTriple)) {
                                fullMeaningful.add(dbPediaTriple);
                            }

                            if (partialMeaningful.contains(dbPediaTriple)) {
                                partialMeaningful.remove(dbPediaTriple);
                            }

                            if (!totalMeaningful.contains(dbPediaTriple))
                                totalMeaningful.add(dbPediaTriple);

                            fullMatches.add(templateRelation);
                            falsePositive.remove(templateRelation);
                            falseNegative.remove(dbPediaTriple);

                        } else {

                            if (!partialMeaningful.contains(dbPediaTriple)) {
                                partialMeaningful.add(dbPediaTriple);
                            }

                            if (!totalMeaningful.contains(dbPediaTriple))
                                totalMeaningful.add(dbPediaTriple);

                            partialMatches.add(templateRelation);
                            falsePositive.remove(templateRelation);
                            falseNegative.remove(dbPediaTriple);
                        }

                    } else {
                        Set<DbPediaTriple> set = partial.get(templateRelation);
                        if (set == null) {
                            set = new HashSet<>();
                            partial.put(templateRelation, set);
                        }
                        if(!set.contains(dbPediaTriple))
                            set.add(dbPediaTriple);
                    }
                }
            }

            if (partial.containsKey(templateRelation) && totalMeaningful.contains(templateRelation)) {
                partial.remove(templateRelation);
            }
        }



        for (TemplateRelation relation : partial.keySet()) {
            Set<DbPediaTriple> set = partial.get(relation);
            Iterator<DbPediaTriple> iterator = partial.get(relation).iterator();
            while (iterator.hasNext()) {
                DbPediaTriple triple = iterator.next();
                if (totalMeaningful.contains(triple)) {
                    set.remove(dbPediaTriples);
                }
            }

            if (set.size() > 0) {

                for (DbPediaTriple dbPediaTriple : set) {
                    if (!partialMeaningful.contains(dbPediaTriple)) {
                        partialMeaningful.add(dbPediaTriple);
                    }

                    if (!totalMeaningful.contains(dbPediaTriple)){
                        totalMeaningful.add(dbPediaTriple);
                    }

                    falseNegative.remove(dbPediaTriple);
                }

                partialMatches.add(relation);
                falsePositive.remove(relation);

            }else{
                partial.remove(relation);
            }
        }


        for (DbPediaTriple t : new ArrayList<>(partialMeaningful)) {
            if (fullMeaningful.contains(t))
                partialMeaningful.remove(t);
        }


        statistics.addPage();
        statistics.addPartialMeaningfulMatchedTriples(partialMeaningful.size());
        statistics.addFullMeaningfulMatchedTriples(fullMeaningful.size());
        statistics.addGlobalMeaningfulMatchedTriples(totalMeaningful.size());
        statistics.addMeaningfulTriples(totalDbPediaTriples);
        statistics.addFullMatches(fullMatches.size());
        statistics.addPartialMatches(partialMatches.size());
        statistics.addFalsePositiveMatches(falsePositive.size());
        statistics.addFalseNegativeMatches(falseNegative.size());

        if (verbose) {
            logger.info("\n\n***** STATISTICs *****");

            logger.info("\n\n\t - AuerLehmann Triples: " + auerLehamannTriples.size());
            logger.info("\n\t - DBPedia Triples: " + totalDbPediaTriples);

            logger.info("\n\t - Full Matched Triples: " + fullMatches.size());
            for (Relation r : fullMatches) {
                logger.info("\t\t - " + r);
            }

            logger.info("\n\t - Partial Matched Triples: " + partialMatches.size());
            for (Relation r : partialMatches) {
                logger.info("\t\t - " + r);
            }

            logger.info("\n\t - Partial Matching with predicate only: " + partial.size());
            for (Relation r : partial.keySet()) {
                StringBuilder info = new StringBuilder();
                for(DbPediaTriple dbPediaTriple : partial.get(r)){
                    if(info.length()>0) info.append(" | ");
                    info.append(dbPediaTriple.getObject());
                }
                logger.info("\t\t - " + r + " --> {" + info.toString() + "}");

            }


            logger.info("\n\t - False Positives Triples: " + falsePositive.size());
            for (Relation r : falsePositive) {
                logger.info("\t\t - " + r);
            }

            logger.info("\n\t - False Negatives Triples: " + falseNegative.size());
            for (Relation r : falseNegative) {
                logger.info("\t\t - " + r);
            }



        }

        return true;
    }


    private static boolean compareObjects(String obj1, String obj2, TemplateRelation.OBJECTY_TYPE type) {

        boolean result = false;

        try {
            switch (type) {

                case DOUBLE:
                case INTEGER:
                case MONEY:
                case LARGE_NUMBER:
                case YEAR_RANGE:
                case BIG_MONEY:


                    BigDecimal n1 = BigDecimal.valueOf(Double.parseDouble(obj1.replaceAll(",", "")));
                    BigDecimal n2 = BigDecimal.valueOf(Double.parseDouble(obj2.replaceAll(",", "")));
                    result = n1.equals(n2);
                    break;

                default:
                    result = ((obj1).contains(obj2.toUpperCase())
                            || normalizeLinks(obj1).contains(obj2.toUpperCase())
                            || obj2.toUpperCase().contains(obj1)
                            || obj2.toUpperCase().contains(normalizeLinks(obj1)));
                    break;
            }

        } catch (Exception e) {
        }

        return result;
    }


    private static String toCamelCase(String str) {

        StringBuilder builder = new StringBuilder();
        String[] tokens = str.split("_|\\s");
        for (String tk : tokens) {
            tk = tk.toLowerCase();
            builder.append(builder.length() > 0 ? StringUtils.capitalise(tk) : tk);
        }

        //logger.debug("" + str + " " + builder.toString());

        return builder.toString();
    }


    private static String normalizeLinks(String str) {
        return str.replaceAll("\\s", "_");
    }

    /**
     * Entry for the WuWeldApp
     *
     * @param args
     */
    public static void main(String args[]) {
        init(new DBpediaAuerCompartorApp(), args);
    }
}