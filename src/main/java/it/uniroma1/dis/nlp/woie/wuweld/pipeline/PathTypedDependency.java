package it.uniroma1.dis.nlp.woie.wuweld.pipeline;

import edu.stanford.nlp.semgraph.SemanticGraph;

import java.util.List;

/**
 * Created by lgu on 25/01/14.
 */
public class PathTypedDependency {

    private List<String> path;
    private SemanticGraph semanticGraphCCProcessedIncludeExtra;

    public PathTypedDependency(List<String> path, SemanticGraph semanticGraphCCProcessedIncludeExtra) {
        this.path = path;
        this.semanticGraphCCProcessedIncludeExtra = semanticGraphCCProcessedIncludeExtra;
    }

    public List<String> getPath() {
        return path;
    }

    public SemanticGraph getSemanticGraphCCProcessedIncludeExtra() {
        return semanticGraphCCProcessedIncludeExtra;
    }
}

