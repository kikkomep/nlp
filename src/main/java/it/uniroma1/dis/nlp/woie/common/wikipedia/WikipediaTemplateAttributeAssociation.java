package it.uniroma1.dis.nlp.woie.common.wikipedia;

import javax.persistence.*;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by kikkomep on 12/18/13.
 */
@Entity
public class WikipediaTemplateAttributeAssociation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @Lob
    protected String name;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    protected WikipediaTemplateAttribute owner;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    protected WikipediaTemplateAttribute attribute;

    @ManyToOne(optional = true, cascade = CascadeType.MERGE)
    protected WikipediaCategoryAttributeCorePath dependencyPath;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "attributeAssociation")
    protected List<WikipediaTemplateAttributeProperty> properties = new LinkedList<>();


    public WikipediaTemplateAttributeAssociation() {
    }


    public WikipediaTemplateAttributeAssociation(String name, WikipediaTemplateAttribute attribute) {
        this.name = name;
        this.attribute = attribute;
    }




    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    void setParent(WikipediaTemplateAttribute parent) {
        this.owner = parent;
    }

    public WikipediaTemplateAttribute getParent() {
        return owner;
    }

    public WikipediaTemplateAttribute getAttribute() {
        return attribute;
    }

    public void addProperty(String name, String value) {
        if (name != null) {
            WikipediaTemplateLiteralAttribute l = new WikipediaTemplateLiteralAttribute(WikipediaTemplateLiteralAttribute.TYPE.STRING, value);
            this.properties.add(new WikipediaTemplateAttributeProperty(name, l, this));
        }
    }

    public void removeProperty(WikipediaTemplateAttributeProperty property) {
        if (property != null) {
            this.properties.remove(property);
        }
    }


    public List<WikipediaTemplateAttributeProperty> getProperties() {
        return this.properties;
    }

    public WikipediaCategoryAttributeCorePath getDependencyPath() {
        return dependencyPath;
    }

    public void setDependencyPath(WikipediaCategoryAttributeCorePath dependencyPath) {
        this.dependencyPath = dependencyPath;
    }

    @Override
    public String toString() {
        return "WikipediaTemplateAttributeAssociation{" +
                "value='" + attribute + '\'' +
                '}';
    }
}
