package it.uniroma1.dis.nlp.woie.wuweld.woe.pattern;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by kikkomep on 12/11/13.
 *
 */
@MappedSuperclass
public class DependencyPath {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;


    @NotNull
    protected String path;

    protected Long frequency=0L;
    
    public DependencyPath() {
    }

    public DependencyPath(String path) {
        this.path = path;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getPath() {
        return path;
    }

    public Long getFrequency() {
        return frequency;
    }

    public void setFrequency(Long counts) {
        this.frequency = counts;
    }

    public void increaseFrequencyByOne(){
        this.frequency += 1;
    }

    public boolean equalsTo(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DependencyPath dependencyPath = (DependencyPath) o;

        if (path != null ? !path.equals(dependencyPath.path) : dependencyPath.path != null) return false;

        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DependencyPath)) return false;

        DependencyPath that = (DependencyPath) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!path.equals(that.path)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + path.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "DependencyPath{" +
                "id=" + id +
                ", path='" + path + '\'' +
                '}';
    }
    
    public String[] getPathStrings(){
        return path.split("#");
    }
}
