/*
 * Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
 * Mountain View, California, 94041, USA.
 */
package it.uniroma1.dis.nlp.woie.common.wikipedia;

/**
 *
 * @author kikkomep
 */
public class WikipediaPageNotFoundException extends Exception {

    /**
     * Creates a new instance of
     * <code>WikipediaPageNotFoundException</code> without detail message.
     */
    public WikipediaPageNotFoundException() {
    }

    /**
     * Constructs an instance of
     * <code>WikipediaPageNotFoundException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public WikipediaPageNotFoundException(String msg) {
        super(msg);
    }

    /**
     * 
     * @param message
     * @param cause 
     */
    public WikipediaPageNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
