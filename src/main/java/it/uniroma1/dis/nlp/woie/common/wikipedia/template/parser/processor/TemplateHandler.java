package it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.processor;

import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaTemplate;
import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaTemplateAttributeAssociation;

/**
 * Created by kikkomep on 12/19/13.
 */
public class TemplateHandler {

    private TemplateHandler parent;

    private WikipediaTemplate template;

    private StringBuilder rawText = new StringBuilder();

    public TemplateHandler(TemplateHandler parent, WikipediaTemplate template) {
        this.parent = parent;
        this.template = template;
    }

    public TemplateHandler(WikipediaTemplate template) {
        this.template = template;
    }

    public TemplateHandler getParent() {
        return parent;
    }

    public void setName(String name) {
        this.template.setName(name);
    }

    public void appendRawText(String text) {
        this.rawText.append(text);
    }

    public void addAttribute(WikipediaTemplateAttributeAssociation attribute) {
        this.template.addAttribute(attribute);
    }

    public WikipediaTemplate getTemplate() {
        return this.template;
    }

    public String getRawText() {
        return "{{" + this.rawText.toString() + "\n}}";
    }
}
