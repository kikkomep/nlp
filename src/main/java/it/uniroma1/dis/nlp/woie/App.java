/*
 * Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
 * Mountain View, California, 94041, USA.
 */
package it.uniroma1.dis.nlp.woie;

import it.uniroma1.dis.nlp.woie.auerlehmann.AuerLehmannApp;
import it.uniroma1.dis.nlp.woie.common.wikipedia.utils.WikipediaProcessedPageHistory;
import it.uniroma1.dis.nlp.woie.utils.AppEntityManagerFactory;
import it.uniroma1.dis.nlp.woie.wuweld.WuWeldApp;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 */
public abstract class App {

    /**
     * Config folder name
     */
    private static final String CONFIG_FOLDER = "config";
    /**
     * Logger for the current app instance
     */
    protected Logger logger = null;
    /**
     * The current instance
     */
    private static App app = null;
    /**
     * Parameters fileName ("configuration.properties" by default)
     */
    protected String parametersFileName = "configuration.properties";
    /**
     * Parameters fileName ("configuration.properties" by default)
     */
    protected Properties configurationProperties = null;
    /**
     * InputFilename
     */
    protected String inputFileName = null;
    /**
     * OutputFilename
     */
    protected String outputFileName = null;

    /**
     * Arguments
     */
    protected static HashMap<String, String> arguments = new HashMap<String, String>();

    /**
     * Initialization method for the specific app, called after the
     * parsing of parameters
     */
    public abstract void start();

    public String getParametersFileName() {
        return parametersFileName;
    }

    public String getInputFileName() {
        return inputFileName;
    }

    public String getOutputFileName() {
        return outputFileName;
    }

    public Properties getConfigurationProperties() {
        return configurationProperties;
    }

    public Set<String> getArgumentNames() {
        return arguments.keySet();
    }

    public String getArgumentValue(String name) {
        return arguments.get(name);
    }

    /**
     * Return the current app instance
     *
     * @return App the current app instance
     */
    public static App getInstance() {
        return app;
    }

    /**
     * Entry point of this app
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println(
                    "You miss the parameter --app [wuweld|auerlehmann]");
            System.exit(0);
        }

        init(null, args);
    }

    /**
     * Initialization of this app
     *
     * @param args
     */
    protected static void init(App instance, String[] args) {

        Properties log4jProperties = new Properties();

        try {

            // Set app instance
            app = instance;

            // Parameters as String
            String parameters = StringUtils.join(args, " ", 0, args.length);

            // RegExp vars
            /*
            Pattern pattern = null;
            Matcher matcher = null;

            pattern = Pattern.compile("--([^\\s]+)\\s*([^\\s(--)]+)?");
            matcher = pattern.matcher(parameters);
            while (matcher.find()) {
                String parName = matcher.group(1);
                String parValue = matcher.group(2);
                System.out.println("Parameter " + parName + "=" + parValue); // Decomment for debug
                arguments.put(parName, parValue);
            }*/


            int count = 0;
            String parName = null;
            String parValue = "";
            for (String arg : args) {
                count++;

                if (arg.startsWith("--")) {

                    if (parName != null) {
                        //System.out.println("Parameter " + parName + (parValue.isEmpty() ? "" : "=" + parValue)); // Decomment for debug
                        arguments.put(parName, parValue.trim());
                        parValue = "";
                    }

                    parName = arg.substring(2);


                } else {
                    if (!parValue.isEmpty()) parValue += " ";
                    parValue += arg;
                }

                if (count == args.length)
                    arguments.put(parName, parValue);
            }


//            for (String k : arguments.keySet()) {
//                System.out.println(k + "  " + arguments.get(k));
//            }


            // Parameter --app
            String appName = null;
            if (app == null) {
                appName = arguments.get("app");

                if (appName.equals("wuweld")) {
                    app = WuWeldApp.class.newInstance();
                } else if (appName.equals("auerlehmann")) {
                    app = AuerLehmannApp.class.newInstance();
                }
            }

            app.logger = LoggerFactory.getLogger(app.getClass());

            // Parameter --parameters
            if (arguments.get("parameters") != null) {
                app.parametersFileName = arguments.get("parameters");
            }


            // Read properties files if it exists         
            app.configurationProperties = new Properties();
            app.configurationProperties.load(new FileInputStream(
                    CONFIG_FOLDER + File.separator + app.parametersFileName));

            log4jProperties.
                    load(new FileInputStream(
                            CONFIG_FOLDER + File.separator + "log4j.properties"));

            PropertyConfigurator.configure(log4jProperties);

            AppEntityManagerFactory.init(app.getConfigurationProperties());

            Date start = Calendar.getInstance().getTime();


            // lauch start of the current app instance
            app.start();


            Date end = Calendar.getInstance().getTime();


            app.logger.info("\n\n ===> Total Processing time: " + (end.getTime() - start.getTime()) + " ms. <===\n");

            app.logger.info("\n **** " + app.getClass().getSimpleName() + " closed ****\n");


        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(App.class.getName()).
                    log(Level.SEVERE, ex.getMessage(), ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(App.class.getName()).
                    log(Level.SEVERE, ex.getMessage(), ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(App.class.getName()).
                    log(Level.SEVERE, ex.getMessage(), ex);
        }finally {
            WikipediaProcessedPageHistory.getInstance().save();
        }
    }


    public Object[] getConfiguredObjectsByPrefix(String prefix) {

        ArrayList list = new ArrayList();

        Pattern pattern = Pattern.compile(prefix + "_(\\w)+");
        for (Object p : this.getConfigurationProperties().keySet()) {
            String propertyValue = String.valueOf(p);
            Matcher matcher = pattern.matcher(propertyValue);
            if (matcher.find()) {
                String objectName = matcher.group(1);
                String objectClassName = this.getConfigurationProperties().getProperty(
                        propertyValue);
                try {

                    Object instance = Class.forName(
                            objectClassName).newInstance();

                    list.add(instance);

                    this.logger.info(prefix + " " + objectName + " (" + instance.
                            getClass().getSimpleName() + ") registered");

                } catch (ClassNotFoundException ex) {
                    this.logger.error(
                            "Impossible to load the class '" + objectClassName + "' for the object " + objectName);

                } catch (Exception ex) {
                    this.logger.error(
                            "Impossible to instantiate the class '" + objectClassName + "' for the object " + objectName);
                }
            }
        }

        Object[] result = new Object[list.size()];
        return list.toArray(result);
    }
}
