/*
 * Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
 * Mountain View, California, 94041, USA.
 */
package it.uniroma1.dis.nlp.woie.auerlehmann;

import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaTemplate;
import org.apache.log4j.Logger;

import java.util.regex.Pattern;

/**
 *
 * @author kikkomep
 */
public class WikipediaTemplateNumberOfAttributesFilter extends WikipediaTemplateFilter {

    private Logger logger = Logger.getLogger(WikipediaTemplateNumberOfAttributesFilter.class);

    /**
     * Rejects a template if the of number of its triples
     * is less than 3 (included the blank node used to represent 
     * the template itself).
     * 
     * @param template a template
     * @return <code>true</code> if the number of triples is less than 3;
     * <code>false</code> otherwise
     */
    @Override
    public boolean reject(WikipediaTemplate template) {

        boolean result = true;
        if(template!=null && template.getName()!=null ){

            logger.debug("\n\n**** " + WikipediaTemplateNumberOfAttributesFilter.class.getName() + " ****");
            logger.debug("===> Template: " + template);
            logger.debug("===> ParentTemplate: " + template.getParentAssociation());
            logger.debug("===> Triples: " + template.getAttributes().size());

            // We reject a template if the number of its attributes is less than or equal to 3
            if(Pattern.matches("\\blist\\b", template.getName()) || template.getAttributes().size()>3)
                result = false;
        }
        return result;
    }
}
