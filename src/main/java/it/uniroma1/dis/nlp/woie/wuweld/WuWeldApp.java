/*
 * Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *
 * 
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
 * Mountain View, California, 94041, USA.
 */
package it.uniroma1.dis.nlp.woie.wuweld;

import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.TypedDependency;
import it.uniroma1.dis.nlp.woie.App;

import it.uniroma1.dis.nlp.woie.common.wikipedia.*;
import it.uniroma1.dis.nlp.woie.common.wikipedia.services.synonym.WikipediaSynonymsRepository;
import it.uniroma1.dis.nlp.woie.common.wikipedia.template.parser.ParseException;
import it.uniroma1.dis.nlp.woie.common.wikipedia.utils.*;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.*;

import java.io.*;
import java.util.*;
import java.util.List;
import java.util.regex.Pattern;

//Facades

/**
 * @author kikkomep
 */
public class WuWeldApp extends App {


    private TrainingSetHistory registry;

    @Override
    public void start() {

        registry = TrainingSetHistory.getInstance();
        try {

            if (getArgumentNames().contains("training")) {

                boolean forceUpdate = getArgumentNames().contains("force-update");

                String outputFile = getArgumentValue("out");
                logger.info("Output file: " + getArgumentValue("out"));

                if (getArgumentNames().contains("page")) {

                    logger.info("Input page for the training: " + getArgumentValue("page"));
                    processPage(getArgumentValue("page"), forceUpdate);

                } else if (getArgumentNames().contains("limit")) {

                    try {

                        int limit = Integer.parseInt(getArgumentValue("limit"));
                        logger.info("Number of pages to process: " + limit);

                        logger.info("\n");
                        logger.info("The actual training set uses " + registry.countProcessedPages() + " pages ");
                        logger.info("Preparing the training of new " + limit + " pages ...");
                        logger.info("\n");
                        wuWeldTraining(limit, outputFile);

                    } catch (NumberFormatException ne) {
                        System.out.println("\nERROR: You have to specify a valid number of pages !!!");
                    }
                }

            } else if (getArgumentNames().contains("file")) {

                String textFile = getArgumentValue("file");
                test(textFile);

            } else { // Default mode interactive

                boolean stop = false;
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

                try {
                    while (!stop) {


                        System.out.print("\nInsert a new sentence: ");
                        String text = in.readLine();
                        if (text != null) {
                            testSentence(text, 0);
                        }


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } finally {
            registry.save();
        }
    }

    private void testiSingleSentence(String inputFile, int i) {

        try {
            BufferedReader inputFileReader = new BufferedReader(new FileReader(inputFile));
            String line = null;
            int count = 1;
            try {
                while ((line = inputFileReader.readLine()) != null) {
                    if (count != i) {
                        count++;
                        continue;
                    }

                    testSentence(line, count);

                    break;

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void testSentence(String line, int count) {
        PlainTextPipeline ptp = new PlainTextPipeline(line);
        ptp.process();
        logger.info("==============================================================");
        logger.info(count++ + ") " + line);

        logger.info("Relations Extracted: \n");
        List<InformationTriple> triples = ptp.getTriples();
        for (InformationTriple triple : triples) {
            logger.info(triple.toString());
        }

        Collection<TypedDependency> tdl = ptp.getCollectionOfCCProcessedTypedDependencies();
        logger.info("\nDependecy tree Collapsed and processed:");
        for (TypedDependency ty : tdl) {
            String rel = createStringGeneralizedRelation(ty.reln(), ty.gov().label().tag(), ty.dep().label().tag());
            if (rel.length() < 12) {
                logger.info(rel + "\t\t\t\t" + ty.toString());
            } else if (rel.length() >= 12) {
                logger.info(rel + "\t\t\t" + ty.toString());
            }
        }

        tdl = ptp.getDependecies();

        logger.info("\nDependecy tree including extra :");

        for (TypedDependency ty : tdl) {
            String rel = createStringGeneralizedRelation(ty.reln(), ty.gov().label().tag(), ty.dep().label().tag());
            if (rel.length() < 12) {
                logger.info(rel + "\t\t\t\t" + ty.toString());
            } else if (rel.length() >= 12) {
                logger.info(rel + "\t\t\t" + ty.toString());
            }
        }
    }


    protected static String createStringGeneralizedRelation(GrammaticalRelation g, String tagGovernor, String tagDependent) {
        StringBuilder sb = new StringBuilder();
        Generalizator gen = Generalizator.getInstance();

        sb.append((gen.transformRelation(g)));
        sb.append("(");
        sb.append(gen.transfoPOS(tagGovernor));
        sb.append(",");
        sb.append(gen.transfoPOS(tagDependent));
        sb.append(")");

        return sb.toString();

    }

    public void test(String inputFile) {
        try {
            BufferedReader inputFileReader = new BufferedReader(new FileReader(inputFile));
            String line = null;
            int count = 1;
            try {
                while ((line = inputFileReader.readLine()) != null) {
                    if (line.isEmpty()) continue;
                    PlainTextPipeline ptp = new PlainTextPipeline(line);
                    ptp.process();
                    logger.info("==============================================================");
                    logger.info(count++ + ") " + line);

                    logger.info("Relations Extracted: \n");
                    List<InformationTriple> triples = ptp.getTriples();
                    for (InformationTriple triple : triples) {
                        logger.info(triple.toString());
                    }

                    Collection<TypedDependency> tdl = ptp.getCollectionOfCCProcessedTypedDependencies();
                    logger.info("\nDependecy tree Collapsed and processed:");
                    for (TypedDependency ty : tdl) {
                        String rel = createStringGeneralizedRelation(ty.reln(), ty.gov().label().tag(), ty.dep().label().tag());
                        if (rel.length() < 12) {
                            logger.info(rel + "\t\t\t\t" + ty.toString());
                        } else if (rel.length() >= 12) {
                            logger.info(rel + "\t\t\t" + ty.toString());
                        }
                    }

                    tdl = ptp.getDependecies();
                    logger.info("\nDependecy tree including extra :");
                    for (TypedDependency ty : tdl) {
                        String rel = createStringGeneralizedRelation(ty.reln(), ty.gov().label().tag(), ty.dep().label().tag());
                        if (rel.length() < 12) {
                            logger.info(rel + "\t\t\t\t" + ty.toString());
                        } else if (rel.length() >= 12) {
                            logger.info(rel + "\t\t\t" + ty.toString());
                        }
                    }

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void prepareTesting() {

        Wikipedia wikipedia = Wikipedia.getInstance();
        int num = 0;
        Pattern toBeFiltered = Pattern.compile("(?i)REDIRECT|redirect|disambiguation");
        for (WikipediaPage wp : wikipedia.findAll()) {

            try {
                logger.info(wp.getTitle());
                String text = wp.getPlainText();
                PlainTextPipeline ptp = new PlainTextPipeline(text);

                List<String> sentences = ptp.getSentencesTitleFiltered(wp.getTitle());
                for (String s : sentences) {

                    if (!toBeFiltered.matcher(s).find()) {
                        logger.info(num++ + ") " + s);
                    }
                }


            } catch (WikipediaPageInputStreamNotAvailableException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            if (num >= 300) {
                break;
            }
        }

    }

    public void tryP(String pageTitle) {
        Wikipedia wppr = Wikipedia.getInstance();
        WikipediaPage wp = null;
        if (wp == null) {
            try {
                wp = wppr.loadByTitle(pageTitle);
                wppr.parse(wp);
                wppr.save(wp);

                for (WikipediaTemplate template : wp.getTemplates()) {

                    if (template.getName().startsWith("Infobox")) {

                        logger.debug("Analysing Infobox...");

                        //get the attribute list of the page
                        List<WikipediaTemplateAttributeAssociation> attributesList = (List<WikipediaTemplateAttributeAssociation>) template.getAttributes();
                        Iterator<WikipediaTemplateAttributeAssociation> attributesItearator = attributesList.iterator();

                        //associating to each attribute value a regex that matches the value and its synonyms
                        while (attributesItearator.hasNext()) {

                            WikipediaTemplateAttributeAssociation attributeAssociation = attributesItearator.next();
                            WikipediaTemplateAttribute wta = attributeAssociation.getAttribute();

                            logger.debug("\n" + wta.getClass().getCanonicalName() + "\n");

                            List<String> a = WikipediaUtils.getLiteralAttributeValues(wta);

                            for (String s : a) {
                                logger.debug(s);
                            }
                        }
                    }
                }
            } catch (WikipediaPageNotFoundException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (WikipediaPageInputStreamNotAvailableException e) {
                e.printStackTrace();
            }
        }
    }

    private void processPage(String pageTitle, boolean forceUpdate) {

        logger.info("\nProcessing Pages: " + pageTitle);

        Wikipedia wppr = Wikipedia.getInstance();

        try {

            WikipediaPage wp = wppr.findByTitle(pageTitle);
            if (wp == null) {
                wp = wppr.loadByTitle(pageTitle);
                wppr.parse(wp);
                wppr.save(wp);
            } else if (forceUpdate) {
                wp = wppr.refresh(wp);
            }

            logger.info("Page Found: " + (wp != null));

            WikipediaPagePipeline wpp = new WikipediaPagePipeline(wp);
            wpp.process();

            HashMap<String, PathTypedDependency> res = wpp.getMatchings();

            for (String sentence : res.keySet()) {
                logger.info("------------------------------------------");
                logger.info("Sentence: " + sentence);
                logger.info("\nGeneralizedCorePath: \n");
                for (String gcp : res.get(sentence).getPath()) {
                    logger.info(gcp);
                }
            }

            //TODO
            registry.addProcessedPage(wp.getTitle());

        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }


    private void wuWeldTraining(int numOfPages, String outputFile) {

        int toSkip = registry.countProcessedPages();
        wuWeldTraining(numOfPages, toSkip, outputFile);
    }


    private void wuWeldTraining(int numOfPages, int skip, String outputFile) {

        int pageWithInfobox = 0;
        int numberOfPath = 0;


        int numberOfProcessedPages = 0;
        Wikipedia wikipedia = Wikipedia.getInstance();
        for (WikipediaPage wp : wikipedia.find(numOfPages, skip, true)) {

            numberOfProcessedPages++;
            if (numberOfProcessedPages == 1) logger.info("\n\n");

            logger.info("______________________________________________________________________");
            logger.info("\n Processing Page #" + numberOfProcessedPages + ": " + wp.getTitle());

            List<WikipediaTemplate> wtl = wp.getTemplates();
            HashMap<String, PathTypedDependency> res = null;

            for (WikipediaTemplate wt : wtl) {
                if (wt != null && wt.getName() != null && wt.getName().startsWith("Infobox")) {
                    pageWithInfobox++;
                    try {
                        WikipediaPagePipeline wpp = new WikipediaPagePipeline(wp);
                        wpp.process();
                        res = wpp.getMatchings();

                        for (String sentence : res.keySet()) {
                            logger.info("------------------------------------------");
                            logger.info("Sentence: " + sentence);
                            logger.info("\nGeneralizedCorePath: \n");
                            for (String gcp : res.get(sentence).getPath()) {
                                numberOfPath++;
                                logger.info(gcp);
                            }
                        }

                    } catch (IOException e) {
                        logger.error(e.getMessage());
                    } catch (WikipediaPageInputStreamNotAvailableException e) {
                        logger.error(e.getMessage());
                    } catch (it.uniroma1.dis.nlp.woie.wuweld.pipeline.ParseException e) {
                        logger.error(e.getMessage());
                    }
                }
            }


            if (outputFile != null) {
                File report = new File(outputFile);
                if (res != null && !res.keySet().isEmpty()) {
                    try {
                        FileWriter fw = new FileWriter(report, true);
                        fw.write("------------------------------------------\n");
                        fw.write("\nPage: " + wp.getTitle() + "\n");
                        int count = 1;
                        for (String sentence : res.keySet()) {
                            fw.write("\tSentence: " + "\"" + sentence + "\"\n\n");
                            fw.write("\tDependency Graph : \n\n\t\t" + res.get(sentence).getSemanticGraphCCProcessedIncludeExtra().toString("plain").replaceAll("\\n", "\n\t\t") + "\n\nExtracted Pattern : \n\n");
                            for (String path : res.get(sentence).getPath()) {
                                fw.write("\t\t" + count++ + ")" + path + "\n");
                            }
                            fw.write("\n\nPage with Infofox: " + pageWithInfobox + "\n");
                        }
                        fw.flush();
                        fw.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            registry.addProcessedPage(wp.getTitle());
        }

        logger.info("______________________________________________________________________\n\n");
        logger.info(" -- Overall Statistics -- ");
        logger.info(" # of pages processed: " + (numberOfProcessedPages));
        logger.info(" # of pages containing infobox: " + pageWithInfobox);
        logger.info(" # generalized Core Paths Found: " + numberOfPath);
        logger.info("\n\n______________________________________________________________________");
    }

    private void getSynonyms(int numOfPages, int skip) {

        Wikipedia wikipedia = Wikipedia.getInstance();
        WikipediaSynonymsRepository sf = WikipediaSynonymsRepository.getInstance();

        for (WikipediaPage wp : wikipedia.find(numOfPages, skip, true)) {

            List<WikipediaTemplate> wtl = wp.getTemplates();

            for (WikipediaTemplate wt : wtl) {
                if (wt != null && wt.getName() != null && wt.getName().startsWith("Infobox")) {

                    List<String> synTitle = new ArrayList<>(sf.findSynonyms(wp.getTitle()));
                    for (String s : synTitle) {
                        System.out.print(s + " ");
                    }
                    System.out.println();


                    //get the attribute list of the page
                    List<WikipediaTemplateAttributeAssociation> attributesList = (List<WikipediaTemplateAttributeAssociation>) wt.getAttributes();
                    Iterator<WikipediaTemplateAttributeAssociation> attributesItearator = attributesList.iterator();


                    //associating to each attribute value a regex that matches the value and its synonyms
                    while (attributesItearator.hasNext()) {

                        WikipediaTemplateAttributeAssociation attributeAssociation = attributesItearator.next();
                        WikipediaTemplateAttribute wta = attributeAssociation.getAttribute();

                        List<String> a = it.uniroma1.dis.nlp.woie.common.wikipedia.utils.WikipediaUtils.getLiteralAttributeValues(wta);

                        for (String s : a) {
                            if (s != null) {

                                List<String> syn = new ArrayList<>(sf.findSynonyms(s));
                                for (String str : syn) {
                                    System.out.print(str + " ");
                                }
                                System.out.println();

                            }
                        }

                    }

                }
            }

        }

    }


    private static void printUsage() {
        System.out.println("\n\tOptions:");
        System.out.println("\t --training [--limit <NUM_OF_PAGES> | --page <PAGE_TITLE>]");
        System.out.println("\t [--limit <NUM_OF_PAGES> | --page <PAGE_TITLE>]");
    }


    /**
     * Entry for the WuWeldApp
     *
     * @param args
     */
    public static void main(String args[]) {
        init(new WuWeldApp(), args);
    }
}
