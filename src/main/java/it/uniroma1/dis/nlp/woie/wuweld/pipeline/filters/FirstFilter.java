package it.uniroma1.dis.nlp.woie.wuweld.pipeline.filters;

import it.uniroma1.dis.nlp.woie.common.wikipedia.WikipediaCategoryAttributeCorePath;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.AttributePattern;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.Match;
import it.uniroma1.dis.nlp.woie.wuweld.pipeline.ParsedSentence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * Created by lgu on 05/02/14.
 */
public class FirstFilter {

    private static final Logger logger = LoggerFactory.getLogger(it.uniroma1.dis.nlp.woie.wuweld.pipeline.Pipeline.class);

    //it skips the attribute completely when multiple sentences mention
    //the value or its synonym

    public static boolean isApplicable(Map<AttributePattern, ParsedSentence> attributeSentenceMap, AttributePattern attributeInfobox, ParsedSentence sentence){

        logger.debug("First filtering");


        return attributeSentenceMap.get(attributeInfobox) != null           // == NULL if the attribute is not associated to any sentence
                && attributeSentenceMap.get(attributeInfobox) != sentence;  //
    }

    public static void apply(Map<AttributePattern, List<WikipediaCategoryAttributeCorePath>> attributeFiltering,
                             Map<AttributePattern, ParsedSentence> attributeApp,
                             AttributePattern ap){

        logger.debug("The attribute was already found, not in this sentence!");
        Match toRemove = null;

        //loadByTitle the sentence to remove
        for (Match ma : attributeApp.get(ap).getAttributeMatchings()) {
            if (ma.getAttributeValue().getAttributeKey().equalsIgnoreCase(ap.getAttribute())) {
                toRemove = ma;
            }
        }

        if (toRemove != null) {

            logger.debug("\tRemoving match: " + toRemove.toString());
            attributeApp.get(ap).getAttributeMatchings().remove(toRemove);
            attributeApp.remove(ap);
            attributeFiltering.remove(ap);

        } else {

            logger.debug("Error: no match to be removed");

        }

    }

}
