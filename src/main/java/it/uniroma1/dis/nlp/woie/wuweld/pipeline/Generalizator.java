package it.uniroma1.dis.nlp.woie.wuweld.pipeline;

import edu.stanford.nlp.trees.GrammaticalRelation;
import java.util.HashMap;

/**
 *
 * @author lgu
 */
public class Generalizator {

    private  HashMap<String, String> posTransformer = new HashMap<>();
    private static final Generalizator instance = new Generalizator();
    private  HashMap<String, Character> wordNetTransormer = new HashMap<>();

    public static final String VERB = "V";

    private Generalizator() {
        initializationGuarantor();
    }

    public static Generalizator getInstance() {
        return instance;
    }

    private  void initializationGuarantor() {

        posTransformer.put("NN", "N");
        posTransformer.put("NNS", "N");
        posTransformer.put("NNP", "N");
        posTransformer.put("NNPS", "N");
        posTransformer.put("PRP", "N");

        posTransformer.put("VB", VERB);
        posTransformer.put("VBD", VERB);
        posTransformer.put("VBG", VERB);
        posTransformer.put("VBN", VERB);
        posTransformer.put("VBP", VERB);
        posTransformer.put("VBZ", VERB);

        posTransformer.put("WRB", "RB");
        posTransformer.put("RB", "RB");
        posTransformer.put("RBR", "RB");
        posTransformer.put("RBS", "RB");

        posTransformer.put("JJ", "J");
        posTransformer.put("JJR", "J");
        posTransformer.put("JJS", "J");

        wordNetTransormer.put("NN", 'n');
        wordNetTransormer.put("NNS", 'n');
        wordNetTransormer.put("NNP", 'n');
        wordNetTransormer.put("NNPS", 'n');
        wordNetTransormer.put("PRP", 'n');

        wordNetTransormer.put("VB", 'v');
        wordNetTransormer.put("VBD", 'v');
        wordNetTransormer.put("VBG", 'v');
        wordNetTransormer.put("VBN", 'v');
        wordNetTransormer.put("VBP", 'v');
        wordNetTransormer.put("VBZ", 'v');

        wordNetTransormer.put("WRB", 'r');
        wordNetTransormer.put("RB", 'r');
        wordNetTransormer.put("RBR", 'r');
        wordNetTransormer.put("RBS", 'r');

        wordNetTransormer.put("JJ", 'a');
        wordNetTransormer.put("JJR", 'a');
        wordNetTransormer.put("JJS", 'a');


    }

    public String transformRelation(GrammaticalRelation g) {

        if(g.toString().matches(".*(prep).*")){
            return "prep";
        }
        return g.toString();
    }

    public String transfoPOS(String p) {

        if (posTransformer.containsKey(p)) {
            return posTransformer.get(p);
        } else {
            return p;
        }
    }

    public Character transtormPOSForBabelNet(String p) throws ParseException {
        if (posTransformer.containsKey(p)) {
            return wordNetTransormer.get(p);
        } else {
            throw new ParseException("Cannot converte the POS Tag "+p);
        }
    }
}
