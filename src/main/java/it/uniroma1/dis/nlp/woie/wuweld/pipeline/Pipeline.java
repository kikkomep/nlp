package it.uniroma1.dis.nlp.woie.wuweld.pipeline;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import opennlp.tools.sentdetect.SentenceDetector;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author lgu
 */
public abstract class Pipeline {

    protected final Logger logger = LoggerFactory.getLogger(it.uniroma1.dis.nlp.woie.wuweld.pipeline.Pipeline.class);
    protected final StanfordCoreNLP stanfordCorePipeline;
    private final TreebankLanguagePack treebankLanguagePack;
    protected final GrammaticalStructureFactory grammaticalStructureFactory;
    protected final String text;
    protected ParsedSentence[] parsedSentences;
    public static final String FIRST_EDGE_PATTERN = ".*(subj).*|partmod|rcmod";
    protected int numberOfProcessableSentencesLimit = -1;

    public abstract void process();

    protected Pipeline(String text) {

//        Properties props = new Properties();
//        props.put("annotators", "tokenize, ssplit, parse");
//
//        // creates a StanfordCoreNLP object with parsing capabilities
//        stanfordCorePipeline = new StanfordCoreNLP(props);
//        treebankLanguagePack = new PennTreebankLanguagePack();
//        grammaticalStructureFactory = treebankLanguagePack.grammaticalStructureFactory();

        StanfordPipeline sp = StanfordPipeline.getInstance();
        stanfordCorePipeline = sp.getStanfordCorePipeline();
        treebankLanguagePack = sp.getTreebankLanguagePack();
        grammaticalStructureFactory = sp.getGrammaticalStructureFactory();

        this.text = text;

    }


    protected void setNumberOfProcessableSentencesLimit(int numberOfProcessableSentencesLimit) {
        this.numberOfProcessableSentencesLimit = numberOfProcessableSentencesLimit;
    }

    protected String[] splitIntoSentences(String html) {

        String[] sentences = null;

        try {

            SentenceDetector sentenceDetector;
            try (InputStream modelIn = new FileInputStream("open_nlp_models/en-sent.bin")) {

                logger.debug("Model input stream: " + modelIn);

                // Loading sentence detection model
                final SentenceModel sentenceModel = new SentenceModel(modelIn);
                sentenceDetector = new SentenceDetectorME(sentenceModel);

                Pattern aTagPattern = Pattern.compile("(<a.*?>)(.*?(?<=</a>))");

                int count = 0;
                String sentence;
                String replacement;
                sentences = sentenceDetector.sentDetect(html);

                String openTag;
                String textTag;


                for (int j = 0; j < sentences.length; j++) {

                    sentence = sentences[j];
                    replacement = "";

                    int lastIndex = 0;

                    Matcher openTagMatcher = aTagPattern.matcher(sentence);
                    while (openTagMatcher.find()) {

                        String txt = openTagMatcher.group();
                        logger.debug("TAG DETECTED:  " + txt);

                        openTag = openTagMatcher.group(1);
                        textTag = openTagMatcher.group(2).replace("</a>", "");

                        replacement += sentence.substring(lastIndex, openTagMatcher.start());

                        String replacementText = textTag.replaceAll(" ", "_");
                        String replacementTag = openTag + replacementText + "</a>";
                        replacement += replacementTag;

                        lastIndex = openTagMatcher.end();

                        logger.debug("ORIGINAL TAG TEXT: " + textTag);
                        logger.debug("REPLACEMENT TAG TEXT: " + replacementText);
                    }

                    replacement += sentence.substring(lastIndex, sentence.length());

                    sentences[j] = replacement;

                    logger.debug("Sentence " + count + ": " + replacement);
                    count++;

                    if(j==this.numberOfProcessableSentencesLimit) break;
                }
            }
            return sentences;

        } catch (FileNotFoundException ex) {
            logger.error(ex.getMessage(), ex);
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
        }

        return sentences;
    }

    protected void initializePipeline() {

        //split the text into sentences
        String[] sentences = this.splitIntoSentences(text);

        //array of parsed sentences
        parsedSentences = new ParsedSentence[sentences.length];

        //adding the annotation (dependency tree and POS) to each sentence
        int cont = 0;
        for (String s : sentences) {
            //annotate sentence
            Annotation document = new Annotation(s);
            stanfordCorePipeline.annotate(document);
            parsedSentences[cont++] = new ParsedSentence(document, s);
            logger.debug("Sentence " + cont + " annotated : " + s);
        }

    }

    protected void printDependcies(ParsedSentence sentence) {

        logger.debug("==========================================");
        logger.debug("Sentence: \"" + sentence.getSentence() + "\"");
        logger.debug("\n");

        for (CoreMap c : sentence.getAnnotation().get(CoreAnnotations.SentencesAnnotation.class)) {
            Tree t = c.get(TreeCoreAnnotations.TreeAnnotation.class);
            GrammaticalStructure gs = grammaticalStructureFactory.newGrammaticalStructure(t);

            Collection<TypedDependency> tdl = gs.typedDependenciesCCprocessed(true);
            for (TypedDependency ty : tdl) {
                logger.debug(ty.toString());
            }
        }
        logger.debug("\n==========================================\n");
    }

    public ParsedSentence[] getParsedSentences() {
        return parsedSentences;
    }
}
class StanfordPipeline {

    private static StanfordPipeline instance= new StanfordPipeline();
    private final StanfordCoreNLP stanfordCorePipeline;
    private final TreebankLanguagePack treebankLanguagePack;
    private final GrammaticalStructureFactory grammaticalStructureFactory;

    private StanfordPipeline(){

        Properties props = new Properties();
        props.put("annotators", "tokenize, ssplit, parse, lemma");

        // creates a StanfordCoreNLP object with parsing capabilities
        stanfordCorePipeline = new StanfordCoreNLP(props);
        treebankLanguagePack = new PennTreebankLanguagePack();
        grammaticalStructureFactory = treebankLanguagePack.grammaticalStructureFactory();

    }

    public static StanfordPipeline getInstance(){
        return instance;
    }

    public StanfordCoreNLP getStanfordCorePipeline() {
        return stanfordCorePipeline;
    }

    public TreebankLanguagePack getTreebankLanguagePack() {
        return treebankLanguagePack;
    }

    public GrammaticalStructureFactory getGrammaticalStructureFactory() {
        return grammaticalStructureFactory;
    }
}
